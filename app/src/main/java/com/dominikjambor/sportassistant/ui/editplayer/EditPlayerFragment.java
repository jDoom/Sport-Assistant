package com.dominikjambor.sportassistant.ui.editplayer;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import com.dominikjambor.sportassistant.R;
import com.dominikjambor.sportassistant.data.Player;
import com.dominikjambor.sportassistant.databinding.FragmentEditPlayerBinding;
import com.dominikjambor.sportassistant.ui.utils.Utils;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class EditPlayerFragment extends Fragment {

    @BindView(R.id.editplayer_team) Spinner teamSpinner;
    @BindView(R.id.editplayer_fab) FloatingActionButton fab;
    @BindView(R.id.editplayer_delete) ImageButton deleteButton;

    private CompositeDisposable trash = new CompositeDisposable();

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        FragmentEditPlayerBinding binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_edit_player,
                container, false);
        binding.setLifecycleOwner(this);

        View root = binding.getRoot();
        ButterKnife.bind(this, root);

        EditPlayerViewModel viewModel = new ViewModelProvider(this).get(EditPlayerViewModel.class);
        if (getArguments() != null) {
            Player player = EditPlayerFragmentArgs.fromBundle(getArguments()).getSelectedPlayer();
            viewModel.setParameters(
                    player, EditPlayerFragmentArgs.fromBundle(getArguments()).getSelectedTeamId()
            );
            deleteButton.setEnabled(player != null);
            deleteButton.setImageAlpha(player != null ? 255 : 155);
        }
        fab.setEnabled(false);
        trash.add(
                viewModel.getTeamNames()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(list -> {
                            ArrayAdapter<String> adapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_list_item_1, list);
                            teamSpinner.setAdapter(adapter);
                            teamSpinner.setSelection(viewModel.getSelectedPlayer() == null ?
                                    0 : viewModel.getPlayerTeamPosition() + 1);
                            teamSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    viewModel.setSelectedTeamPosition(position);
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });
                            fab.setEnabled(true);
                        }, Throwable::printStackTrace)
        );

        deleteButton.setOnClickListener(v -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(requireActivity());
            builder.setMessage(R.string.confirm_player_delete)
                    .setPositiveButton(R.string.yes, (dialog, id) -> viewModel.deleteClicked())
                    .setNegativeButton(R.string.cancel, null);
            builder.create().show();
        });

        viewModel.getDisableFabCommand().observe(getViewLifecycleOwner(),
                o -> fab.setEnabled(false));
        viewModel.getSavedCommand().observe(getViewLifecycleOwner(),
                o -> Navigation.findNavController(root).popBackStack());
        viewModel.getDeletedCommand().observe(getViewLifecycleOwner(),
                o -> Navigation.findNavController(root).popBackStack(R.id.nav_players, false));
        viewModel.getErrCommand().observe(getViewLifecycleOwner(),
                o -> fab.setEnabled(true));

        binding.setViewmodel(viewModel);

        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        trash.clear();
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.action_help) {
            Utils.showSimpleTextDialog(requireContext(), R.string.help_editplayer);
        }
        return false;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.fragments_base, menu);
    }
}
