package com.dominikjambor.sportassistant.ui.teams;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
class TeamListItem {
    private String name;
    private String description;
}
