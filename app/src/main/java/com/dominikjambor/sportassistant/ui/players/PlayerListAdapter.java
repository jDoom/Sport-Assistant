package com.dominikjambor.sportassistant.ui.players;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.dominikjambor.sportassistant.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PlayerListAdapter extends ArrayAdapter<PlayerListItem> {
    private List<PlayerListItem> playerList;
    @BindView(R.id.item_players_teamlist_name) TextView nameTextView;
    @BindView(R.id.item_players_teamlist_description) TextView descriptionTextView;

    PlayerListAdapter(Context context, List<PlayerListItem> playerList) {
        super(context, R.layout.item_players_playerlist, playerList);
        this.playerList = playerList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = convertView != null ? convertView :
                ((LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                        .inflate(R.layout.item_players_playerlist, parent, false);
        ButterKnife.bind(this, view);

        nameTextView.setText(playerList.get(position).getName());
        descriptionTextView.setText(playerList.get(position).getDescription());

        return view;
    }
}
