package com.dominikjambor.sportassistant.ui.editteam;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.dominikjambor.sportassistant.R;
import com.dominikjambor.sportassistant.data.Team;
import com.dominikjambor.sportassistant.data.TeamWithPlayers;
import com.dominikjambor.sportassistant.logic.ApplicationState;
import com.dominikjambor.sportassistant.persistence.SportAssistantPersistence;
import com.dominikjambor.sportassistant.ui.utils.SingleLiveEvent;

import java.util.ArrayList;
import java.util.UUID;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import lombok.Getter;

public class EditTeamViewModel extends AndroidViewModel {
    @Getter private TeamWithPlayers selectedTeamWithPlayers;
    @Getter private MutableLiveData<String> teamName = new MutableLiveData<>("");
    @Getter private MutableLiveData<String> description = new MutableLiveData<>("");
    @Getter private MutableLiveData<String> nameError = new MutableLiveData<>();

    @Getter private SingleLiveEvent<Void> disableFabCommand = new SingleLiveEvent<>();
    @Getter private SingleLiveEvent<Void> savedCommand = new SingleLiveEvent<>();
    @Getter private SingleLiveEvent<Void> errCommand = new SingleLiveEvent<>();
    @Getter private SingleLiveEvent<Void> deletedCommand = new SingleLiveEvent<>();

    private CompositeDisposable trash = new CompositeDisposable();

    public EditTeamViewModel(@NonNull Application application) {
        super(application);
    }

    private boolean newTeam = false;

    void setSelectedTeamWithPlayers(TeamWithPlayers teamWithPlayers) {
        if (teamWithPlayers == null) {
            teamWithPlayers = new TeamWithPlayers(
                    new Team(UUID.randomUUID(), ""),
                    new ArrayList<>()
            );
            newTeam = true;
        }
        this.selectedTeamWithPlayers = teamWithPlayers;
        teamName.postValue(teamWithPlayers.team.getName());
        description.postValue(teamWithPlayers.team.getDescription());
    }

    private boolean checkTeamName() {
        Context context = getApplication();
        String value = teamName.getValue();
        if (value != null) {
            if (value.trim().isEmpty()) {
                nameError.postValue(context.getString(R.string.field_cant_be_empty));
                return false;
            } else if (value.length() >= 30) {
                nameError.postValue(context.getString(R.string.field_too_long));
                return false;
            }
        } else {
            nameError.postValue(context.getString(R.string.unknown_error));
            return false;
        }
        nameError.postValue(null);
        return true;
    }

    public void saveClicked() {
        if (checkTeamName()) {
            disableFabCommand.call();

            selectedTeamWithPlayers.team.setName(teamName.getValue());
            selectedTeamWithPlayers.team.setDescription(description.getValue());

            SportAssistantPersistence persistence = ApplicationState.getInstance().getPersistence();
            Completable action = newTeam ? persistence.saveTeam(selectedTeamWithPlayers.team) :
                    persistence.updateTeam(selectedTeamWithPlayers.team);
            trash.add(
                    action.subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(
                                    () -> savedCommand.call(),
                                    e -> errCommand.call())
            );
        } else {
            errCommand.call();
        }
    }

    void deleteClicked() {
        SportAssistantPersistence persistence = ApplicationState.getInstance().getPersistence();
        trash.add(
                persistence.deleteTeam(selectedTeamWithPlayers.team)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                () -> deletedCommand.call(),
                                e -> errCommand.call())
        );
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        trash.clear();
    }
}
