package com.dominikjambor.sportassistant.ui.editarchivedevent;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import com.dominikjambor.sportassistant.R;
import com.dominikjambor.sportassistant.databinding.FragmentEditArchivedEventBinding;
import com.dominikjambor.sportassistant.ui.utils.Utils;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EditArchivedEventFragment extends Fragment {

    @BindView(R.id.editarchivedevent_fab) FloatingActionButton fab;
    @BindView(R.id.editarchivedevent_delete) ImageButton deleteButton;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        FragmentEditArchivedEventBinding binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_edit_archived_event,
                container, false);
        binding.setLifecycleOwner(this);

        View root = binding.getRoot();
        ButterKnife.bind(this, root);

        EditArchivedEventViewModel viewModel = new ViewModelProvider(this).get(EditArchivedEventViewModel.class);
        if (getArguments() != null) {
            viewModel.setSelectedEvent(EditArchivedEventFragmentArgs.fromBundle(getArguments()).getSelectedEvent()
            );
        }

        deleteButton.setOnClickListener(v -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(requireActivity());
            builder.setMessage(R.string.confirm_event_delete)
                    .setPositiveButton(R.string.yes, (dialog, id) -> viewModel.deleteClicked())
                    .setNegativeButton(R.string.cancel, null);
            builder.create().show();
        });

        viewModel.getDisableFabCommand().observe(getViewLifecycleOwner(),
                o -> fab.setEnabled(false));
        viewModel.getSavedCommand().observe(getViewLifecycleOwner(),
                o -> Navigation.findNavController(root).popBackStack());
        viewModel.getDeletedCommand().observe(getViewLifecycleOwner(),
                o -> Navigation.findNavController(root).popBackStack(R.id.nav_archive, false));
        viewModel.getErrCommand().observe(getViewLifecycleOwner(),
                o -> fab.setEnabled(true));

        binding.setViewmodel(viewModel);

        return root;
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.action_help) {
            Utils.showSimpleTextDialog(requireContext(), R.string.help_editarchivedevent);
        }
        return false;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.fragments_base, menu);
    }
}
