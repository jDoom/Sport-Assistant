package com.dominikjambor.sportassistant.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import com.dominikjambor.sportassistant.R;
import com.dominikjambor.sportassistant.databinding.FragmentHomeBinding;
import com.dominikjambor.sportassistant.ui.utils.Utils;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeFragment extends Fragment {

    @BindView(R.id.home_newEventButton) FloatingActionButton fab;
    @BindView(R.id.home_archivecard) CardView archiveCard;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        FragmentHomeBinding binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_home,
                container, false);
        binding.setLifecycleOwner(this);

        HomeViewModel viewModel = new ViewModelProvider(this).get(HomeViewModel.class);
        binding.setViewmodel(viewModel);

        View root = binding.getRoot();
        ButterKnife.bind(this, root);

        fab.setOnClickListener(view -> Navigation.findNavController(view).navigate(R.id.action_nav_home_to_nav_newevent));
        archiveCard.setOnClickListener(view -> Navigation.findNavController(view).navigate(R.id.action_nav_home_to_nav_archive));
        viewModel.refresh();

        viewModel.getNewEventCommand().observe(getViewLifecycleOwner(), o -> {
            HomeFragmentDirections.ActionNavHomeToNavNewevent action = HomeFragmentDirections
                    .actionNavHomeToNavNewevent();
            action.setSelectedSportPosition(viewModel.getSportPos());
            Navigation.findNavController(root).navigate(action);
        });

        return root;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.action_help) {
            Utils.showSimpleTextDialog(requireContext(), R.string.help_home);
        }
        return false;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.fragments_base, menu);
    }
}