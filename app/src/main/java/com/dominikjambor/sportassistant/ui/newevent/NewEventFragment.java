package com.dominikjambor.sportassistant.ui.newevent;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import com.dominikjambor.sportassistant.R;
import com.dominikjambor.sportassistant.databinding.FragmentNewEventBinding;
import com.dominikjambor.sportassistant.ui.utils.SimpleOnItemSelectedListener;
import com.dominikjambor.sportassistant.ui.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class NewEventFragment extends Fragment {

    @BindView(R.id.newEvent_moduleListView) ListView moduleListView;
    @BindView(R.id.newEvent_team1Spinner) Spinner team1Spinner;
    @BindView(R.id.newEvent_team2Spinner) Spinner team2Spinner;
    @BindView(R.id.newEvent_continueButton) Button continueButton;
    @BindView(R.id.newEvent_sportSpinner) Spinner sportSpinner;
    @BindView(R.id.newEvent_presetSpinner) Spinner presetSpinner;
    @BindView(R.id.newEvent_presetName) EditText presetName;
    @BindView(R.id.newEvent_deletePresetButton) ImageButton deletePresetButton;

    private CompositeDisposable trash = new CompositeDisposable();

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        FragmentNewEventBinding binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_new_event,
                container, false);
        binding.setLifecycleOwner(this);
        NewEventViewModel viewModel = new ViewModelProvider(this).get(NewEventViewModel.class);
        binding.setViewmodel(viewModel);

        View root = binding.getRoot();
        ButterKnife.bind(this, root);

        viewModel.getContinueCommand().observe(getViewLifecycleOwner(),
                o -> {
                    NewEventFragmentDirections.ActionNavNeweventToNavConfig action = NewEventFragmentDirections
                            .actionNavNeweventToNavConfig();
                    action.setSelectedPreset(viewModel.getSelectedPreset());
                    action.setNewPreset(viewModel.isNewPreset());
                    Navigation.findNavController(root).navigate(action);
                });
        viewModel.getSkipToEventCommand().observe(getViewLifecycleOwner(),
                o -> Navigation.findNavController(root).navigate(R.id.action_nav_newevent_to_nav_event));
        viewModel.getCancelCommand().observe(getViewLifecycleOwner(),
                o -> Navigation.findNavController(root).popBackStack());

        updateModuleList(viewModel);
        viewModel.getUpdateModulesCommand().observe(getViewLifecycleOwner(), o -> updateModuleList(viewModel));

        trash.add(
                viewModel.getTeamNames()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(list -> {
                            ArrayAdapter<String> adapter = new ArrayAdapter<>(requireContext(),
                                    android.R.layout.simple_list_item_1, list);
                            team1Spinner.setAdapter(adapter);
                            team1Spinner.setSelection(viewModel.getTeam1Selection());
                            team1Spinner.setOnItemSelectedListener(
                                    new SimpleOnItemSelectedListener(viewModel::setTeam1Selection));
                            team2Spinner.setAdapter(adapter);
                            team2Spinner.setSelection(viewModel.getTeam2Selection());
                            team2Spinner.setOnItemSelectedListener(
                                    new SimpleOnItemSelectedListener(viewModel::setTeam2Selection));
                            if (list.isEmpty()) {
                                adapter.add(requireContext().getString(R.string.noteams));
                                team1Spinner.setEnabled(false);
                                team2Spinner.setEnabled(false);
                            }
                            continueButton.setEnabled(false);
                        }, Throwable::printStackTrace)
        );
        updatePresetList(viewModel);
        viewModel.getShowPresetNameCommand().observe(getViewLifecycleOwner(), o -> {
            deletePresetButton.setVisibility(viewModel.getSelectedPreset() == null ? GONE : VISIBLE);
            presetName.setVisibility(VISIBLE);
        });
        viewModel.getHidePresetNameCommand().observe(getViewLifecycleOwner(), o -> {
            deletePresetButton.setVisibility(viewModel.getSelectedPreset() == null ? GONE : VISIBLE);
            presetName.setVisibility(GONE);
        });
        viewModel.getUpdateButtonCommand().observe(getViewLifecycleOwner(), b -> continueButton.setEnabled(b));

        viewModel.getUpdatePresetCommand().observe(getViewLifecycleOwner(), o -> {
            updateModuleList(viewModel);
            sportSpinner.setSelection(viewModel.getSportTypeSelection());
        });

        deletePresetButton.setOnClickListener(v -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(requireActivity());
            builder.setMessage(R.string.confirm_preset_delete)
                    .setPositiveButton(R.string.yes, (dialog, id) -> trash.add(viewModel.deletePresetClicked()
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(() -> updatePresetList(viewModel))))
                    .setNegativeButton(R.string.cancel, null);
            builder.create().show();
        });

        sportSpinner.setAdapter(new ArrayAdapter<>(requireContext(),
                android.R.layout.simple_list_item_1,
                viewModel.getSportNameList()));
        sportSpinner.setSelection(viewModel.getSportTypeSelection());
        sportSpinner.setOnItemSelectedListener(new SimpleOnItemSelectedListener(viewModel::selectSport));

        if (getArguments() != null) {
            NewEventFragmentArgs args = NewEventFragmentArgs.fromBundle(getArguments());
            sportSpinner.setSelection(args.getSelectedSportPosition());
        }

        return root;
    }

    private void updatePresetList(NewEventViewModel viewModel) {
        trash.add(viewModel.getPresetList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        list -> {
                            ArrayAdapter<String> adapter = new ArrayAdapter<>(requireContext(),
                                    android.R.layout.simple_list_item_1, list);
                            presetSpinner.setAdapter(adapter);
                            presetSpinner.setSelection(viewModel.getPresetSelection());
                            presetSpinner.setOnItemSelectedListener(
                                    new SimpleOnItemSelectedListener(viewModel::presetSelected));
                        }
                )
        );
    }

    private void updateModuleList(NewEventViewModel viewModel) {
        moduleListView.setAdapter(new ModuleListAdapter(getContext(),
                viewModel.getModuleListItems(),
                viewModel.getSelectedSportType())
        );
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        trash.clear();
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.action_help) {
            Utils.showSimpleTextDialog(requireContext(), R.string.help_newevent);
        }
        return false;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.fragments_base, menu);
    }
}
