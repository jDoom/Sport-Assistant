package com.dominikjambor.sportassistant.ui.configurator;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModel;

import com.dominikjambor.sportassistant.data.Preset;
import com.dominikjambor.sportassistant.logic.ApplicationState;
import com.dominikjambor.sportassistant.logic.SportEvent;
import com.dominikjambor.sportassistant.modules.Module;
import com.dominikjambor.sportassistant.persistence.SportAssistantPersistence;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import io.reactivex.Completable;
import lombok.Getter;
import lombok.Setter;

import static com.dominikjambor.sportassistant.logic.SportEvent.EventState.CREATED;
import static com.dominikjambor.sportassistant.ui.configurator.ConfiguratorViewModel.ConfigurationType.CONFIGURATION;
import static com.dominikjambor.sportassistant.ui.configurator.ConfiguratorViewModel.ConfigurationType.FINALIZATION;

public class ConfiguratorViewModel extends ViewModel {

    private SportEvent currentEvent;

    public enum ConfigurationType {CONFIGURATION, FINALIZATION}

    private List<Fragment> fragments;

    @Setter private Preset preset;
    @Getter
    @Setter
    private boolean isNewPreset = false;

    List<Fragment> getFragments() {
        JSONObject data = preset == null ? null : preset.getData();
        if (fragments == null) {
            fragments = getCurrentEvent().getModules().stream()
                    .map(m -> {
                        JSONObject jsonPreset = null;
                        try {
                            if (data != null) {
                                if (data.has(m.getClass().getSimpleName())) {
                                    jsonPreset = data.getJSONObject(m.getClass().getSimpleName());
                                }
                            }
                        } catch (JSONException e) {
                            throw new RuntimeException(e);
                        }
                        return getConfigType() == CONFIGURATION ? m.createConfigurationFragment(jsonPreset) :
                                m.createFinalizationFragment();
                    })
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
        }
        return fragments;
    }

    ConfigurationType getConfigType() {
        return getCurrentEvent().getState() == CREATED ? CONFIGURATION : FINALIZATION;
    }

    private SportEvent getCurrentEvent() {
        if (currentEvent == null) {
            currentEvent = ApplicationState.getInstance().getCurrentEvent();
        }
        return currentEvent;
    }

    void saveConfigurations() {
        getCurrentEvent().getModules().forEach(Module::saveConfiguration);
    }

    Completable savePreset() {
        if (isNewPreset) {
            SportAssistantPersistence persistence = ApplicationState.getInstance().getPersistence();
            getCurrentEvent().getModules().forEach(m -> {
                try {
                    preset.getData().put(m.getClass().getSimpleName(), m.createPreset());
                } catch (JSONException e) {
                    throw new RuntimeException(e);
                }
            });
            return persistence.savePreset(preset);
        } else {
            return Completable.complete();
        }
    }
}
