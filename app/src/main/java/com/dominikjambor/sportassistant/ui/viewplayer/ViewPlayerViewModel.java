package com.dominikjambor.sportassistant.ui.viewplayer;

import androidx.lifecycle.ViewModel;

import com.dominikjambor.sportassistant.data.Player;
import com.dominikjambor.sportassistant.data.Team;
import com.dominikjambor.sportassistant.logic.ApplicationState;
import com.dominikjambor.sportassistant.persistence.SportAssistantPersistence;
import com.dominikjambor.sportassistant.ui.utils.SingleLiveEvent;

import io.reactivex.Single;
import lombok.Getter;

public class ViewPlayerViewModel extends ViewModel {
    @Getter private Player selectedPlayer;
    @Getter private SingleLiveEvent<Void> editPlayerCommand = new SingleLiveEvent<>();

    void setSelectedPlayer(Player selectedPlayer) {
        if (this.selectedPlayer == null) {
            this.selectedPlayer = selectedPlayer;
        }
    }

    Single<Team> getTeam() {
        SportAssistantPersistence persistence =
                ApplicationState.getInstance().getPersistence();
        return persistence.findTeamById(selectedPlayer.getTeamId());
    }

    public void editClicked() {
        editPlayerCommand.call();
    }
}
