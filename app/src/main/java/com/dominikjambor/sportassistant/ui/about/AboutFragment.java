package com.dominikjambor.sportassistant.ui.about;

import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.dominikjambor.sportassistant.BuildConfig;
import com.dominikjambor.sportassistant.R;

import java.util.Locale;

public class AboutFragment extends Fragment {

    boolean showVersionCode = true;
    TextView versionTextView;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_about, container, false);

        ((TextView) view.findViewById(R.id.about_imgcredit1)).setMovementMethod(LinkMovementMethod.getInstance());
        ((TextView) view.findViewById(R.id.about_imgcredit2)).setMovementMethod(LinkMovementMethod.getInstance());
        versionTextView = view.findViewById(R.id.versionText);
        versionTextView.setOnClickListener(c -> updateVersionText());
        updateVersionText();

        return view;
    }

    void updateVersionText() {
        showVersionCode = !showVersionCode;
        String versionText = String.format(Locale.getDefault(), "ver. %s", BuildConfig.VERSION_NAME);
        if (showVersionCode)
            versionText = versionText.concat(String.format(Locale.getDefault(), " (%04d)", BuildConfig.VERSION_CODE));
        versionTextView.setText(versionText);
    }
}
