package com.dominikjambor.sportassistant.ui.players;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.room.EmptyResultSetException;

import com.dominikjambor.sportassistant.R;
import com.dominikjambor.sportassistant.databinding.FragmentPlayersBinding;
import com.dominikjambor.sportassistant.ui.utils.Utils;

import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class PlayersFragment extends Fragment {

    @BindView(R.id.players_playerList) ListView playerListView;
    @BindView(R.id.players_subText) TextView subTextView;
    private CompositeDisposable trash = new CompositeDisposable();
    private PlayersViewModel viewModel;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        FragmentPlayersBinding binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_players,
                container, false);
        binding.setLifecycleOwner(this);
        View root = binding.getRoot();
        ButterKnife.bind(this, root);

        viewModel = new ViewModelProvider(this).get(PlayersViewModel.class);
        if (getArguments() != null) {
            UUID selectedTeamId = PlayersFragmentArgs.fromBundle(getArguments()).getSelectedTeamId();
            viewModel.setSelectedTeamId(selectedTeamId);
        }
        binding.setViewmodel(viewModel);

        trash.add(viewModel.getPlayerListItems()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        items -> {
                            ActionBar actionBar = ((AppCompatActivity) requireActivity()).getSupportActionBar();
                            if (actionBar != null) {
                                actionBar.setTitle(viewModel.getTitle());
                            }
                            playerListView.setAdapter(new PlayerListAdapter(getContext(), items));
                            playerListView.setOnItemClickListener((parent, view, position, id) -> viewModel.selectPlayer(position));
                            if (items.isEmpty()) {
                                subTextView.setText(getString(R.string.no_data_to_show));
                                playerListView.setEmptyView(subTextView);
                            }
                        },
                        e -> {
                            if (e instanceof EmptyResultSetException) {
                                subTextView.setText(getString(R.string.no_data_to_show));
                            } else {
                                subTextView.setText(getString(R.string.error_occurred));
                            }
                            playerListView.setEmptyView(subTextView);
                        }
                ));

        viewModel.getSelectPlayerCommand().observe(getViewLifecycleOwner(), o -> {
            PlayersFragmentDirections.ActionNavPlayersToNavViewplayer action = PlayersFragmentDirections
                    .actionNavPlayersToNavViewplayer(viewModel.getSelectedPlayer());
            Navigation.findNavController(root).navigate(action);
        });

        viewModel.getAddPlayerCommand().observe(getViewLifecycleOwner(), o -> {
            PlayersFragmentDirections.ActionNavPlayersToNavEditplayer action = PlayersFragmentDirections
                    .actionNavPlayersToNavEditplayer(null, viewModel.getSelectedTeamId());
            Navigation.findNavController(root).navigate(action);
        });

        return root;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.action_editteam && viewModel != null) {
            PlayersFragmentDirections.ActionNavPlayersToNavEditteam action = PlayersFragmentDirections
                    .actionNavPlayersToNavEditteam(viewModel.getSelectedTeamWithPlayers());
            Navigation.findNavController(requireView()).navigate(action);
        } else if (item.getItemId() == R.id.action_help) {
            Utils.showSimpleTextDialog(requireContext(), R.string.help_players);
        }
        return false;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        if (viewModel.getSelectedTeamId() != null) {
            inflater.inflate(R.menu.fragment_players, menu);
        }
        inflater.inflate(R.menu.fragments_base, menu);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        trash.clear();
    }
}
