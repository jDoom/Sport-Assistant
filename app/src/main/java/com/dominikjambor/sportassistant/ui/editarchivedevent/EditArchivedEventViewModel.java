package com.dominikjambor.sportassistant.ui.editarchivedevent;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.dominikjambor.sportassistant.R;
import com.dominikjambor.sportassistant.data.ArchivedSportEvent;
import com.dominikjambor.sportassistant.logic.ApplicationState;
import com.dominikjambor.sportassistant.logic.SportEvent;
import com.dominikjambor.sportassistant.persistence.SportAssistantPersistence;
import com.dominikjambor.sportassistant.ui.utils.SingleLiveEvent;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import lombok.Getter;

public class EditArchivedEventViewModel extends AndroidViewModel {
    @Getter private ArchivedSportEvent selectedEvent;
    @Getter private MutableLiveData<String> name = new MutableLiveData<>("");
    @Getter private MutableLiveData<String> description = new MutableLiveData<>("");
    @Getter private MutableLiveData<String> nameError = new MutableLiveData<>("");
    @Getter private MutableLiveData<String> sportType = new MutableLiveData<>("");
    @Getter private SingleLiveEvent<Void> disableFabCommand = new SingleLiveEvent<>();
    @Getter private SingleLiveEvent<Void> errCommand = new SingleLiveEvent<>();
    @Getter private SingleLiveEvent<Void> savedCommand = new SingleLiveEvent<>();
    @Getter private SingleLiveEvent<Void> deletedCommand = new SingleLiveEvent<>();

    private CompositeDisposable trash = new CompositeDisposable();

    public EditArchivedEventViewModel(@NonNull Application application) {
        super(application);
    }

    void setSelectedEvent(ArchivedSportEvent event) {
        if (selectedEvent == null) {

            Context context = getApplication();

            this.selectedEvent = event;
            name.postValue(event.getName());
            description.postValue(event.getDescription());
            sportType.postValue(context.getString(SportEvent.getNameOf(event.getSportType())));
        }
    }

    private boolean checkEventName() {
        Context context = getApplication();
        String value = name.getValue();
        if (value != null) {
            if (value.trim().isEmpty()) {
                nameError.postValue(context.getString(R.string.field_cant_be_empty));
                return false;
            } else if (value.length() >= 30) {
                nameError.postValue(context.getString(R.string.field_too_long));
                return false;
            }
        } else {
            nameError.postValue(context.getString(R.string.unknown_error));
            return false;
        }
        nameError.postValue(null);
        return true;
    }

    public void saveClicked() {
        if (checkEventName()) {
            disableFabCommand.call();

            selectedEvent.setName(name.getValue());
            selectedEvent.setDescription(description.getValue());

            SportAssistantPersistence persistence = ApplicationState.getInstance().getPersistence();
            trash.add(
                    persistence.updateSportEvent(selectedEvent)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(
                                    () -> savedCommand.call(),
                                    e -> errCommand.call())
            );
        } else {
            errCommand.call();
        }
    }

    public void deleteClicked() {
        SportAssistantPersistence persistence = ApplicationState.getInstance().getPersistence();
        trash.add(
                persistence.deleteSportEvent(selectedEvent)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                () -> deletedCommand.call(),
                                e -> errCommand.call())
        );
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        trash.clear();
    }
}
