package com.dominikjambor.sportassistant.ui.configurator;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.viewpager.widget.ViewPager;

import com.dominikjambor.sportassistant.R;
import com.dominikjambor.sportassistant.ui.configurator.ConfiguratorViewModel.ConfigurationType;
import com.dominikjambor.sportassistant.ui.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.dominikjambor.sportassistant.ui.configurator.ConfiguratorViewModel.ConfigurationType.CONFIGURATION;
import static com.dominikjambor.sportassistant.ui.configurator.ConfiguratorViewModel.ConfigurationType.FINALIZATION;

public class ConfiguratorFragment extends Fragment {

    @BindView(R.id.configPrevButton) Button prevButton;
    @BindView(R.id.configNextButton) Button nextButton;
    @BindView(R.id.configNumberText) TextView pageText;
    @BindView(R.id.configViewPager) ViewPager viewPager;
    private ConfigPagerAdapter adapter;
    private ConfigurationType type;

    private CompositeDisposable trash = new CompositeDisposable();

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        ConfiguratorViewModel viewModel = new ViewModelProvider(this).get(ConfiguratorViewModel.class);

        View root = inflater.inflate(R.layout.fragment_configurator, container, false);
        ButterKnife.bind(this, root);

        if (getArguments() != null) {
            ConfiguratorFragmentArgs args = ConfiguratorFragmentArgs.fromBundle(getArguments());
            viewModel.setPreset(args.getSelectedPreset());
            viewModel.setNewPreset(args.getNewPreset());
        }

        adapter = new ConfigPagerAdapter(this.getChildFragmentManager(), viewModel.getFragments()) {
            @Override
            public void pageSelected() {
                updatePrevButton();
                updateNextButton();
                updatePageText();
            }
        };

        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(adapter.getPageSelectedListener());

        type = viewModel.getConfigType();
        prevButton.setEnabled(type == CONFIGURATION);
        prevButton.setText(getString(type == FINALIZATION ? R.string.previous : R.string.cancel));
        prevButton.setOnClickListener(view -> {
            if (viewPager.getCurrentItem() > 0) {
                viewPager.setCurrentItem(viewPager.getCurrentItem() - 1, true);
                updatePageText();
            } else {
                Navigation.findNavController(root).popBackStack();
            }
            updatePrevButton();
            updateNextButton();
        });

        updateNextButton();
        nextButton.setOnClickListener(view -> {
            if (viewPager.getCurrentItem() + 1 < adapter.getCount()) {
                viewPager.setCurrentItem(viewPager.getCurrentItem() + 1, true);
                updatePageText();
            } else if (type == CONFIGURATION) {
                hideKeyboard();
                viewModel.saveConfigurations();
                nextButton.setEnabled(false);
                trash.add(viewModel.savePreset()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                () -> {
                                    if (viewModel.isNewPreset()) {
                                        Toast.makeText(requireContext(), R.string.preset_saved, Toast.LENGTH_SHORT).show();
                                    }
                                    Navigation.findNavController(root).navigate(R.id.action_nav_config_to_nav_event);
                                },
                                e -> {
                                    e.printStackTrace();
                                    nextButton.setEnabled(true);
                                }
                        ));
            } else {
                Navigation.findNavController(root).navigate(R.id.action_nav_config_to_nav_afterevent);
            }
            updateNextButton();
            updatePrevButton();
        });

        updatePageText();

        return root;
    }

    private void updatePrevButton() {
        if (type == FINALIZATION) {
            prevButton.setEnabled(viewPager.getCurrentItem() > 0);
        } else {
            prevButton.setText(getString(viewPager.getCurrentItem() > 0 ? R.string.previous : R.string.cancel));
        }
    }

    private void updateNextButton() {
        nextButton.setText(viewPager.getCurrentItem() + 1 == adapter.getCount() ?
                getText(R.string.done) : getText(R.string.next));
    }

    private void updatePageText() {
        pageText.setText(getString(R.string.config_page_number,
                viewPager.getCurrentItem() + 1,
                adapter.getCount()));
    }

    private void hideKeyboard() {
        Activity currentActivity = getActivity();
        if (currentActivity != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
            View view = getActivity().getCurrentFocus();
            if (view == null) {
                view = new View(getActivity());
            }
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        trash.clear();
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.action_help) {
            Utils.showSimpleTextDialog(requireContext(), R.string.help_configurator);
        }
        return false;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.fragments_base, menu);
    }
}
