package com.dominikjambor.sportassistant.ui.viewplayer;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import com.dominikjambor.sportassistant.R;
import com.dominikjambor.sportassistant.databinding.FragmentViewplayerBinding;
import com.dominikjambor.sportassistant.ui.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class ViewPlayerFragment extends Fragment {

    @BindView(R.id.viewplayer_teamname) TextView teamTextView;
    private CompositeDisposable trash = new CompositeDisposable();

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        FragmentViewplayerBinding binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_viewplayer,
                container, false);
        binding.setLifecycleOwner(this);

        ViewPlayerViewModel viewModel = new ViewModelProvider(this).get(ViewPlayerViewModel.class);
        if (getArguments() != null) {
            viewModel.setSelectedPlayer(
                    ViewPlayerFragmentArgs.fromBundle(getArguments()).getSelectedPlayer()
            );
        }

        binding.setViewmodel(viewModel);
        View root = binding.getRoot();
        ButterKnife.bind(this, root);

        trash.add(viewModel.getTeam()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(team ->
                                teamTextView.setText(String.format("%s %s",
                                        getString(R.string.team),
                                        team.getName())
                                ),
                        e -> teamTextView.setText(String.format("%s %s",
                                getString(R.string.team),
                                getString(R.string.unassigned)))
                ));

        viewModel.getEditPlayerCommand().observe(getViewLifecycleOwner(), o -> {
            ViewPlayerFragmentDirections.ActionNavViewplayerToNavEditplayer action = ViewPlayerFragmentDirections
                    .actionNavViewplayerToNavEditplayer(viewModel.getSelectedPlayer(), null);
            Navigation.findNavController(root).navigate(action);
        });

        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        trash.clear();
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.action_help) {
            Utils.showSimpleTextDialog(requireContext(), R.string.help_viewplayer);
        }
        return false;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.fragments_base, menu);
    }
}
