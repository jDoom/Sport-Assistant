package com.dominikjambor.sportassistant.ui.log;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
class LogListItem {
    private String prefix;
    private String content;
    boolean createdByModule;
}
