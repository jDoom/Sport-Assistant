package com.dominikjambor.sportassistant.ui.archivedevent;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import com.dominikjambor.sportassistant.R;
import com.dominikjambor.sportassistant.databinding.FragmentArchivedeventBinding;
import com.dominikjambor.sportassistant.ui.utils.Utils;

import butterknife.ButterKnife;

public class ArchivedEventFragment extends Fragment {

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        FragmentArchivedeventBinding binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_archivedevent,
                container, false);
        binding.setLifecycleOwner(this);

        ArchivedEventViewModel viewModel = new ViewModelProvider(this).get(ArchivedEventViewModel.class);
        if (getArguments() != null) {
            viewModel.setSelectedEvent(
                    ArchivedEventFragmentArgs.fromBundle(getArguments()).getSelectedEvent()
            );
        }

        binding.setViewmodel(viewModel);
        View root = binding.getRoot();
        ButterKnife.bind(this, root);

        viewModel.getEditEventCommand().observe(getViewLifecycleOwner(), o -> {
            ArchivedEventFragmentDirections.ActionNavArchivedeventToNavEditarchivedevent action = ArchivedEventFragmentDirections
                    .actionNavArchivedeventToNavEditarchivedevent(viewModel.getSelectedEvent());
            Navigation.findNavController(root).navigate(action);
        });
        viewModel.getViewLogCommand().observe(getViewLifecycleOwner(), o -> {
            ArchivedEventFragmentDirections.ActionNavArchivedeventToLogFragment action = ArchivedEventFragmentDirections
                    .actionNavArchivedeventToLogFragment(viewModel.getSelectedEvent());
            Navigation.findNavController(root).navigate(action);
        });

        return root;
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.action_help) {
            Utils.showSimpleTextDialog(requireContext(), R.string.help_archivedevent);
        }
        return false;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.fragments_base, menu);
    }
}
