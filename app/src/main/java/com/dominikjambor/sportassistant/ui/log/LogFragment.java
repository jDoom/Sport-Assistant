package com.dominikjambor.sportassistant.ui.log;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.room.EmptyResultSetException;

import com.dominikjambor.sportassistant.R;
import com.dominikjambor.sportassistant.databinding.FragmentLogBinding;
import com.dominikjambor.sportassistant.ui.archivedevent.ArchivedEventFragmentArgs;
import com.dominikjambor.sportassistant.ui.utils.FixedListView;
import com.dominikjambor.sportassistant.ui.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class LogFragment extends Fragment {
    private CompositeDisposable trash = new CompositeDisposable();
    @BindView(R.id.log_list) FixedListView logList;
    @BindView(R.id.log_subText) TextView subTextView;
    private LogViewModel viewModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        FragmentLogBinding binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_log,
                container, false);
        binding.setLifecycleOwner(this);

        viewModel = new ViewModelProvider(this).get(LogViewModel.class);
        if (getArguments() != null) {
            viewModel.setSelectedEvent(
                    ArchivedEventFragmentArgs.fromBundle(getArguments()).getSelectedEvent()
            );
        }

        trash.add(viewModel.getListItems()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(list -> {
                            LogListAdapter adapter = new LogListAdapter(requireContext(), list);
                            logList.setAdapter(adapter);
                            if (list.isEmpty()) {
                                subTextView.setText(getString(R.string.no_data_to_show));
                                subTextView.setVisibility(View.VISIBLE);
                            }
                        },
                        e -> {
                            if (e instanceof EmptyResultSetException) {
                                subTextView.setText(getString(R.string.no_data_to_show));
                            } else {
                                subTextView.setText(getString(R.string.error_occurred));
                            }
                            subTextView.setVisibility(View.VISIBLE);
                            logList.setEmptyView(subTextView);
                        }));

        binding.setViewmodel(viewModel);
        View root = binding.getRoot();
        ButterKnife.bind(this, root);

        return root;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.action_share_log && viewModel != null) {
            shareClicked();
        } else if (item.getItemId() == R.id.action_help) {
            Utils.showSimpleTextDialog(requireContext(), R.string.help_log);
        }
        return false;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.fragment_log, menu);
        inflater.inflate(R.menu.fragments_base, menu);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        trash.clear();
    }

    private void shareClicked() {
        viewModel.shareClicked(requireActivity());
    }
}
