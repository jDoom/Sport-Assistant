package com.dominikjambor.sportassistant.ui.statistics;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.dominikjambor.sportassistant.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

class ModuleListAdapter extends ArrayAdapter<ModuleListItem> {
    private List<ModuleListItem> moduleList;
    @BindView(R.id.item_statistics_modulelist_name) TextView nameTextView;
    @BindView(R.id.item_statistics_modulelist_playerbutton) Button playerButton;
    @BindView(R.id.item_statistics_modulelist_teambutton) Button teamButton;

    ModuleListAdapter(Context context, List<ModuleListItem> moduleList) {
        super(context, R.layout.item_players_playerlist, moduleList);
        this.moduleList = moduleList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = convertView != null ? convertView :
                ((LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                        .inflate(R.layout.item_statistics_modulelist, parent, false);
        ButterKnife.bind(this, view);

        ModuleListItem item = moduleList.get(position);
        if (item.getName() == null) {
            view.setBackgroundColor(Color.parseColor("#e0e0e0"));
            nameTextView.setText(item.getSportName());
            playerButton.setVisibility(GONE);
            teamButton.setVisibility(GONE);
        } else {
            view.setBackgroundColor(Color.parseColor("#ffffff"));
            nameTextView.setText(item.getName());
            playerButton.setVisibility(item.isPlayerStatAvailable() ? VISIBLE : GONE);
            playerButton.setOnClickListener(v -> onPlayerButtonClicked(item.getModuleClass()));
            teamButton.setVisibility(item.isTeamStatAvailable() ? VISIBLE : GONE);
            teamButton.setOnClickListener(v -> onTeamButtonClicked(item.getModuleClass()));
        }

        return view;
    }

    void onPlayerButtonClicked(Class selectedClass) {
    }

    void onTeamButtonClicked(Class selectedClass) {
    }
}
