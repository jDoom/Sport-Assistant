package com.dominikjambor.sportassistant.ui.log;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.dominikjambor.sportassistant.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

class LogListAdapter extends ArrayAdapter<LogListItem> {
    private List<LogListItem> entryList;
    @BindView(R.id.item_log_entrylist_prefix) TextView prefixTextView;
    @BindView(R.id.item_log_entrylist_content) TextView contentTextView;

    LogListAdapter(Context context, List<LogListItem> entryList) {
        super(context, R.layout.item_log_entrylist, entryList);
        this.entryList = entryList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = convertView != null ? convertView :
                ((LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                        .inflate(R.layout.item_log_entrylist, parent, false);
        ButterKnife.bind(this, view);

        prefixTextView.setText(entryList.get(position).getPrefix());
        contentTextView.setText(entryList.get(position).getContent());

        return view;
    }
}
