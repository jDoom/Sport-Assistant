package com.dominikjambor.sportassistant.ui.event;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import com.dominikjambor.sportassistant.MainActivity;
import com.dominikjambor.sportassistant.R;
import com.dominikjambor.sportassistant.databinding.FragmentEventBinding;
import com.dominikjambor.sportassistant.ui.utils.Utils;
import com.jmedeisis.draglinearlayout.DragLinearLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EventFragment extends Fragment {

    @BindView(R.id.eventWidgetContainer) DragLinearLayout widgetContainer;
    @BindView(R.id.eventEndButton) Button endButton;
    private View root;
    private EventViewModel viewModel;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        FragmentEventBinding binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_event,
                container, false);
        binding.setLifecycleOwner(this);
        root = binding.getRoot();

        ButterKnife.bind(this, root);
        viewModel = new ViewModelProvider(this).get(EventViewModel.class);

        viewModel.getWidgetViews(this).forEach(v -> {
            if (v != null) {
                widgetContainer.addDragView(v, v.findViewById(R.id.widgetDragHandle));
            }
        });
        viewModel.getEndCommand().observe(getViewLifecycleOwner(),
                o -> {
                    setDrawerEnabled(true);
                    Navigation.findNavController(container).navigate(R.id.action_nav_event_to_nav_config);
                });
        viewModel.getSkipToAfterEventCommand().observe(getViewLifecycleOwner(),
                o -> {
                    setDrawerEnabled(true);
                    Navigation.findNavController(container).navigate(R.id.action_nav_event_to_nav_afterevent);
                });
        viewModel.getRefreshCommand().observe(getViewLifecycleOwner(),
                o -> binding.invalidateAll());
        viewModel.getStartCommand().observe(getViewLifecycleOwner(), o -> endButton.setEnabled(true));
        binding.setViewmodel(viewModel);
        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                onBackPressed();
            }
        });
        setDrawerEnabled(false);
        return root;
    }


    private void onBackPressed() {
        if (!viewModel.isEventStarted()) {
            Navigation.findNavController(root).popBackStack();
            setDrawerEnabled(true);
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(requireActivity());
            builder.setMessage(R.string.confirm_cancel_event)
                    .setPositiveButton(R.string.yes, (dialog, id) -> {
                        viewModel.cancel();
                        setDrawerEnabled(true);
                        Navigation.findNavController(root).popBackStack(R.id.nav_home, false);
                    })
                    .setNegativeButton(R.string.cancel, null);
            builder.create().show();
        }
    }

    private void setDrawerEnabled(boolean state) {
        ((MainActivity) requireActivity()).setDrawerEnabled(state);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.action_help) {
            Utils.showSimpleTextDialog(requireContext(), R.string.help_event);
        } else if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return false;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.fragments_base, menu);
    }
}