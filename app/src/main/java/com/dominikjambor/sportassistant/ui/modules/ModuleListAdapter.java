package com.dominikjambor.sportassistant.ui.modules;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.dominikjambor.sportassistant.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

class ModuleListAdapter extends ArrayAdapter<ModuleListItem> {
    private List<ModuleListItem> moduleList;
    @BindView(R.id.item_modulelist_modulelist_name) TextView nameTextView;
    @BindView(R.id.item_modulelist_modulelist_desc) TextView descText;
    @BindView(R.id.item_modulelist_modulelist_sport) TextView sportText;
    @BindView(R.id.item_modulelist_modulelist_playerstats) TextView playerStatText;
    @BindView(R.id.item_modulelist_modulelist_teamstats) TextView teamStatText;

    ModuleListAdapter(Context context, List<ModuleListItem> moduleList) {
        super(context, R.layout.item_modules_modulelist, moduleList);
        this.moduleList = moduleList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = convertView != null ? convertView :
                ((LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                        .inflate(R.layout.item_modules_modulelist, parent, false);
        ButterKnife.bind(this, view);

        ModuleListItem item = moduleList.get(position);
        nameTextView.setText(item.getName());
        descText.setText(item.getDescription());
        sportText.setText(item.getSport());
        playerStatText.setText(String.format("Player Statistics: %sAvailable", !item.isPlayerStat() ? "Not " : ""));
        teamStatText.setText(String.format("Team Statistics: %sAvailable", !item.isTeamStat() ? "Not " : ""));

        return view;
    }
}
