package com.dominikjambor.sportassistant.ui.archive;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.dominikjambor.sportassistant.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

class ArchiveListAdapter extends ArrayAdapter<ArchiveListItem> {
    private List<ArchiveListItem> eventList;
    @BindView(R.id.item_archive_eventlist_name) TextView nameTextView;
    @BindView(R.id.item_archive_eventlist_teams) TextView teamsTextView;
    @BindView(R.id.item_archive_eventlist_start) TextView startTextView;
    @BindView(R.id.item_archive_eventlist_sport) TextView sportTextView;

    ArchiveListAdapter(Context context, List<ArchiveListItem> eventList) {
        super(context, R.layout.item_archive_eventlist, eventList);
        this.eventList = eventList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = convertView != null ? convertView :
                ((LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                        .inflate(R.layout.item_archive_eventlist, parent, false);
        ButterKnife.bind(this, view);

        nameTextView.setText(eventList.get(position).getEventName());
        teamsTextView.setText(eventList.get(position).getTeams());
        startTextView.setText(eventList.get(position).getDateTimeString());
        sportTextView.setText(eventList.get(position).getSportType());

        return view;
    }
}
