package com.dominikjambor.sportassistant.ui.statistics;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import com.dominikjambor.sportassistant.R;
import com.dominikjambor.sportassistant.ui.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class StatisticsFragment extends Fragment {

    @BindView(R.id.statistics_modulelist) ListView moduleList;

    private CompositeDisposable trash = new CompositeDisposable();

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View root = inflater.inflate(R.layout.fragment_statistics, container, false);
        ButterKnife.bind(this, root);

        StatisticsViewModel viewModel = new ViewModelProvider(this).get(StatisticsViewModel.class);

        ModuleListAdapter adapter = new ModuleListAdapter(requireContext(), viewModel.getModuleList()) {
            @Override
            void onPlayerButtonClicked(Class selectedClass) {
                trash.add(viewModel.findAnyPlayers()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(r -> {
                            if (r) viewModel.playerStatSelected(selectedClass);
                            else
                                Toast.makeText(requireContext(), R.string.no_players, Toast.LENGTH_SHORT).show();
                        }, Throwable::printStackTrace));
            }

            @Override
            void onTeamButtonClicked(Class selectedClass) {
                trash.add(viewModel.findAnyTeams()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(r -> {
                            if (r) viewModel.teamStatSelected(selectedClass);
                            else
                                Toast.makeText(requireContext(), R.string.no_teams, Toast.LENGTH_SHORT).show();
                        }, Throwable::printStackTrace));
            }
        };
        moduleList.setAdapter(adapter);

        viewModel.getOpenStatCommand().observe(getViewLifecycleOwner(), o -> {
            StatisticsFragmentDirections.ActionNavStatisticsToNavViewstat action = StatisticsFragmentDirections
                    .actionNavStatisticsToNavViewstat(viewModel.getSelectedModule(), viewModel.getSelectedType());
            Navigation.findNavController(root).navigate(action);
        });

        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        trash.clear();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.action_help) {
            Utils.showSimpleTextDialog(requireContext(), R.string.help_statistics);
        }
        return false;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.fragments_base, menu);
    }
}
