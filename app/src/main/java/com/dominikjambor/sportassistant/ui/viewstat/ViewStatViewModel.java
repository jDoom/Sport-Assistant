package com.dominikjambor.sportassistant.ui.viewstat;

import androidx.lifecycle.ViewModel;

import com.dominikjambor.sportassistant.modules.Module;

import lombok.Getter;
import lombok.SneakyThrows;

public class ViewStatViewModel extends ViewModel {
    private Class<? extends Module> selectedModule;
    @Getter private Module selectedModuleObject;

    @SneakyThrows
    void setSelectedModule(Class<? extends Module> selectedModule) {
        if (this.selectedModule == null) {
            this.selectedModule = selectedModule;
            selectedModuleObject = selectedModule.newInstance();
        }
    }
}
