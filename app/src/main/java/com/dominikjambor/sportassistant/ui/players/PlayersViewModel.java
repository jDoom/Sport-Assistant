package com.dominikjambor.sportassistant.ui.players;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.dominikjambor.sportassistant.R;
import com.dominikjambor.sportassistant.data.Player;
import com.dominikjambor.sportassistant.data.Team;
import com.dominikjambor.sportassistant.data.TeamWithPlayers;
import com.dominikjambor.sportassistant.logic.ApplicationState;
import com.dominikjambor.sportassistant.persistence.SportAssistantPersistence;
import com.dominikjambor.sportassistant.ui.utils.SingleLiveEvent;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import io.reactivex.Single;
import lombok.Getter;

public class PlayersViewModel extends AndroidViewModel {
    @Getter UUID selectedTeamId;
    @Getter private TeamWithPlayers selectedTeamWithPlayers;

    public PlayersViewModel(@NonNull Application application) {
        super(application);
    }

    void setSelectedTeamId(UUID id) {
        selectedTeamId = id;
    }

    Single<List<PlayerListItem>> getPlayerListItems() {
        Context context = getApplication();
        SportAssistantPersistence persistence =
                ApplicationState.getInstance().getPersistence();
        Single<TeamWithPlayers> playersQuery;
        if (selectedTeamId == null) {
            playersQuery = persistence.findPlayersByTeamId(null)
                    .map(players -> new TeamWithPlayers(new Team(new UUID(0, 0), null), players));
        } else {
            playersQuery = persistence.findTeamWithPlayersByTeamId(selectedTeamId);
        }
        return playersQuery.map(teamWithPlayers -> {
            selectedTeamWithPlayers = teamWithPlayers;
            return selectedTeamWithPlayers.players.stream()
                    .map(player -> new PlayerListItem(
                            player.getName(),
                            context.getString(R.string.jersey) + ": " +
                                    player.getJerseyNumber()
                    )).collect(Collectors.toList());
        });
    }

    String getTitle() {
        Context context = getApplication();
        if (selectedTeamId == null || selectedTeamId.equals(new UUID(0, 0))) {
            return context.getString(R.string.unassigned_players);
        } else {
            return selectedTeamWithPlayers.team.getName();
        }
    }

    @Getter private Player selectedPlayer;
    @Getter SingleLiveEvent<Void> selectPlayerCommand = new SingleLiveEvent<>();

    void selectPlayer(int position) {
        selectedPlayer = selectedTeamWithPlayers.players.get(position);
        selectPlayerCommand.call();
    }

    @Getter SingleLiveEvent<Void> addPlayerCommand = new SingleLiveEvent<>();

    public void addClicked() {
        addPlayerCommand.call();
    }
}
