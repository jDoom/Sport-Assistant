package com.dominikjambor.sportassistant.ui.editplayer;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.dominikjambor.sportassistant.R;
import com.dominikjambor.sportassistant.data.Player;
import com.dominikjambor.sportassistant.data.Team;
import com.dominikjambor.sportassistant.logic.ApplicationState;
import com.dominikjambor.sportassistant.persistence.SportAssistantPersistence;
import com.dominikjambor.sportassistant.ui.utils.SingleLiveEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import lombok.Getter;
import lombok.Setter;

public class EditPlayerViewModel extends AndroidViewModel {
    @Getter private Player selectedPlayer;
    @Getter private MutableLiveData<String> playerName = new MutableLiveData<>("");
    @Getter private MutableLiveData<String> playerJersey = new MutableLiveData<>("");
    @Getter private MutableLiveData<String> nameError = new MutableLiveData<>();
    @Getter private int playerTeamPosition = -1;

    @Getter private SingleLiveEvent<Void> disableFabCommand = new SingleLiveEvent<>();
    @Getter private SingleLiveEvent<Void> savedCommand = new SingleLiveEvent<>();
    @Getter private SingleLiveEvent<Void> errCommand = new SingleLiveEvent<>();
    @Getter private SingleLiveEvent<Void> deletedCommand = new SingleLiveEvent<>();

    @Setter private int selectedTeamPosition;
    private Single<List<String>> teamNames;
    private List<Team> teams = new ArrayList<>();

    private CompositeDisposable trash = new CompositeDisposable();

    public EditPlayerViewModel(@NonNull Application application) {
        super(application);
    }

    private boolean newPlayer = false;

    void setParameters(Player player, UUID teamId) {
        if (player == null) {
            player = new Player(UUID.randomUUID(), "", "", teamId);
            newPlayer = true;
        }
        if (selectedPlayer == null) {
            selectedPlayer = player;
            playerName.postValue(player.getName());
            playerJersey.postValue(player.getJerseyNumber());
        }
    }

    Single<List<String>> getTeamNames() {
        if (teamNames == null) {
            Context context = getApplication();
            SportAssistantPersistence persistence =
                    ApplicationState.getInstance().getPersistence();
            teamNames = persistence.findAllTeams()
                    .map(list -> {
                                if (selectedPlayer != null) {
                                    playerTeamPosition = list.stream()
                                            .filter(item -> item.getId().equals(selectedPlayer.getTeamId()))
                                            .findAny().map(list::indexOf)
                                            .orElse(-1);
                                }
                                teams.addAll(list);
                                return list.stream()
                                        .map(Team::getName)
                                        .collect(Collectors.toList());
                            }
                    ).map(strings -> {
                        strings.add(0, context.getString(R.string.unassigned));
                        return strings;
                    }).cache();
        }
        return teamNames;
    }

    private boolean checkEventName() {
        Context context = getApplication();
        String value = playerName.getValue();
        if (value != null) {
            if (value.trim().isEmpty()) {
                nameError.postValue(context.getString(R.string.field_cant_be_empty));
                return false;
            } else if (value.length() >= 30) {
                nameError.postValue(context.getString(R.string.field_too_long));
                return false;
            }
        } else {
            nameError.postValue(context.getString(R.string.unknown_error));
            return false;
        }
        nameError.postValue(null);
        return true;
    }

    public void saveClicked() {
        if (checkEventName()) {
            disableFabCommand.call();

            selectedPlayer.setName(playerName.getValue());
            selectedPlayer.setJerseyNumber(playerJersey.getValue());
            selectedPlayer.setTeamId(selectedTeamPosition <= 0 ?
                    null : teams.get(selectedTeamPosition - 1).getId());

            SportAssistantPersistence persistence = ApplicationState.getInstance().getPersistence();
            Completable action = newPlayer ? persistence.savePlayer(selectedPlayer) :
                    persistence.updatePlayer(selectedPlayer);
            trash.add(
                    action.subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(
                                    () -> savedCommand.call(),
                                    e -> errCommand.call())
            );
        } else {
            errCommand.call();
        }
    }

    void deleteClicked() {
        SportAssistantPersistence persistence = ApplicationState.getInstance().getPersistence();
        trash.add(
                persistence.deletePlayer(selectedPlayer)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                () -> deletedCommand.call(),
                                e -> errCommand.call())
        );
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        trash.clear();
    }
}
