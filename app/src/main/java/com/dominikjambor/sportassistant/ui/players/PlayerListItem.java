package com.dominikjambor.sportassistant.ui.players;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
class PlayerListItem {
    private String name;
    private String description;
}