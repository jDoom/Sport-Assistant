package com.dominikjambor.sportassistant.ui.archivedevent;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.dominikjambor.sportassistant.data.ArchivedSportEvent;
import com.dominikjambor.sportassistant.logic.ApplicationState;
import com.dominikjambor.sportassistant.logic.SportEvent;
import com.dominikjambor.sportassistant.ui.utils.SingleLiveEvent;

import org.json.JSONObject;

import java.time.format.DateTimeFormatter;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import lombok.Getter;

@Getter
public class ArchivedEventViewModel extends AndroidViewModel {
    private ArchivedSportEvent selectedEvent;
    private SingleLiveEvent<Void> editEventCommand = new SingleLiveEvent<>();
    private SingleLiveEvent<Void> viewLogCommand = new SingleLiveEvent<>();
    private MutableLiveData<String> name = new MutableLiveData<>("");
    private MutableLiveData<String> description = new MutableLiveData<>("");
    private MutableLiveData<String> sportType = new MutableLiveData<>("");
    private MutableLiveData<String> startDate = new MutableLiveData<>("");
    private MutableLiveData<String> endDate = new MutableLiveData<>("");
    private MutableLiveData<String> team1 = new MutableLiveData<>("");
    private MutableLiveData<String> team2 = new MutableLiveData<>("");
    private MutableLiveData<String> resultString = new MutableLiveData<>("");

    private CompositeDisposable trash = new CompositeDisposable();

    public ArchivedEventViewModel(@NonNull Application application) {
        super(application);
    }

    void setSelectedEvent(ArchivedSportEvent selectedEvent) {
        Context context = getApplication();

        this.selectedEvent = selectedEvent;
        name.postValue(selectedEvent.getName());
        description.postValue(selectedEvent.getDescription());
        sportType.postValue(
                context.getString(SportEvent.getNameOf(selectedEvent.getSportType())));
        startDate.postValue(selectedEvent.getStartDateTime().format(DateTimeFormatter.ofPattern("YYYY.MM.dd HH:mm")));
        endDate.postValue(selectedEvent.getEndDateTime().format(DateTimeFormatter.ofPattern("YYYY.MM.dd HH:mm")));
        team1.postValue(selectedEvent.getTeam1());
        team2.postValue(selectedEvent.getTeam2());

        trash.add(ApplicationState.getInstance().getPersistence()
                .findLogEntriesByEventId(selectedEvent.getId())
                .subscribeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(l -> {
                    StringBuilder sb = new StringBuilder();
                    l.forEach(entry -> {
                        try {
                            JSONObject object = new JSONObject(entry.getData());
                            if (object.has("resultText")) {
                                sb.append(object.getString("resultText")).append("\n");
                            }
                        } catch (Exception ignored) {
                        }
                    });
                    resultString.postValue(sb.toString());
                }));
    }

    public void editClicked() {
        editEventCommand.call();
    }

    public void viewLogClicked() {
        viewLogCommand.call();
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        trash.clear();
    }
}
