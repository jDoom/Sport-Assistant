package com.dominikjambor.sportassistant.ui.viewstat;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;

import com.dominikjambor.sportassistant.R;
import com.dominikjambor.sportassistant.ui.utils.Utils;

import static com.dominikjambor.sportassistant.ui.viewstat.ViewStatFragment.StatType.*;

public class ViewStatFragment extends Fragment {

    private ViewStatViewModel viewModel;
    private StatType type;

    public enum StatType {PLAYER, TEAM}

    @SuppressWarnings("unchecked")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        viewModel = new ViewModelProvider(this).get(ViewStatViewModel.class);
        if (getArguments() != null) {
            viewModel.setSelectedModule(
                    ViewStatFragmentArgs.fromBundle(getArguments()).getSelectedModule());
            type = ViewStatFragmentArgs.fromBundle(getArguments()).getStatType();
        }
        return inflater.inflate(R.layout.fragment_statcontainer, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        Fragment childFragment = type == PLAYER ?
                viewModel.getSelectedModuleObject().createPlayerStatFragment() :
                viewModel.getSelectedModuleObject().createTeamStatFragment();

        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(R.id.statcontainer_container, childFragment).commit();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.action_help) {
            Utils.showSimpleTextDialog(requireContext(), R.string.help_viewstat);
        }
        return false;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.fragments_base, menu);
    }
}
