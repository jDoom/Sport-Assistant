package com.dominikjambor.sportassistant.ui.teams;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.dominikjambor.sportassistant.R;
import com.dominikjambor.sportassistant.data.TeamWithPlayers;
import com.dominikjambor.sportassistant.logic.ApplicationState;
import com.dominikjambor.sportassistant.persistence.SportAssistantPersistence;
import com.dominikjambor.sportassistant.ui.utils.SingleLiveEvent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import io.reactivex.Single;
import lombok.Getter;

public class TeamsViewModel extends AndroidViewModel {

    private Single<List<TeamListItem>> teamListItems;
    private List<TeamWithPlayers> teamsWithPlayers;

    public TeamsViewModel(@NonNull Application application) {
        super(application);
    }

    Single<List<TeamListItem>> getTeamListItems() {
        if (teamListItems == null) {
            Context context = getApplication();
            SportAssistantPersistence persistence = ApplicationState.getInstance().getPersistence();
            teamListItems = persistence.findAllTeamsWithPlayers()
                    .map(list -> {
                                teamsWithPlayers = list;
                                return list.stream()
                                        .map(item -> new TeamListItem(
                                                item.team.getName(),
                                                item.players.size() + " " + context.getString(R.string.players)))
                                        .collect(Collectors.toList());
                            }
                    ).concatWith(
                            persistence.findPlayersByTeamId(null)
                                    .map(players ->
                                            Collections.singletonList(new TeamListItem(context.getString(R.string.noteam),
                                                    players.size() + " " + context.getString(R.string.players)))
                                    )
                    ).collect(ArrayList::new, List::addAll);
        }
        return teamListItems;
    }

    @Getter private UUID selectedTeamId;
    @Getter private SingleLiveEvent<Void> openTeamCommand = new SingleLiveEvent<>();

    void selectItem(int position) {
        if (position == teamsWithPlayers.size()) {
            selectedTeamId = null;
        } else {
            selectedTeamId = teamsWithPlayers.get(position).team.getId();
        }
        openTeamCommand.call();
    }

    @Getter SingleLiveEvent<Void> addTeamCommand = new SingleLiveEvent<>();

    public void addClicked() {
        addTeamCommand.call();
    }
}
