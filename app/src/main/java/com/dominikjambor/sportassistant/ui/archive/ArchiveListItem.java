package com.dominikjambor.sportassistant.ui.archive;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
class ArchiveListItem {
    private String eventName;
    private String dateTimeString;
    private String teams;
    private String sportType;
}
