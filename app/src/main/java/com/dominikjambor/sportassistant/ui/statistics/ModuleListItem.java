package com.dominikjambor.sportassistant.ui.statistics;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
class ModuleListItem {
    private String name;
    private String sportName;
    private Class moduleClass;
    private boolean teamStatAvailable;
    private boolean playerStatAvailable;
}
