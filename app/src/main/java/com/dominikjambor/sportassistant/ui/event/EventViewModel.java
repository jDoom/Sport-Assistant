package com.dominikjambor.sportassistant.ui.event;

import android.app.Application;
import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.dominikjambor.sportassistant.R;
import com.dominikjambor.sportassistant.logic.ApplicationState;
import com.dominikjambor.sportassistant.logic.SportEvent;
import com.dominikjambor.sportassistant.ui.utils.SingleLiveEvent;

import java.util.List;
import java.util.stream.Collectors;

import lombok.Getter;

import static com.dominikjambor.sportassistant.logic.SportEvent.EventState.CREATED;
import static com.dominikjambor.sportassistant.logic.SportEvent.EventState.STARTED;

@Getter
public class EventViewModel extends AndroidViewModel {

    private SportEvent currentEvent = ApplicationState.getInstance().getCurrentEvent();
    private SingleLiveEvent<Void> endCommand = new SingleLiveEvent<>();
    private SingleLiveEvent<Void> refreshCommand = new SingleLiveEvent<>();
    private SingleLiveEvent<Void> startCommand = new SingleLiveEvent<>();
    private SingleLiveEvent<Void> skipToAfterEventCommand = new SingleLiveEvent<>();
    private MutableLiveData<String> startButtonText = new MutableLiveData<>(
            getApplication().getString(R.string.start_match)
    );

    public EventViewModel(@NonNull Application application) {
        super(application);
    }

    public void pauseStartClicked() {
        switch (currentEvent.getState()) {
            case CREATED:
                currentEvent.start();
                startCommand.call();
                break;
            case STARTED:
                currentEvent.pause();
                break;
            case PAUSED:
                currentEvent.resume();
                break;
        }
        Context context = getApplication();
        startButtonText.postValue(currentEvent.getState() == STARTED ?
                context.getString(R.string.pause_match) :
                context.getString(R.string.resume_match)
        );
    }

    List<View> getWidgetViews(Fragment container) {
        return currentEvent.getModules().stream()
                .map(module -> module.createWidgetView(container))
                .collect(Collectors.toList());
    }

    void cancel() {
        currentEvent.end();
    }

    boolean isEventStarted() {
        return !(currentEvent.getState() == CREATED);
    }

    public void endClicked() {
        currentEvent.end();
        if (currentEvent.getModules().stream()
                .anyMatch(m -> m.getConfiguration().hasFinalFragment())) {
            endCommand.call();
        } else {
            skipToAfterEventCommand.call();
        }
    }
}
