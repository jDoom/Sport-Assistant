package com.dominikjambor.sportassistant.ui.newevent;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.dominikjambor.sportassistant.R;
import com.dominikjambor.sportassistant.logic.SportEvent;
import com.dominikjambor.sportassistant.logic.SportEvent.SportType;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.dominikjambor.sportassistant.logic.SportEvent.SportType.GENERIC;

class ModuleListAdapter extends ArrayAdapter<ModuleListItem> {

    private List<ModuleListItem> itemList;
    private SportType selectedSportType;

    @BindView(R.id.item_newevent_modulelist_name) TextView nameTextView;
    @BindView(R.id.item_newevent_modulelist_selected) CheckBox selectedCheckBox;
    @BindView(R.id.item_newevent_modulelist_typeicon) ImageView expandImage;
    @BindView(R.id.item_newevent_modulelist_separator) View separator;

    ModuleListAdapter(Context context, List<ModuleListItem> itemList, SportType selectedSportType) {
        super(context, R.layout.item_newevent_modulelist, itemList);
        this.itemList = itemList;
        this.selectedSportType = selectedSportType;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = convertView != null ? convertView :
                ((LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                        .inflate(R.layout.item_newevent_modulelist, parent, false);
        ButterKnife.bind(this, view);

        if (position == 0) {
            expandImage.setVisibility(View.VISIBLE);
            separator.setVisibility(View.VISIBLE);
            nameTextView.setText(
                    getContext().getString(SportEvent.getNameOf(GENERIC)));
            selectedCheckBox.setVisibility(View.GONE);
        } else if (itemList.get(position) == null) {
            expandImage.setVisibility(View.VISIBLE);
            separator.setVisibility(View.VISIBLE);
            nameTextView.setText(SportEvent.getNameOf(selectedSportType));
            selectedCheckBox.setVisibility(View.GONE);
        } else {
            expandImage.setVisibility(View.GONE);
            separator.setVisibility(View.GONE);
            nameTextView.setText(itemList.get(position).getName());
            selectedCheckBox.setVisibility(View.VISIBLE);
            selectedCheckBox.setChecked(itemList.get(position).isSelected());
        }
        view.setOnClickListener(item -> {
            ButterKnife.bind(this, item);
            if (itemList.get(position) == null) {
                return;
            }
            selectedCheckBox.setChecked(!selectedCheckBox.isChecked());
            nameTextView.setTypeface(null, selectedCheckBox.isChecked() ? Typeface.BOLD : Typeface.NORMAL);
            itemList.get(position).setSelected(selectedCheckBox.isChecked());
        });

        return view;
    }
}
