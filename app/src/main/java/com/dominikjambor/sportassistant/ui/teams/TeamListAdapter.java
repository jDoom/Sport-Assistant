package com.dominikjambor.sportassistant.ui.teams;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.dominikjambor.sportassistant.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

class TeamListAdapter extends ArrayAdapter<TeamListItem> {
    private List<TeamListItem> teamList;
    @BindView(R.id.item_teams_teamlist_name) TextView nameTextView;
    @BindView(R.id.item_teams_teamlist_description) TextView descriptionTextView;

    TeamListAdapter(Context context, List<TeamListItem> teamList) {
        super(context, R.layout.item_newevent_modulelist, teamList);
        this.teamList = teamList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = convertView != null ? convertView :
                ((LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                        .inflate(R.layout.item_teams_teamlist, parent, false);
        ButterKnife.bind(this, view);

        nameTextView.setText(teamList.get(position).getName());
        descriptionTextView.setText(teamList.get(position).getDescription());

        return view;
    }
}
