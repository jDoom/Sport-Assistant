package com.dominikjambor.sportassistant.ui.log;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.FileProvider;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.dominikjambor.sportassistant.data.ArchivedLogEntry;
import com.dominikjambor.sportassistant.data.ArchivedSportEvent;
import com.dominikjambor.sportassistant.data.LogEntry;
import com.dominikjambor.sportassistant.logic.ApplicationState;
import com.dominikjambor.sportassistant.logic.SportEvent;
import com.dominikjambor.sportassistant.persistence.SportAssistantPersistence;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import lombok.Getter;

import static com.dominikjambor.sportassistant.BuildConfig.APPLICATION_ID;

public class LogViewModel extends AndroidViewModel {
    @Getter private ArchivedSportEvent selectedEvent;
    @Getter private MutableLiveData<String> name = new MutableLiveData<>("");
    @Getter private MutableLiveData<String> description = new MutableLiveData<>("");
    @Getter private MutableLiveData<String> sportType = new MutableLiveData<>("");
    @Getter private MutableLiveData<String> startDate = new MutableLiveData<>("");
    @Getter private MutableLiveData<String> endDate = new MutableLiveData<>("");
    @Getter private MutableLiveData<String> team1 = new MutableLiveData<>("");
    @Getter private MutableLiveData<String> team2 = new MutableLiveData<>("");
    @Getter private MutableLiveData<String> resultString = new MutableLiveData<>("");

    private CompositeDisposable trash = new CompositeDisposable();

    private List<ArchivedLogEntry> entries;
    private Single<List<LogListItem>> listItems;

    public LogViewModel(@NonNull Application application) {
        super(application);
    }

    void setSelectedEvent(ArchivedSportEvent selectedEvent) {
        Context context = getApplication();

        this.selectedEvent = selectedEvent;
        name.postValue(selectedEvent.getName());
        description.postValue(selectedEvent.getDescription());
        sportType.postValue(
                context.getString(SportEvent.getNameOf(selectedEvent.getSportType())));
        startDate.postValue(selectedEvent.getStartDateTime().format(DateTimeFormatter.ofPattern("YYYY.MM.dd HH:mm")));
        endDate.postValue(selectedEvent.getEndDateTime().format(DateTimeFormatter.ofPattern("YYYY.MM.dd HH:mm")));
        team1.postValue(selectedEvent.getTeam1());
        team2.postValue(selectedEvent.getTeam2());
        trash.add(ApplicationState.getInstance().getPersistence()
                .findLogEntriesByEventId(selectedEvent.getId())
                .subscribeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(l -> {
                    StringBuilder sb = new StringBuilder();
                    l.forEach(entry -> {
                        try {
                            JSONObject object = new JSONObject(entry.getData());
                            if (object.has("resultText")) {
                                sb.append(object.getString("resultText")).append("\n");
                            }
                        } catch (Exception ignored) {
                        }
                    });
                    resultString.postValue(sb.toString());
                }));
    }

    Single<List<LogListItem>> getListItems() {
        if (listItems == null) {
            SportAssistantPersistence persistence = ApplicationState.getInstance().getPersistence();
            listItems = persistence.findLogEntriesByEventId(selectedEvent.getId())
                    .map(list -> {
                        entries = list;
                        return list.stream()
                                .filter(i -> !i.getText().isEmpty())
                                .map(i -> new LogListItem(
                                        i.getMillisSinceStart() >= 0 ? String.valueOf(i.getMillisSinceStart()) : "",
                                        i.getText(),
                                        false
                                )).collect(Collectors.toList());
                    }).cache();
        }
        return listItems;
    }

    void shareClicked(Activity activity) {
        if (entries == null) return;
        saveAndSharePdf(getPdfLog(), activity);
    }

    private PdfDocument getPdfLog() {
        PdfDocument document = new PdfDocument();

        PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(595, 842, 1).create();

        PdfDocument.Page page = document.startPage(pageInfo);
        Canvas canvas = page.getCanvas();
        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setTextSize(26);
        canvas.drawText(selectedEvent.getName(), 12, 45, paint);

        paint.setTextSize(18);
        paint.setColor(Color.rgb(130, 130, 130));
        canvas.drawText(getApplication().getString(SportEvent.getNameOf(selectedEvent.getSportType())) +
                " match log", 12, 70, paint);

        paint.setTextSize(14);
        paint.setColor(Color.rgb(40, 40, 40));
        canvas.drawText("Start:", 12, 108, paint);
        canvas.drawText("End:", 12, 130, paint);
        canvas.drawText(selectedEvent.getStartDateTime().format(DateTimeFormatter.ofPattern("YYYY/MM/dd hh:mm")), 60, 108, paint);
        canvas.drawText(selectedEvent.getEndDateTime().format(DateTimeFormatter.ofPattern("YYYY/MM/dd hh:mm")), 60, 130, paint);

        if (selectedEvent.getDescription() != null && !selectedEvent.getDescription().isEmpty()) {
            canvas.drawText("Description:", 300, 70, paint);

            String desc = selectedEvent.getDescription();
            desc = addLineBreaks(desc, 50);

            paint.setTextSize(12);
            int x = 300, y = 90;
            for (String line : desc.split("\n")) {
                canvas.drawText(line, x, y, paint);
                y += 15;
            }
            paint.setTextSize(14);
        }

        canvas.drawText("Teams:", 12, 158, paint);
        canvas.drawText(selectedEvent.getTeam1(), 78, 158, paint);
        canvas.drawText(selectedEvent.getTeam2(), 78, 178, paint);

        paint.setColor(Color.rgb(130, 130, 130));
        paint.setStrokeWidth(1);
        canvas.drawLine(12, 188, 583, 188, paint);
        paint.setColor(Color.rgb(70, 70, 70));

        paint.setTextSize(12);
        List<ArchivedLogEntry> filteredEntries = entries.stream()
                .filter(e -> !e.getText().isEmpty()).collect(Collectors.toList());
        entries.stream().filter(e -> {
            try {
                JSONObject object = new JSONObject(e.getData());
                return object.has("resultText");
            } catch (Exception ignored) {
                return false;
            }
        }).forEach(e -> {
            try {
                JSONObject object = new JSONObject(e.getData());
                filteredEntries.add(new ArchivedLogEntry(new LogEntry(
                        object.getString("resultText"), "", "", null, -1),
                        null
                ));
            } catch (JSONException ignored) {
            }
        });
        int count = 0;
        int pos;
        for (pos = 0; pos < filteredEntries.size() && count < 37; pos++) {
            ArchivedLogEntry e = filteredEntries.get(pos);
            canvas.drawText(e.getText(), 12, 218 + pos * 16, paint);
            count++;
        }

        paint.setTextAlign(Paint.Align.RIGHT);
        paint.setColor(Color.rgb(30, 30, 30));
        canvas.drawText(selectedEvent.getName() + " - event log - page 1", 582, 820, paint);
        paint.setColor(Color.rgb(70, 70, 70));
        paint.setTextAlign(Paint.Align.LEFT);

        int pageNumber = 1;
        count = 49;
        for (; pos < filteredEntries.size(); pos++) {
            if (count % 49 == 0) {
                document.finishPage(page);
                pageNumber++;
                count = 0;
                pageInfo = new PdfDocument.PageInfo.Builder(595, 842, pageNumber).create();
                page = document.startPage(pageInfo);
                canvas = page.getCanvas();

                paint.setTextAlign(Paint.Align.RIGHT);
                paint.setColor(Color.rgb(30, 30, 30));
                canvas.drawText(selectedEvent.getName() + " - event log - page " + pageNumber, 582, 820, paint);
                paint.setColor(Color.rgb(70, 70, 70));
                paint.setTextAlign(Paint.Align.LEFT);
            }
            ArchivedLogEntry e = filteredEntries.get(pos);
            canvas.drawText(e.getText(), 12, 30 + count * 16, paint);
            count++;
        }

        document.finishPage(page);

        return document;
    }

    private void saveAndSharePdf(PdfDocument document, Activity activity) {
        String directory_path = getApplication().getFilesDir().getPath() + "/";
        File file = new File(directory_path);
        if (!file.exists()) {
            file.mkdirs();
        }
        String targetPdf = directory_path + "match.pdf";
        File filePath = new File(targetPdf);
        try {
            document.writeTo(new FileOutputStream(filePath));
            sharePdf(filePath, activity);
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(getApplication(), "Couldn't create pdf: " + e.toString(), Toast.LENGTH_LONG).show();
        }
        document.close();
    }

    private void sharePdf(File path, Activity activity) {
        if (path.exists()) {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            Uri uri = FileProvider.getUriForFile(getApplication(), APPLICATION_ID, path);
            intent.setDataAndType(uri, "application/pdf");
            intent.putExtra(Intent.EXTRA_STREAM, uri);

            PackageManager pm = activity.getPackageManager();
            if (intent.resolveActivity(pm) != null) {
                activity.startActivity(intent);
            }
        }
    }

    private static String addLineBreaks(String input, int maxLineLength) {
        StringTokenizer tok = new StringTokenizer(input, " ");
        StringBuilder output = new StringBuilder(input.length());
        int lineLen = 0;
        while (tok.hasMoreTokens()) {
            String word = tok.nextToken() + " ";

            if (lineLen + word.length() > maxLineLength) {
                output.append("\n");
                lineLen = 0;
            }
            output.append(word);
            lineLen += word.length();
        }
        return output.toString();
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        trash.clear();
    }
}
