package com.dominikjambor.sportassistant.ui.modules;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.dominikjambor.sportassistant.logic.SportEvent;
import com.dominikjambor.sportassistant.modules.ModuleConfiguration;
import com.dominikjambor.sportassistant.modules.ModuleManager;

import java.util.List;
import java.util.stream.Collectors;

public class ModulesViewModel extends AndroidViewModel {
    private List<ModuleListItem> moduleList;

    public ModulesViewModel(@NonNull Application application) {
        super(application);
    }

    List<ModuleListItem> getModuleList() {
        if (moduleList == null) {
            moduleList = ModuleManager.MODULES.stream()
                    .map(m -> {
                        ModuleConfiguration config = ModuleManager.getInstance().getConfigurationFor(m);
                        return new ModuleListItem(
                                config.getModuleName(),
                                config.getShortDescription(),
                                getApplication().getString(SportEvent.getNameOf(config.getSportType())),
                                config.hasPlayerStats(),
                                config.hasTeamStats()
                        );
                    }).collect(Collectors.toList());
        }
        return moduleList;
    }
}
