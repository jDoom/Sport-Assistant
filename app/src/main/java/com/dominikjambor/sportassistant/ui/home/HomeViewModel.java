package com.dominikjambor.sportassistant.ui.home;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.dominikjambor.sportassistant.R;
import com.dominikjambor.sportassistant.logic.ApplicationState;
import com.dominikjambor.sportassistant.logic.SportEvent;
import com.dominikjambor.sportassistant.logic.SportEvent.SportType;
import com.dominikjambor.sportassistant.persistence.SportAssistantPersistence;
import com.dominikjambor.sportassistant.ui.utils.SingleLiveEvent;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import lombok.Getter;

import static com.dominikjambor.sportassistant.logic.SportEvent.SportType.BASKETBALL;
import static com.dominikjambor.sportassistant.logic.SportEvent.SportType.SOCCER;
import static com.dominikjambor.sportassistant.logic.SportEvent.SportType.ULTIMATE;
import static com.dominikjambor.sportassistant.logic.SportEvent.SportType.VOLLEYBALL;

public class HomeViewModel extends AndroidViewModel {

    @Getter private MutableLiveData<String> bbCount = new MutableLiveData<>(archiveLine(0, BASKETBALL));
    @Getter private MutableLiveData<String> scCount = new MutableLiveData<>(archiveLine(0, SOCCER));
    @Getter private MutableLiveData<String> ufCount = new MutableLiveData<>(archiveLine(0, ULTIMATE));
    @Getter private MutableLiveData<String> vbCount = new MutableLiveData<>(archiveLine(0, VOLLEYBALL));
    private CompositeDisposable trash = new CompositeDisposable();

    public HomeViewModel(@NonNull Application application) {
        super(application);
    }

    void refresh() {
        SportAssistantPersistence persistence = ApplicationState.getInstance().getPersistence();
        trash.add(persistence.findAllSportEvents()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(events -> {
                    bbCount.postValue(archiveLine(events.stream().filter(event -> event.getSportType() == BASKETBALL).count(),
                            BASKETBALL));
                    scCount.postValue(archiveLine(events.stream().filter(event -> event.getSportType() == SOCCER).count(),
                            SOCCER));
                    ufCount.postValue(archiveLine(events.stream().filter(event -> event.getSportType() == ULTIMATE).count(),
                            ULTIMATE));
                    vbCount.postValue(archiveLine(events.stream().filter(event -> event.getSportType() == VOLLEYBALL).count(),
                            VOLLEYBALL));
                }));
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        trash.clear();
    }

    private String archiveLine(long count, SportType type) {
        return count + " " + getApplication().getString(SportEvent.getNameOf(type)) + " " +
                getApplication().getString(R.string.home_events);
    }

    @Getter private int sportPos = 0;
    @Getter private SingleLiveEvent<Void> newEventCommand = new SingleLiveEvent<>();

    public void sportClicked(int pos) {
        sportPos = pos;
        newEventCommand.call();
    }
}