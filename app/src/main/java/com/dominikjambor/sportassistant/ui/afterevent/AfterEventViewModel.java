package com.dominikjambor.sportassistant.ui.afterevent;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.dominikjambor.sportassistant.data.ArchivedLogEntry;
import com.dominikjambor.sportassistant.data.ArchivedSportEvent;
import com.dominikjambor.sportassistant.logic.ApplicationState;
import com.dominikjambor.sportassistant.logic.SportEvent;
import com.dominikjambor.sportassistant.persistence.SportAssistantPersistence;
import com.dominikjambor.sportassistant.ui.utils.SingleLiveEvent;

import org.json.JSONObject;

import java.util.UUID;
import java.util.stream.Collectors;

import io.reactivex.Completable;
import lombok.Getter;
import lombok.Setter;

import static com.dominikjambor.sportassistant.logic.SportEvent.EventState.ENDED;

public class AfterEventViewModel extends AndroidViewModel {
    private String resultText = null;
    @Getter private SingleLiveEvent<Void> doneCommand = new SingleLiveEvent<>();
    @Getter private SingleLiveEvent<Void> archiveCommand = new SingleLiveEvent<>();

    public AfterEventViewModel(@NonNull Application application) {
        super(application);
    }

    public String getResultText() {

        SportEvent currentEvent = ApplicationState.getInstance().getCurrentEvent();
        if (resultText == null) {
            StringBuilder sb = new StringBuilder();
            currentEvent.getLog().forEach(e -> {
                try {
                    JSONObject o = new JSONObject(e.getData());
                    if (o.has("resultText")) {
                        sb.append(o.getString("resultText")).append("\n");
                    }
                } catch (Exception ignored) {
                }
            });
            resultText = sb.toString();
        }
        return resultText;
    }

    @Setter boolean saved = false;

    Completable saveEvent() {
        if (saved) return Completable.complete();
        ApplicationState state = ApplicationState.getInstance();
        SportAssistantPersistence persistence = state.getPersistence();
        SportEvent currentEvent = state.getCurrentEvent();
        if (currentEvent.getState() != ENDED) currentEvent.end();

        UUID eventId = UUID.randomUUID();

        return state.getPersistence().saveSportEvent(
                new ArchivedSportEvent(eventId, currentEvent.getName(),
                        currentEvent.getDescription(), currentEvent.getStartDateTime(),
                        currentEvent.getEndDateTime(), currentEvent.getSport(),
                        currentEvent.getTeam1().team.getName(), currentEvent.getTeam2().team.getName(),
                        currentEvent.getTeam1().team.getId(), currentEvent.getTeam2().team.getId())
        ).concatWith(persistence.saveLogEntries(currentEvent.getLog().stream()
                .map(l -> new ArchivedLogEntry(l, eventId))
                .collect(Collectors.toList()))
        );
    }

    public void doneClicked() {
        doneCommand.call();
    }

    public void archiveClicked() {
        archiveCommand.call();
    }
}
