package com.dominikjambor.sportassistant.ui.statistics;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.dominikjambor.sportassistant.logic.ApplicationState;
import com.dominikjambor.sportassistant.logic.SportEvent;
import com.dominikjambor.sportassistant.modules.ModuleConfiguration;
import com.dominikjambor.sportassistant.modules.ModuleManager;
import com.dominikjambor.sportassistant.persistence.SportAssistantPersistence;
import com.dominikjambor.sportassistant.ui.utils.SingleLiveEvent;
import com.dominikjambor.sportassistant.ui.viewstat.ViewStatFragment.StatType;

import java.util.List;
import java.util.stream.Collectors;

import io.reactivex.Single;
import lombok.Getter;

import static com.dominikjambor.sportassistant.ui.viewstat.ViewStatFragment.StatType.PLAYER;
import static com.dominikjambor.sportassistant.ui.viewstat.ViewStatFragment.StatType.TEAM;

public class StatisticsViewModel extends AndroidViewModel {
    private List<ModuleListItem> moduleList;

    public StatisticsViewModel(@NonNull Application application) {
        super(application);
    }

    Single<Boolean> findAnyPlayers() {
        SportAssistantPersistence persistence = ApplicationState.getInstance().getPersistence();
        return persistence.findAllPlayers().map(list -> !list.isEmpty()).cache();
    }

    Single<Boolean> findAnyTeams() {
        SportAssistantPersistence persistence = ApplicationState.getInstance().getPersistence();
        return persistence.findAllTeams().map(list -> !list.isEmpty()).cache();
    }

    List<ModuleListItem> getModuleList() {
        if (moduleList == null) {
            Context context = getApplication();
            ModuleManager manager = ModuleManager.getInstance();
            moduleList = ModuleManager.MODULES.stream()
                    .map(m -> {
                        ModuleConfiguration config = manager.getConfigurationFor(m);
                        return new ModuleListItem(
                                config.getModuleName(),
                                context.getString(SportEvent.getNameOf(config.getSportType())),
                                m,
                                config.hasTeamStats(),
                                config.hasPlayerStats()
                        );
                    }).filter(i -> i.isPlayerStatAvailable() || i.isTeamStatAvailable())
                    .collect(Collectors.toList());
            String prevName = "";
            for (int i = 0; i < moduleList.size(); i++) {
                if (!moduleList.get(i).getSportName().equals(prevName)) {
                    prevName = moduleList.get(i).getSportName();
                    moduleList.add(i, new ModuleListItem(null, moduleList.get(i).getSportName(),
                            null, false, false));
                    i++;
                }
            }
        }
        return moduleList;
    }

    @Getter private Class selectedModule;
    @Getter private StatType selectedType;

    @Getter SingleLiveEvent<Void> openStatCommand = new SingleLiveEvent<>();

    void teamStatSelected(Class moduleClass) {
        selectedModule = moduleClass;
        selectedType = TEAM;
        openStatCommand.call();
    }

    void playerStatSelected(Class moduleClass) {
        selectedModule = moduleClass;
        selectedType = PLAYER;
        openStatCommand.call();
    }
}
