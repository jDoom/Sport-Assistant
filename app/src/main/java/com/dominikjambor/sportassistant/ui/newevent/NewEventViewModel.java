package com.dominikjambor.sportassistant.ui.newevent;

import static com.dominikjambor.sportassistant.logic.SportEvent.SportType.GENERIC;
import static java.util.Objects.requireNonNull;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.dominikjambor.sportassistant.R;
import com.dominikjambor.sportassistant.data.Preset;
import com.dominikjambor.sportassistant.data.TeamWithPlayers;
import com.dominikjambor.sportassistant.logic.ApplicationState;
import com.dominikjambor.sportassistant.logic.SportEvent;
import com.dominikjambor.sportassistant.logic.SportEvent.SportType;
import com.dominikjambor.sportassistant.modules.Module;
import com.dominikjambor.sportassistant.modules.ModuleManager;
import com.dominikjambor.sportassistant.persistence.SportAssistantPersistence;
import com.dominikjambor.sportassistant.ui.utils.SingleLiveEvent;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import io.reactivex.Completable;
import io.reactivex.Single;
import lombok.Getter;
import lombok.SneakyThrows;

public class NewEventViewModel extends AndroidViewModel {
    private List<ModuleListItem> moduleListItems;

    @Getter
    private SportType selectedSportType = GENERIC;

    public NewEventViewModel(@NonNull Application application) {
        super(application);
    }

    List<ModuleListItem> getModuleListItems() {
        if (moduleListItems == null) {
            updateModuleList();
        }
        return moduleListItems;
    }

    private void updateModuleList() {
        ModuleManager manager = ModuleManager.getInstance();
        moduleListItems = ModuleManager.MODULES
                .stream()
                .filter(m -> manager.getConfigurationFor(m).getSportType() == GENERIC)
                .map(m -> new ModuleListItem(
                        manager.getConfigurationFor(m)
                                .getModuleName(),
                        false, m
                ))
                .collect(Collectors.toList());
        moduleListItems.add(0, null);
        moduleListItems.add(null);
        moduleListItems.addAll(ModuleManager.MODULES
                .stream()
                .filter(m -> manager.getConfigurationFor(m).getSportType() == selectedSportType)
                .map(m -> new ModuleListItem(
                        manager.getConfigurationFor(m)
                                .getModuleName(),
                        false, m
                ))
                .collect(Collectors.toList())
        );
    }

    List<String> getSportNameList() {
        Context context = getApplication();
        return SportEvent.NAMES.values().stream().skip(1)
                .map(context::getString).sorted()
                .collect(Collectors.toList());
    }

    private boolean invalidText(MutableLiveData<String> field, MutableLiveData<String> errorField) {
        Context context = getApplication();
        String value = field.getValue();
        if (value != null) {
            if (value.trim().isEmpty()) {
                errorField.postValue(context.getString(R.string.field_cant_be_empty));
                return true;
            } else if (value.length() >= 30) {
                errorField.postValue(context.getString(R.string.field_too_long));
                return true;
            }
        } else {
            errorField.postValue(context.getString(R.string.unknown_error));
            return true;
        }
        errorField.postValue(null);
        return false;
    }

    private List<Preset> presets;
    @Getter
    private int presetSelection = 0;
    @Getter
    private Preset selectedPreset;

    Completable deletePresetClicked() {
        SportAssistantPersistence persistence = ApplicationState.getInstance().getPersistence();
        return persistence.deletePreset(presets.get(presetSelection - 2))
                .concatWith(Completable.fromAction(() -> {
                    presetSelection = 0;
                    selectedPreset = null;
                }));
    }

    private Single<List<String>> presetList;

    Single<List<String>> getPresetList() {
        if (presetList == null) {
            SportAssistantPersistence persistence = ApplicationState.getInstance().getPersistence();
            Context context = getApplication();
            presetList = persistence.findAllPresets()
                    .map(list -> {
                        presets = new ArrayList<>(list);
                        list.add(0, new Preset(context.getString(R.string.create_new_preset), null));
                        list.add(0, new Preset(context.getString(R.string.no_preset), null));
                        return list.stream().map(Preset::getName).collect(Collectors.toList());
                    });
        }
        return presetList;
    }

    @Getter
    private int sportTypeSelection = 0;
    @Getter
    SingleLiveEvent<Void> updateModulesCommand = new SingleLiveEvent<>();

    void selectSport(int position) {
        SportType newSportType =
                SportEvent.NAMES.entrySet().stream()
                        .filter(e -> e.getKey() != GENERIC)
                        .sorted(Map.Entry.comparingByValue())
                        .map(Map.Entry::getKey)
                        .collect(Collectors.toList())
                        .get(position);
        if (newSportType != selectedSportType) {
            selectedSportType = newSportType;
            updateModuleList();
            updateModulesCommand.call();
        }
        sportTypeSelection = position;
    }

    @Getter
    private SingleLiveEvent<Void> showPresetNameCommand = new SingleLiveEvent<>();
    @Getter
    private SingleLiveEvent<Void> hidePresetNameCommand = new SingleLiveEvent<>();
    @Getter
    private SingleLiveEvent<Void> updatePresetCommand = new SingleLiveEvent<>();
    @Getter
    private boolean newPreset = false;

    @SneakyThrows
    void presetSelected(int position) {
        if (position == 0) {
            newPreset = false;
            selectedPreset = null;
            hidePresetNameCommand.call();
        } else if (position == 1) {
            newPreset = true;
            selectedPreset = null;
            showPresetNameCommand.call();
        } else {
            selectedPreset = presets.get(position - 2);
            newPreset = false;

            selectedSportType = SportType.valueOf(selectedPreset.getData().getString("sportType"));
            sportTypeSelection =
                    SportEvent.NAMES.entrySet().stream()
                            .filter(e -> e.getKey() != GENERIC)
                            .sorted(Map.Entry.comparingByValue())
                            .map(Map.Entry::getKey)
                            .collect(Collectors.toList())
                            .indexOf(selectedSportType);
            updateModuleList();
            JSONArray mArray = selectedPreset.getData().getJSONArray("modules");
            moduleListItems.forEach(i -> {
                if (i != null) {
                    boolean contains = false;
                    for (int n = 0; n < mArray.length(); n++) {
                        try {
                            if (mArray.getString(n).equals(i.getModuleClass().getSimpleName())) {
                                contains = true;
                            }
                        } catch (Exception e) {
                            throw new RuntimeException(e);
                        }
                    }
                    i.setSelected(contains);
                }
            });
            hidePresetNameCommand.call();
            updatePresetCommand.call();
        }
        presetSelection = position;
    }

    private Single<List<String>> teamNames;
    private List<TeamWithPlayers> teams = new ArrayList<>();

    Single<List<String>> getTeamNames() {
        if (teamNames == null) {
            Context context = getApplication();
            SportAssistantPersistence persistence =
                    ApplicationState.getInstance().getPersistence();
            teamNames = persistence.findAllTeamsWithPlayers()
                    .map(list -> {
                                teams.addAll(list);
                                return list.stream()
                                        .map(t -> t.team.getName())
                                        .collect(Collectors.toList());
                            }
                    ).onErrorReturn(e -> Collections.singletonList(context.getString(R.string.error_occurred))
                    ).cache();
        }
        return teamNames;
    }

    @Getter
    private MutableLiveData<String> eventName = new MutableLiveData<>("");
    @Getter
    private MutableLiveData<String> eventNameError = new MutableLiveData<>();
    @Getter
    private MutableLiveData<String> presetName = new MutableLiveData<>("");
    @Getter
    private MutableLiveData<String> presetNameError = new MutableLiveData<>();
    @Getter
    private MutableLiveData<String> team1Name = new MutableLiveData<>("");
    @Getter
    private MutableLiveData<String> team2Name = new MutableLiveData<>("");
    @Getter
    private MutableLiveData<String> eventDescription = new MutableLiveData<>();
    @Getter
    private SingleLiveEvent<Void> continueCommand = new SingleLiveEvent<>();
    @Getter
    private SingleLiveEvent<Void> skipToEventCommand = new SingleLiveEvent<>();
    @Getter
    private SingleLiveEvent<Void> cancelCommand = new SingleLiveEvent<>();

    @Getter
    private int team1Selection = 0;
    @Getter
    private int team2Selection = 0;

    public void submit() {
        if (invalidText(eventName, eventNameError) ||
                (invalidText(presetName, presetNameError) && newPreset))
            return;
        SportEvent newEvent = new SportEvent(
                eventName.getValue(),
                eventDescription.getValue(),
                teams.get(team1Selection),
                teams.get(team2Selection),
                selectedSportType);
        newEvent.getModules().addAll(getModuleListItems().stream()
                .filter(i -> i != null && i.isSelected())
                .map(item -> {
                    try {
                        return (Module) ((Class<?>) item.getModuleClass()).getDeclaredConstructor(SportEvent.class)
                                .newInstance(newEvent);
                    } catch (Exception e) {
                        return null;
                    }
                }).filter(Objects::nonNull).collect(Collectors.toList()));
        newEvent.updateTimeProviderModule();
        ApplicationState.getInstance().setCurrentEvent(newEvent);
        saveModulesToPreset();
        if (newEvent.getModules().stream()
                .anyMatch(m -> m.getConfiguration().hasConfigFragment())) {
            continueCommand.call();
        } else {
            skipToEventCommand.call();
        }
    }

    @SneakyThrows
    private void saveModulesToPreset() {
        if (newPreset) {
            JSONArray moduleArray = new JSONArray();
            moduleListItems.forEach(m -> {
                if (m != null && m.isSelected())
                    moduleArray.put(m.getModuleClass().getSimpleName());
            });
            JSONObject modulesObject = new JSONObject();
            modulesObject.put("modules", moduleArray);
            modulesObject.put("sportType", selectedSportType.toString());

            selectedPreset = new Preset(requireNonNull(presetName.getValue()), modulesObject);
        }
    }

    public void cancel() {
        cancelCommand.call();
    }

    @Getter
    private SingleLiveEvent<Boolean> updateButtonCommand = new SingleLiveEvent<>();

    void setTeam1Selection(int team1Selection) {
        this.team1Selection = team1Selection;
        updateButtonCommand.setValue(team1Selection != team2Selection);
    }

    void setTeam2Selection(int team2Selection) {
        this.team2Selection = team2Selection;
        updateButtonCommand.setValue(team1Selection != team2Selection);
    }
}
