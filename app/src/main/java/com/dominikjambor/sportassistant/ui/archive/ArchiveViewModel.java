package com.dominikjambor.sportassistant.ui.archive;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.dominikjambor.sportassistant.data.ArchivedSportEvent;
import com.dominikjambor.sportassistant.logic.ApplicationState;
import com.dominikjambor.sportassistant.logic.SportEvent;
import com.dominikjambor.sportassistant.persistence.SportAssistantPersistence;
import com.dominikjambor.sportassistant.ui.utils.SingleLiveEvent;

import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

import io.reactivex.Single;
import lombok.Getter;

public class ArchiveViewModel extends AndroidViewModel {
    private Single<List<ArchiveListItem>> eventListItems;
    private List<ArchivedSportEvent> events;

    public ArchiveViewModel(@NonNull Application application) {
        super(application);
    }

    Single<List<ArchiveListItem>> getEventListItems() {
        if (eventListItems == null) {
            SportAssistantPersistence persistence = ApplicationState.getInstance().getPersistence();
            Context context = getApplication();
            eventListItems = persistence.findAllSportEvents()
                    .map(list -> {
                                events = list;
                                return list.stream()
                                        .map(item -> new ArchiveListItem(
                                                item.getName(),
                                                item.getStartDateTime().format(DateTimeFormatter.ofPattern("YYYY.MM.dd HH:mm")),
                                                item.getTeam1() + " - " + item.getTeam2(),
                                                context.getString(SportEvent.getNameOf(item.getSportType()))
                                        ))
                                        .collect(Collectors.toList());
                            }
                    );
        }
        return eventListItems;
    }

    @Getter private ArchivedSportEvent selectedEvent;
    @Getter private SingleLiveEvent<Void> openTeamCommand = new SingleLiveEvent<>();

    void selectItem(int position) {
        selectedEvent = events.get(position);
        openTeamCommand.call();
    }
}
