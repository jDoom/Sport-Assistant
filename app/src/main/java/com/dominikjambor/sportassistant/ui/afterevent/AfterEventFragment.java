package com.dominikjambor.sportassistant.ui.afterevent;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import com.dominikjambor.sportassistant.R;
import com.dominikjambor.sportassistant.databinding.FragmentAftereventBinding;
import com.dominikjambor.sportassistant.ui.utils.Utils;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class AfterEventFragment extends Fragment {

    CompositeDisposable trash = new CompositeDisposable();

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        FragmentAftereventBinding binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_afterevent,
                container, false);
        binding.setLifecycleOwner(this);

        AfterEventViewModel viewModel = new ViewModelProvider(this).get(AfterEventViewModel.class);

        trash.add(viewModel.saveEvent()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> viewModel.setSaved(true),
                        e -> Toast.makeText(requireContext(), "Couldn't save event! " + e.getMessage(), Toast.LENGTH_SHORT).show()));

        viewModel.getDoneCommand().observe(getViewLifecycleOwner(),
                o -> Navigation.findNavController(container).navigate(R.id.action_nav_afterevent_to_nav_home));
        viewModel.getArchiveCommand().observe(getViewLifecycleOwner(),
                o -> Navigation.findNavController(container).navigate(R.id.action_nav_afterevent_to_nav_archive));
        binding.setViewmodel(viewModel);

        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        trash.clear();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.action_help) {
            Utils.showSimpleTextDialog(requireContext(), R.string.help_afterevent);
        }
        return false;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.fragments_base, menu);
    }
}
