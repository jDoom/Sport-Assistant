package com.dominikjambor.sportassistant.ui.modules;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
class ModuleListItem {
    private String name;
    private String description;
    private String sport;
    private boolean playerStat;
    private boolean teamStat;
}
