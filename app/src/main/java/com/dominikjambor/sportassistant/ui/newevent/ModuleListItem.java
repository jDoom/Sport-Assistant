package com.dominikjambor.sportassistant.ui.newevent;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
class ModuleListItem {
    private String name;
    private boolean selected;
    private Class moduleClass;
}
