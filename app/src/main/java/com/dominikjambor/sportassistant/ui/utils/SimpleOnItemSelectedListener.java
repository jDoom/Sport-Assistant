package com.dominikjambor.sportassistant.ui.utils;

import android.view.View;
import android.widget.AdapterView;

import androidx.core.util.Consumer;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class SimpleOnItemSelectedListener implements AdapterView.OnItemSelectedListener {
    private final Consumer<Integer> handler;

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        handler.accept(position);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
