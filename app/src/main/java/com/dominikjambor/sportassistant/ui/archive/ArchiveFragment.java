package com.dominikjambor.sportassistant.ui.archive;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.room.EmptyResultSetException;

import com.dominikjambor.sportassistant.R;
import com.dominikjambor.sportassistant.databinding.FragmentArchiveBinding;
import com.dominikjambor.sportassistant.ui.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class ArchiveFragment extends Fragment {

    @BindView(R.id.archive_teamList) ListView teamListView;
    @BindView(R.id.archive_subText) TextView subTextView;
    @BindView(R.id.archive_progressBar) ProgressBar progressBar;

    private CompositeDisposable trash = new CompositeDisposable();

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        FragmentArchiveBinding binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_archive,
                container, false);
        binding.setLifecycleOwner(this);

        View root = binding.getRoot();
        ButterKnife.bind(this, root);

        ArchiveViewModel viewModel = new ViewModelProvider(this).get(ArchiveViewModel.class);
        binding.setViewmodel(viewModel);

        trash.add(viewModel.getEventListItems()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(list -> {
                            teamListView.setAdapter(new ArchiveListAdapter(getContext(), list));
                            teamListView.setVisibility(View.VISIBLE);
                            progressBar.setVisibility(View.INVISIBLE);
                            if (list.isEmpty()) {
                                subTextView.setText(getString(R.string.no_data_to_show));
                                teamListView.setEmptyView(subTextView);
                            }
                        },
                        e -> {
                            if (e instanceof EmptyResultSetException) {
                                subTextView.setText(getString(R.string.no_data_to_show));
                            } else {
                                subTextView.setText(getString(R.string.error_occurred));
                            }
                            teamListView.setEmptyView(subTextView);
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                ));
        teamListView.setOnItemClickListener((parent, view, position, id) -> viewModel.selectItem(position));
        viewModel.getOpenTeamCommand().observe(getViewLifecycleOwner(), o -> {
            ArchiveFragmentDirections.ActionNavArchiveToArchivedEventFragment action = ArchiveFragmentDirections
                    .actionNavArchiveToArchivedEventFragment(viewModel.getSelectedEvent());
            Navigation.findNavController(root).navigate(action);
        });

        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        trash.clear();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.action_help) {
            Utils.showSimpleTextDialog(requireContext(), R.string.help_archive);
        }
        return false;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.fragments_base, menu);
    }
}
