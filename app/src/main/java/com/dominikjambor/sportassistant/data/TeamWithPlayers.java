package com.dominikjambor.sportassistant.data;

import androidx.room.Embedded;
import androidx.room.Ignore;
import androidx.room.Relation;

import java.io.Serializable;
import java.util.List;

import lombok.EqualsAndHashCode;
import lombok.ToString;

@ToString
@EqualsAndHashCode
public class TeamWithPlayers implements Serializable {
    @Embedded
    public Team team;
    @Relation(
            parentColumn = "id",
            entityColumn = "teamId"
    )
    public List<Player> players;

    @Ignore
    public TeamWithPlayers(Team team, List<Player> players) {
        this.team = team;
        this.players = players;
    }

    public TeamWithPlayers() {
    }
}
