package com.dominikjambor.sportassistant.data;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.util.UUID;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity(indices = {@Index("eventId")},
        foreignKeys = {
                @ForeignKey(entity = ArchivedSportEvent.class, parentColumns = "id",
                        childColumns = "eventId", onDelete = ForeignKey.CASCADE)}
)
@EqualsAndHashCode(callSuper = true)
public class ArchivedLogEntry extends LogEntry {
    public ArchivedLogEntry(LogEntry entry, UUID eventId) {
        super(entry.text, entry.data, entry.author, entry.created, entry.millisSinceStart);
        id = UUID.randomUUID();
        this.eventId = eventId;
    }


    @NonNull
    @PrimaryKey
    private UUID id;
    private UUID eventId;

    public ArchivedLogEntry(@NonNull UUID id, UUID eventId) {
        this.id = id;
        this.eventId = eventId;
    }
}
