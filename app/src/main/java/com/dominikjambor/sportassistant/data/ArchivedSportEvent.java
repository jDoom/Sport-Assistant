package com.dominikjambor.sportassistant.data;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.dominikjambor.sportassistant.logic.SportEvent.SportType;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

import lombok.Data;

@Data
@Entity
public class ArchivedSportEvent implements Serializable {
    @NonNull
    @PrimaryKey
    private UUID id;
    private String name;
    private String description;
    private LocalDateTime startDateTime;
    private LocalDateTime endDateTime;
    private SportType sportType;
    private String team1;
    private String team2;
    private UUID team1Id;
    private UUID team2Id;


    public ArchivedSportEvent(@NonNull UUID id, String name, String description, LocalDateTime startDateTime, LocalDateTime endDateTime, SportType sportType, String team1, String team2, UUID team1Id, UUID team2Id) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.startDateTime = startDateTime;
        this.endDateTime = endDateTime;
        this.sportType = sportType;
        this.team1 = team1;
        this.team2 = team2;
        this.team1Id = team1Id;
        this.team2Id = team2Id;
    }
}
