package com.dominikjambor.sportassistant.data;

import androidx.room.Embedded;
import androidx.room.Ignore;
import androidx.room.Relation;

import java.io.Serializable;
import java.util.List;

import lombok.EqualsAndHashCode;
import lombok.ToString;

@ToString
@EqualsAndHashCode
public class ArchivedSportEventWithLogs implements Serializable {
    @Embedded
    public ArchivedSportEvent event;
    @Relation(
            parentColumn = "id",
            entityColumn = "eventId"
    )
    public List<ArchivedLogEntry> logEntries;

    @Ignore
    public ArchivedSportEventWithLogs(ArchivedSportEvent event, List<ArchivedLogEntry> logEntries) {
        this.event = event;
        this.logEntries = logEntries;
    }

    public ArchivedSportEventWithLogs() {
    }
}