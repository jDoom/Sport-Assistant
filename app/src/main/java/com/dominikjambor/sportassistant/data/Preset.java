package com.dominikjambor.sportassistant.data;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.json.JSONObject;

import java.io.Serializable;

import lombok.Data;

@Entity
@Data
public class Preset implements Serializable {
    @NonNull
    @PrimaryKey
    private String name;
    private JSONObject data;


    public Preset(@NonNull String name, JSONObject data) {
        this.name = name;
        this.data = data;
    }
}
