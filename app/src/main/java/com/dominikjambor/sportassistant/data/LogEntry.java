package com.dominikjambor.sportassistant.data;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LogEntry {
    protected String text;
    protected String data;
    protected String author;
    protected LocalDateTime created;
    protected long millisSinceStart;
}
