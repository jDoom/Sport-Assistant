package com.dominikjambor.sportassistant.data;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;
import java.util.UUID;

import lombok.Data;

@Data
@Entity
public class Team implements Serializable {
    @NonNull
    @PrimaryKey
    private UUID id;
    private String name;
    private String description;

    public Team(@NonNull UUID id, String name) {
        this.id = id;
        this.name = name;
    }
}
