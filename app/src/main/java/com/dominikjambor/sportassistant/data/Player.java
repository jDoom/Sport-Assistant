package com.dominikjambor.sportassistant.data;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.io.Serializable;
import java.util.UUID;

import lombok.Data;

@Data
@Entity(indices = {@Index("teamId")},
        foreignKeys = {
                @ForeignKey(entity = Team.class, parentColumns = "id",
                        childColumns = "teamId", onDelete = ForeignKey.SET_NULL)}
)
public class Player implements Serializable {
    @NonNull
    @PrimaryKey
    private UUID id;
    private String name;
    private String jerseyNumber;
    private UUID teamId;

    public Player(@NonNull UUID id, String name, String jerseyNumber, UUID teamId) {
        this.id = id;
        this.name = name;
        this.jerseyNumber = jerseyNumber;
        this.teamId = teamId;
    }
}