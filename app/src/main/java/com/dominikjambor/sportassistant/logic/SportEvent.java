package com.dominikjambor.sportassistant.logic;

import com.dominikjambor.sportassistant.R;
import com.dominikjambor.sportassistant.data.LogEntry;
import com.dominikjambor.sportassistant.data.TeamWithPlayers;
import com.dominikjambor.sportassistant.modules.Module;
import com.google.common.collect.ImmutableMap;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import lombok.Getter;

import static com.dominikjambor.sportassistant.logic.SportEvent.EventState.CREATED;
import static com.dominikjambor.sportassistant.logic.SportEvent.EventState.ENDED;
import static com.dominikjambor.sportassistant.logic.SportEvent.EventState.PAUSED;
import static com.dominikjambor.sportassistant.logic.SportEvent.EventState.STARTED;
import static com.dominikjambor.sportassistant.logic.SportEvent.SportType.BASKETBALL;
import static com.dominikjambor.sportassistant.logic.SportEvent.SportType.GENERIC;
import static com.dominikjambor.sportassistant.logic.SportEvent.SportType.SOCCER;
import static com.dominikjambor.sportassistant.logic.SportEvent.SportType.ULTIMATE;
import static com.dominikjambor.sportassistant.logic.SportEvent.SportType.VOLLEYBALL;

@Getter
public class SportEvent implements EventTimeProvider {

    private final String name;
    private final String description;
    private final List<Module> modules;
    private final TeamWithPlayers team1;
    private final TeamWithPlayers team2;
    private final List<LogEntry> log;
    private EventTimeProvider timeProvider;
    private final SportType sport;
    private LocalDateTime startDateTime;
    private LocalDateTime endDateTime;

    public SportEvent(String name, String description, TeamWithPlayers team1, TeamWithPlayers team2, SportType sport) {
        this.name = name;
        this.description = description;
        this.team1 = team1;
        this.team2 = team2;
        this.sport = sport;
        log = new ArrayList<>();
        modules = new ArrayList<>();
        state = CREATED;
    }

    public enum EventState {
        CREATED,
        STARTED,
        PAUSED,
        ENDED
    }

    private EventState state;

    public void start() {
        if (state == CREATED) {
            startDateTime = LocalDateTime.now();
            modules.forEach(Module::onEventStarted);
            state = STARTED;
            log(getClass().getSimpleName(), "", "Event Started.");
        }
    }

    public void pause() {
        if (state == STARTED) {
            modules.forEach(Module::onEventPaused);
            state = PAUSED;
            log(getClass().getSimpleName(), "", "Event Paused.");
        }
    }

    public void resume() {
        if (state == PAUSED) {
            modules.forEach(Module::onEventResumed);
            state = STARTED;
            log(getClass().getSimpleName(), "", "Event Resumed.");
        }
    }

    public void end() {
        endDateTime = LocalDateTime.now();
        modules.forEach(Module::onEventEnded);
        state = ENDED;
        log(getClass().getSimpleName(), "", "Event Ended.");
    }

    public void log(String author, String data, String text) {
        LogEntry entry = new LogEntry(text, data, author, LocalDateTime.now(), -1);
        log.add(entry);
    }

    public void updateTimeProviderModule() {
        for (Module module : modules) {
            if (module instanceof EventTimeProvider) {
                timeProvider = (EventTimeProvider) module;
                break;
            }
        }
    }

    @Override
    public long getCleanDuration() {
        return timeProvider == null ? -1 : timeProvider.getCleanDuration();
    }

    @Override
    public long getFullDuration() {
        return timeProvider == null ? -1 : timeProvider.getFullDuration();
    }

    @Override
    public String getLogTimeStampText() {
        return timeProvider == null ? "" : timeProvider.getLogTimeStampText();
    }

    public enum SportType {
        GENERIC,
        BASKETBALL,
        ULTIMATE,
        SOCCER,
        VOLLEYBALL
    }

    public static ImmutableMap<SportType, Integer> NAMES =
            new ImmutableMap.Builder<SportType, Integer>()
                    .put(GENERIC, R.string.sporttype_generic)
                    .put(BASKETBALL, R.string.sporttype_basketball)
                    .put(ULTIMATE, R.string.sporttype_ultimate)
                    .put(SOCCER, R.string.sporttype_soccer)
                    .put(VOLLEYBALL, R.string.sporttype_volleyball)
                    .build();

    public static int getNameOf(SportType type) {
        return Objects.requireNonNull(NAMES.get(type));
    }
}