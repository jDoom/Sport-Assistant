package com.dominikjambor.sportassistant.logic;

public interface EventTimeProvider {
    long getCleanDuration();

    long getFullDuration();

    String getLogTimeStampText();
}
