package com.dominikjambor.sportassistant.logic;

import android.annotation.SuppressLint;
import android.content.Context;

import com.dominikjambor.sportassistant.persistence.SportAssistantPersistence;
import com.dominikjambor.sportassistant.persistence.room.RoomPersistenceImpl;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ApplicationState {
    private static ApplicationState instance;

    public static ApplicationState getInstance() {
        return instance;
    }

    public static void init(Context context) {
        if (instance == null) {
            instance = new ApplicationState(context.getApplicationContext());
        }
    }

    private SportEvent currentEvent;
    private Context context;
    private SportAssistantPersistence persistence;

    @SuppressLint("CheckResult")
    private ApplicationState(Context context) {
        this.context = context;
        persistence = new RoomPersistenceImpl(context);
    }
}
