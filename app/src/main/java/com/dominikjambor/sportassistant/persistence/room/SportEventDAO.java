package com.dominikjambor.sportassistant.persistence.room;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import com.dominikjambor.sportassistant.data.ArchivedSportEvent;
import com.dominikjambor.sportassistant.data.ArchivedSportEventWithLogs;
import com.dominikjambor.sportassistant.logic.SportEvent.SportType;

import java.util.List;
import java.util.UUID;

import io.reactivex.Completable;
import io.reactivex.Single;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public
interface SportEventDAO {
    @Query("SELECT * FROM archivedsportevent ORDER BY startdatetime DESC")
    Single<List<ArchivedSportEvent>> findAll();

    @Transaction
    @Query("SELECT * FROM archivedsportevent")
    Single<List<ArchivedSportEventWithLogs>> findAllWithLogs();

    @Transaction
    @Query("SELECT * FROM archivedsportevent WHERE archivedsportevent.sportType=:type")
    Single<List<ArchivedSportEventWithLogs>> findAllWithLogs(SportType type);

    @Transaction
    @Query("SELECT * FROM archivedsportevent WHERE sportType=:type AND (team1Id=:teamId OR team2Id=:teamId)")
    Single<List<ArchivedSportEventWithLogs>> findAllWithLogs(SportType type, UUID teamId);

    @Insert(onConflict = REPLACE)
    Completable insert(ArchivedSportEvent event);

    @Update
    Completable update(ArchivedSportEvent event);

    @Delete
    Completable delete(ArchivedSportEvent event);
}
