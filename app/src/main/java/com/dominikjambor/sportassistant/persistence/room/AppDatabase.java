package com.dominikjambor.sportassistant.persistence.room;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.dominikjambor.sportassistant.data.ArchivedLogEntry;
import com.dominikjambor.sportassistant.data.ArchivedSportEvent;
import com.dominikjambor.sportassistant.data.Player;
import com.dominikjambor.sportassistant.data.Preset;
import com.dominikjambor.sportassistant.data.Team;

@Database(entities = {Player.class, Team.class, ArchivedSportEvent.class, ArchivedLogEntry.class, Preset.class},
        version = 9, exportSchema = false)
@TypeConverters({Converters.class})
public abstract class AppDatabase extends RoomDatabase {
    public abstract PlayerDAO playerDAO();

    public abstract TeamDAO teamDAO();

    public abstract SportEventDAO sportEventDAO();

    public abstract LogEntryDAO logEntryDAO();

    public abstract PresetDAO presetDAO();
}
