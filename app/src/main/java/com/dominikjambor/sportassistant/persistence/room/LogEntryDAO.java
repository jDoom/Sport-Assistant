package com.dominikjambor.sportassistant.persistence.room;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.dominikjambor.sportassistant.data.ArchivedLogEntry;

import java.util.List;
import java.util.UUID;

import io.reactivex.Completable;
import io.reactivex.Single;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public
interface LogEntryDAO {
    @Query("SELECT * FROM archivedlogentry")
    Single<List<ArchivedLogEntry>> findAll();

    @Query("SELECT * FROM archivedlogentry WHERE eventId=:id")
    Single<List<ArchivedLogEntry>> findAllByEventId(UUID id);

    @Query("SELECT * FROM archivedlogentry WHERE author=:author")
    Single<List<ArchivedLogEntry>> findAllByAuthor(String author);

    @Insert(onConflict = REPLACE)
    Completable insert(ArchivedLogEntry entry);

    @Insert(onConflict = REPLACE)
    Completable insert(List<ArchivedLogEntry> entry);
}
