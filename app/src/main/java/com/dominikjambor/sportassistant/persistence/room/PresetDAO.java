package com.dominikjambor.sportassistant.persistence.room;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.dominikjambor.sportassistant.data.Preset;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public
interface PresetDAO {
    @Query("SELECT * FROM preset")
    Single<List<Preset>> findAll();

    @Insert(onConflict = REPLACE)
    Completable insert(Preset preset);

    @Delete
    Completable delete(Preset preset);
}
