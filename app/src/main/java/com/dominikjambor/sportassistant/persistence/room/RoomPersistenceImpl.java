package com.dominikjambor.sportassistant.persistence.room;

import android.content.Context;

import androidx.room.Room;

import com.dominikjambor.sportassistant.data.ArchivedLogEntry;
import com.dominikjambor.sportassistant.data.ArchivedSportEvent;
import com.dominikjambor.sportassistant.data.ArchivedSportEventWithLogs;
import com.dominikjambor.sportassistant.data.Player;
import com.dominikjambor.sportassistant.data.Preset;
import com.dominikjambor.sportassistant.data.Team;
import com.dominikjambor.sportassistant.data.TeamWithPlayers;
import com.dominikjambor.sportassistant.logic.SportEvent.SportType;
import com.dominikjambor.sportassistant.persistence.SportAssistantPersistence;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import io.reactivex.Completable;
import io.reactivex.Single;

public class RoomPersistenceImpl implements SportAssistantPersistence {

    private final AppDatabase db;

    public RoomPersistenceImpl(Context context) {
        db = Room.databaseBuilder(context, AppDatabase.class, "sportassistant-data")
                .fallbackToDestructiveMigration()
                //.allowMainThreadQueries()
                .build();
    }

    @Override
    public Single<List<Player>> findAllPlayers() {
        return db.playerDAO().findAll();
    }

    @Override
    public Single<Team> findTeamById(UUID id) {
        return db.teamDAO().findById(id);
    }

    @Override
    public Single<List<Player>> findPlayersByTeamId(UUID id) {
        return id == null ? db.playerDAO().findNoTeam() : db.playerDAO().findByTeam(id);
    }

    @Override
    public Completable savePlayer(Player player) {
        return db.playerDAO().insert(player);
    }

    @Override
    public Completable deletePlayer(Player player) {
        return db.playerDAO().delete(player);
    }

    @Override
    public Completable updatePlayer(Player player) {
        return db.playerDAO().update(player);
    }

    @Override
    public Completable clear() {
        return Completable.fromFuture(CompletableFuture.runAsync(db::clearAllTables));
    }

    @Override
    public Single<List<ArchivedLogEntry>> findAllLogEntries() {
        return db.logEntryDAO().findAll();
    }

    @Override
    public Single<List<ArchivedLogEntry>> findLogEntriesByEventId(UUID id) {
        return db.logEntryDAO().findAllByEventId(id);
    }

    @Override
    public Single<List<ArchivedLogEntry>> findLogEntriesByAuthor(String author) {
        return db.logEntryDAO().findAllByAuthor(author);
    }

    @Override
    public Completable saveLogEntry(ArchivedLogEntry entry) {
        return db.logEntryDAO().insert(entry);
    }

    @Override
    public Completable saveLogEntries(List<ArchivedLogEntry> entries) {
        return db.logEntryDAO().insert(entries);
    }

    @Override
    public Single<List<ArchivedSportEvent>> findAllSportEvents() {
        return db.sportEventDAO().findAll();
    }

    @Override
    public Single<List<ArchivedSportEventWithLogs>> findAllSportEventsWithLogs() {
        return db.sportEventDAO().findAllWithLogs();
    }

    @Override
    public Single<List<ArchivedSportEventWithLogs>> findAllSportEventsWithLogs(SportType sportType) {
        return db.sportEventDAO().findAllWithLogs(sportType);
    }

    @Override
    public Single<List<ArchivedSportEventWithLogs>> findAllSportEventsWithLogs(SportType sportType, UUID teamId) {
        return db.sportEventDAO().findAllWithLogs(sportType, teamId);
    }

    @Override
    public Completable saveSportEvent(ArchivedSportEvent event) {
        return db.sportEventDAO().insert(event);
    }

    @Override
    public Completable updateSportEvent(ArchivedSportEvent event) {
        return db.sportEventDAO().update(event);
    }

    @Override
    public Completable deleteSportEvent(ArchivedSportEvent event) {
        return db.sportEventDAO().delete(event);
    }

    @Override
    public Single<List<Preset>> findAllPresets() {
        return db.presetDAO().findAll();
    }

    @Override
    public Completable savePreset(Preset preset) {
        return db.presetDAO().insert(preset);
    }

    @Override
    public Completable deletePreset(Preset preset) {
        return db.presetDAO().delete(preset);
    }

    @Override
    public Single<List<Team>> findAllTeams() {
        return db.teamDAO().findAll();
    }

    @Override
    public Completable saveTeam(Team team) {
        return db.teamDAO().insert(team);
    }

    @Override
    public Completable updateTeam(Team team) {
        return db.teamDAO().update(team);
    }

    @Override
    public Single<List<TeamWithPlayers>> findAllTeamsWithPlayers() {
        return db.teamDAO().findAllTeamsWithPlayers();
    }

    @Override
    public Completable deleteTeam(Team team) {
        return db.teamDAO().delete(team);
    }

    @Override
    public Single<TeamWithPlayers> findTeamWithPlayersByTeamId(UUID id) {
        return db.teamDAO().findTeamWithPlayersByTeamId(id);
    }
}
