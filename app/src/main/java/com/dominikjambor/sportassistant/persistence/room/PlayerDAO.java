package com.dominikjambor.sportassistant.persistence.room;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.dominikjambor.sportassistant.data.Player;

import java.util.List;
import java.util.UUID;

import io.reactivex.Completable;
import io.reactivex.Single;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public
interface PlayerDAO {
    @Query("SELECT * FROM player")
    Single<List<Player>> findAll();

    @Query("SELECT * FROM player WHERE player.teamId=:teamId")
    Single<List<Player>> findByTeam(UUID teamId);

    @Query("SELECT * FROM player WHERE player.teamId IS NULL")
    Single<List<Player>> findNoTeam();

    @Insert(onConflict = REPLACE)
    Completable insert(Player player);

    @Update
    Completable update(Player player);

    @Delete
    Completable delete(Player player);
}
