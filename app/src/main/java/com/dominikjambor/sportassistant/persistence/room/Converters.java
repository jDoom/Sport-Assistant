package com.dominikjambor.sportassistant.persistence.room;

import androidx.room.TypeConverter;

import com.dominikjambor.sportassistant.logic.SportEvent.SportType;

import org.json.JSONObject;

import java.time.LocalDateTime;
import java.util.UUID;

import lombok.SneakyThrows;

public class Converters {
    @TypeConverter
    public static UUID uuidFromString(String string) {
        return string == null ? null : UUID.fromString(string);
    }

    @TypeConverter
    public static String uuidToString(UUID uuid) {
        return uuid == null ? null : uuid.toString();
    }

    @TypeConverter
    public static LocalDateTime localDateTimeFromString(String string) {
        return string == null ? null : LocalDateTime.parse(string);
    }

    @TypeConverter
    public static String localDateTimeToString(LocalDateTime localDateTime) {
        return localDateTime == null ? null : localDateTime.toString();
    }

    @TypeConverter
    public static SportType sportTypeFromString(String string) {
        return string == null ? null : SportType.valueOf(string);
    }

    @TypeConverter
    public static String sportTypeToString(SportType sportType) {
        return sportType == null ? null : sportType.toString();
    }

    @SneakyThrows
    @TypeConverter
    public static JSONObject jsonObjectFromString(String string) {
        return new JSONObject(string);
    }

    @TypeConverter
    public static String jsonObjectToString(JSONObject object) {
        return object.toString();
    }
}
