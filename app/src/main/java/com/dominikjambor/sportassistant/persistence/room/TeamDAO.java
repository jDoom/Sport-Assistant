package com.dominikjambor.sportassistant.persistence.room;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import com.dominikjambor.sportassistant.data.Team;
import com.dominikjambor.sportassistant.data.TeamWithPlayers;

import java.util.List;
import java.util.UUID;

import io.reactivex.Completable;
import io.reactivex.Single;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public
interface TeamDAO {
    @Query("SELECT * FROM team")
    Single<List<Team>> findAll();

    @Transaction
    @Query("SELECT * FROM team")
    Single<List<TeamWithPlayers>> findAllTeamsWithPlayers();

    @Transaction
    @Query("SELECT * FROM team WHERE id=:id")
    Single<TeamWithPlayers> findTeamWithPlayersByTeamId(UUID id);

    @Insert(onConflict = REPLACE)
    Completable insert(Team team);

    @Delete
    Completable delete(Team team);

    @Update
    Completable update(Team team);

    @Query("SELECT * FROM team WHERE id=:id")
    Single<Team> findById(UUID id);
}
