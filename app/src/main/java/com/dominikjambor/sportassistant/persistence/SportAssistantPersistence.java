package com.dominikjambor.sportassistant.persistence;

import com.dominikjambor.sportassistant.data.ArchivedLogEntry;
import com.dominikjambor.sportassistant.data.ArchivedSportEvent;
import com.dominikjambor.sportassistant.data.ArchivedSportEventWithLogs;
import com.dominikjambor.sportassistant.data.Player;
import com.dominikjambor.sportassistant.data.Preset;
import com.dominikjambor.sportassistant.data.Team;
import com.dominikjambor.sportassistant.data.TeamWithPlayers;
import com.dominikjambor.sportassistant.logic.SportEvent.SportType;

import java.util.List;
import java.util.UUID;

import io.reactivex.Completable;
import io.reactivex.Single;

public interface SportAssistantPersistence {

    //TEAMS

    Completable saveTeam(Team team);

    Completable updateTeam(Team team);

    Single<Team> findTeamById(UUID id);

    Single<List<Team>> findAllTeams();

    Single<TeamWithPlayers> findTeamWithPlayersByTeamId(UUID id);

    Single<List<TeamWithPlayers>> findAllTeamsWithPlayers();

    Completable deleteTeam(Team team);

    //PLAYERS

    Single<List<Player>> findAllPlayers();

    Single<List<Player>> findPlayersByTeamId(UUID id);

    Completable savePlayer(Player player);

    Completable deletePlayer(Player player);

    Completable updatePlayer(Player player);

    Completable clear();

    //LOG ENTRIES

    Single<List<ArchivedLogEntry>> findAllLogEntries();

    Single<List<ArchivedLogEntry>> findLogEntriesByEventId(UUID id);

    Single<List<ArchivedLogEntry>> findLogEntriesByAuthor(String author);

    Completable saveLogEntry(ArchivedLogEntry entry);

    Completable saveLogEntries(List<ArchivedLogEntry> entry);

    //ARCHIVED EVENTS

    Single<List<ArchivedSportEvent>> findAllSportEvents();

    Single<List<ArchivedSportEventWithLogs>> findAllSportEventsWithLogs();

    Single<List<ArchivedSportEventWithLogs>> findAllSportEventsWithLogs(SportType sportType);

    Single<List<ArchivedSportEventWithLogs>> findAllSportEventsWithLogs(SportType sportType, UUID teamId);

    Completable saveSportEvent(ArchivedSportEvent event);

    Completable updateSportEvent(ArchivedSportEvent event);

    Completable deleteSportEvent(ArchivedSportEvent event);

    //PRESETS

    Single<List<Preset>> findAllPresets();

    Completable savePreset(Preset preset);

    Completable deletePreset(Preset preset);
}
