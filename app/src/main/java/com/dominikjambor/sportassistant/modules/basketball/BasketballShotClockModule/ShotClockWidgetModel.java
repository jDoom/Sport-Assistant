package com.dominikjambor.sportassistant.modules.basketball.BasketballShotClockModule;

import androidx.lifecycle.MutableLiveData;

import com.dominikjambor.sportassistant.data.TeamWithPlayers;
import com.dominikjambor.sportassistant.logic.SportEvent;
import com.dominikjambor.sportassistant.ui.utils.SingleLiveEvent;

import org.json.JSONObject;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;

public class ShotClockWidgetModel {
    @Setter
    @Getter
    private int initialSeconds = 24;

    private BasketballShotClockModule module;
    @Getter private TeamWithPlayers team1;
    @Getter private TeamWithPlayers team2;

    @Getter private MutableLiveData<String> timeString = new MutableLiveData<>("");
    @Getter private MutableLiveData<String> pauseButtonText = new MutableLiveData<>("Pause");
    private Instant start;

    private Runnable updater = new Runnable() {
        @Override
        public void run() {
            long diffSeconds = 0;
            long diffTenths;
            long diffMillis = 0;
            diffMillis = ChronoUnit.MILLIS.between(start, Instant.now());
            while (diffMillis < initialSeconds * 1000) {
                diffSeconds = ((initialSeconds * 1000 - diffMillis) / 1000) % 60;
                diffTenths = ((initialSeconds * 1000 - diffMillis) / 100) % 10;
                timeString.postValue(diffSeconds + "." + diffTenths);
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    return;
                }
                diffMillis = ChronoUnit.MILLIS.between(start, Instant.now());
            }
        }
    };
    private Thread updateThread;

    ShotClockWidgetModel(BasketballShotClockModule module, SportEvent event) {
        this.module = module;
        team1 = event.getTeam1();
        team2 = event.getTeam2();
    }

    void incInitialSeconds() {
        if (initialSeconds < 120) {
            initialSeconds++;
        }
    }

    void decInitialSeconds() {
        if (initialSeconds > 1) {
            initialSeconds--;
        }
    }

    @Getter SingleLiveEvent<Void> enablePauseCommand = new SingleLiveEvent<>();

    void reset() {
        if (paused) {
            start = pauseStart;
            timeString.postValue(initialSeconds + ".0");
        } else {
            enablePauseCommand.call();
            start = Instant.now();
            startThread();
        }
    }

    @SneakyThrows
    void stopThread() {
        if (updateThread != null && updateThread.isAlive()) {
            updateThread.interrupt();
            updateThread.join();
        }
    }

    private void startThread() {
        if (updateThread == null || !updateThread.isAlive()) {
            updateThread = new Thread(updater);
            updateThread.start();
        }
    }

    private Instant pauseStart;
    @Getter private boolean paused = false;

    void pause() {
        if (!paused) {
            pauseStart = Instant.now();
            stopThread();
            pauseButtonText.postValue("Start");
            paused = true;
        } else {
            start = start.plus(Duration.between(pauseStart, Instant.now()));
            startThread();
            pauseButtonText.postValue("Pause");
            paused = false;
        }
    }

    void save() {
        timeString.postValue(initialSeconds + ".0");
    }

    void logViolation(TeamWithPlayers team) {
        JSONObject data = new JSONObject();
        try {
            data.put("teamId", team.team.getId().toString());
        } catch (Exception e) {
            module.log("Shot clock data failure: " + e.getMessage(), "{}");
        }

        module.log(team.team.getName() +
                        " committed a shot clock violation.",
                data.toString());
    }
}
