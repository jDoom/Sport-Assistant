package com.dominikjambor.sportassistant.modules;

import com.dominikjambor.sportassistant.logic.ApplicationState;
import com.dominikjambor.sportassistant.logic.SportEvent.SportType;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class FileModuleConfiguration implements ModuleConfiguration {

    private Properties properties = new Properties();

    FileModuleConfiguration(Object owner) throws IOException {
        properties.load(ApplicationState.getInstance()
                .getContext()
                .getResources()
                .getAssets()
                .open("module-configurations/" +
                        owner.getClass().getSimpleName().toLowerCase() +
                        ".properties")
        );
    }

    public FileModuleConfiguration(FileInputStream fileInputStream) throws IOException {
        properties.load(fileInputStream);
    }

    @Override
    public String getModuleName() {
        return properties.getProperty("moduleName", "No Name");
    }

    @Override
    public String getShortDescription() {
        return properties.getProperty("shortDescription", "No Description");
    }

    @Override
    public String getVersion() {
        return properties.getProperty("version", "0.0.0");
    }

    @Override
    public SportType getSportType() {
        String property = properties.getProperty("sportType", SportType.GENERIC.toString());
        return SportType.valueOf(property);
    }

    @Override
    public boolean hasPlayerStats() {
        String property = properties.getProperty("hasPlayerStats", "false");
        return Boolean.parseBoolean(property);
    }

    @Override
    public boolean hasTeamStats() {
        String property = properties.getProperty("hasTeamStats", "false");
        return Boolean.parseBoolean(property);
    }

    @Override
    public boolean hasConfigFragment() {
        String property = properties.getProperty("hasConfigFragment", "false");
        return Boolean.parseBoolean(property);
    }

    @Override
    public boolean hasFinalFragment() {
        String property = properties.getProperty("hasFinalFragment", "false");
        return Boolean.parseBoolean(property);
    }
}
