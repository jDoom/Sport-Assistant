package com.dominikjambor.sportassistant.modules.volleyball.VolleyballScoreModule;

import android.view.View;

import androidx.fragment.app.Fragment;

import com.dominikjambor.sportassistant.logic.SportEvent;
import com.dominikjambor.sportassistant.modules.Module;

import lombok.AllArgsConstructor;

public class VolleyballScoreModule extends Module {
    private ScoreWidgetView widgetView;

    public VolleyballScoreModule() {
    }

    public VolleyballScoreModule(SportEvent event) {
        super(event);
        model = new ScoreWidgetModel(this, event);
    }

    private ScoreWidgetModel model;

    @Override
    public View createWidgetView(Fragment parent) {
        widgetView = new ScoreWidgetView(model, parent);
        return widgetView;
    }

    @Override
    public void onEventStarted() {
        widgetView.toggleButtons(true);
    }

    @Override
    public void onEventEnded() {
        model.nextSet();
        log("", "{ resultText : \"Result: " +
                model.getTeam1().team.getName() + "   " + model.getTeam1SetScore().getValue() + " - " +
                model.getTeam2SetScore().getValue() + "   " + model.getTeam2().team.getName() + "\" }");
    }

    @Override
    public void onEventPaused() {
        widgetView.toggleButtons(false);
    }

    @Override
    public void onEventResumed() {
        widgetView.toggleButtons(true);
    }

    @AllArgsConstructor
    static class StatTableRow {
        public String dateStr;
        public String opponent;
        public int score;
    }
}
