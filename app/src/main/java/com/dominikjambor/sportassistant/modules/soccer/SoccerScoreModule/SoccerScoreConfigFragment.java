package com.dominikjambor.sportassistant.modules.soccer.SoccerScoreModule;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.dominikjambor.sportassistant.R;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SoccerScoreConfigFragment extends Fragment {

    private ScoreWidgetModel model;
    private JSONObject preset;

    public SoccerScoreConfigFragment(ScoreWidgetModel model, JSONObject preset) {
        this.model = model;
        this.preset = preset;
        setRetainInstance(true);
    }

    @BindView(R.id.soccer_score_config_enablePlayer) CheckBox enablePlayers;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.module_soccer_score_configuration, container, false);

        ButterKnife.bind(this, root);

        if (preset != null) {
            try {
                if (preset.has("enablePlayers"))
                    enablePlayers.setChecked(preset.getBoolean("enablePlayers"));
            } catch (Exception e) {
                Toast.makeText(requireContext(), "Error loading preset", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }

        return root;
    }

    public boolean validateAndSave() {
        model.setPlayerSelectionEnabled(enablePlayers.isChecked());
        return true;
    }

    public JSONObject createPreset() {
        JSONObject preset = new JSONObject();
        try {
            preset.put("enablePlayers", enablePlayers.isChecked());
        } catch (Exception e) {
            Toast.makeText(requireContext(), "Error creating preset", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
        return preset;
    }
}
