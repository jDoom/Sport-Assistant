package com.dominikjambor.sportassistant.modules;

import android.view.View;

import androidx.fragment.app.Fragment;

import com.dominikjambor.sportassistant.logic.SportEvent;

import org.json.JSONObject;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;

@NoArgsConstructor
public abstract class Module {

    protected SportEvent event;

    public Module(SportEvent event) {
        this.event = event;
    }

    public View createWidgetView(Fragment parent) {
        return null;
    }

    public Fragment createConfigurationFragment(JSONObject preset) {
        return null;
    }

    public boolean saveConfiguration() {
        return true;
    }

    public Fragment createFinalizationFragment() {
        return null;
    }

    public Fragment createPlayerStatFragment() {
        return null;
    }

    public Fragment createTeamStatFragment() {
        return null;
    }

    public boolean saveFinalization() {
        return true;
    }

    public void onEventStarted() {
    }

    public void onEventEnded() {
    }

    public void onEventPaused() {
    }

    public void onEventResumed() {
    }

    public JSONObject createPreset() {
        return new JSONObject();
    }

    @SneakyThrows
    public ModuleConfiguration getConfiguration() {
        return new FileModuleConfiguration(this);
    }

    public void log(String text, String data) {
        event.log(this.getClass().getName(), data, text.trim().isEmpty() ? "" : event.getLogTimeStampText() + " " + text);
    }
}
