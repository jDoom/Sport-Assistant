package com.dominikjambor.sportassistant.modules.generic.SubstitutionsModule;

import android.view.View;

import androidx.fragment.app.Fragment;

import com.dominikjambor.sportassistant.logic.SportEvent;
import com.dominikjambor.sportassistant.modules.Module;

import org.json.JSONObject;

import lombok.AllArgsConstructor;

import static com.dominikjambor.sportassistant.logic.SportEvent.EventState.CREATED;

public class SubstitutionsModule extends Module {

    private SubstitutionsWidgetModel widgetModel;
    private SubstitutionsWidgetView widgetView;
    private SubstitutionsConfigFragment configFragment;

    public SubstitutionsModule() {
    }

    public SubstitutionsModule(SportEvent event) {
        super(event);
        widgetModel = new SubstitutionsWidgetModel(this, event);
    }

    @Override
    public View createWidgetView(Fragment parent) {
        widgetView = new SubstitutionsWidgetView(widgetModel, parent);
        return widgetView;
    }

    @Override
    public Fragment createConfigurationFragment(JSONObject preset) {
        configFragment = new SubstitutionsConfigFragment(widgetModel, preset);
        return configFragment;
    }

    @Override
    public Fragment createPlayerStatFragment() {
        return new SubstitutionPlayerStatFragment();
    }

    @Override
    public JSONObject createPreset() {
        return configFragment.createPreset();
    }

    @Override
    public boolean saveConfiguration() {
        return configFragment.validateAndSave();
    }

    @Override
    public void onEventStarted() {
        widgetView.toggleButtons(true);
        widgetModel.resetTimer();
    }

    @Override
    public void onEventEnded() {
        if (event.getState() != CREATED) {
            widgetModel.updateTimes();
        }
        widgetModel.logTimes();
    }

    @Override
    public void onEventPaused() {
        widgetView.toggleButtons(false);
        widgetModel.updateTimes();
    }

    @Override
    public void onEventResumed() {
        widgetView.toggleButtons(true);
        widgetModel.resetTimer();
    }

    @AllArgsConstructor
    static class StatTableRow {
        public String dateStr;
        public String opponent;
        public double duration;
    }
}
