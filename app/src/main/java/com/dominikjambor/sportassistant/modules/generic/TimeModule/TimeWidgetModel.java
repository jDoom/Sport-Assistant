package com.dominikjambor.sportassistant.modules.generic.TimeModule;

import android.annotation.SuppressLint;
import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.dominikjambor.sportassistant.R;
import com.dominikjambor.sportassistant.logic.ApplicationState;
import com.dominikjambor.sportassistant.ui.utils.SingleLiveEvent;

import java.time.LocalDateTime;

import lombok.Getter;

import static com.dominikjambor.sportassistant.logic.SportEvent.EventState.PAUSED;
import static com.dominikjambor.sportassistant.logic.SportEvent.EventState.STARTED;

class TimeWidgetModel {
    private final TimeModuleModel model;
    private final MutableLiveData<String> timeString;
    private final MutableLiveData<String> buttonString;
    private Runnable updater = new Runnable() {
        @Override
        public void run() {
            long durationInMillis;
            while (true) {
                synchronized (model) {
                    durationInMillis = model.getUpdatedDuration(LocalDateTime.now());
                }
                synchronized (timeString) {
                    timeString.postValue(formatMillis(durationInMillis));
                }
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    return;
                }
            }
        }
    };
    private Thread updateThread;

    @SuppressLint("DefaultLocale")
    static String formatMillis(long millis) {
        long tenths = (millis / 100) % 10;
        long second = (millis / 1000) % 60;
        long minute = (millis / (1000 * 60)) % 60;
        long hour = (millis / (1000 * 60 * 60)) % 24;

        return String.format("%02d:%02d:%02d.%d", hour, minute, second, tenths);
    }

    TimeWidgetModel(TimeModuleModel model) {
        this.model = model;
        timeString = new MutableLiveData<>("00:00:00.0");
        buttonString = new MutableLiveData<>();
        updateThread = new Thread(updater);
        updateStartPauseButtonText();
    }

    private void updateThread(boolean updaterState) {
        if (!updaterState) {
            updateThread.interrupt();
            try {
                updateThread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } else {
            if (updateThread == null || !updateThread.isAlive()) {
                updateThread = new Thread(updater);
                updateThread.start();
            }
        }
    }

    @Getter private SingleLiveEvent<Void> startBlinkCommand = new SingleLiveEvent<>();
    @Getter private SingleLiveEvent<Void> stopBlinkCommand = new SingleLiveEvent<>();

    void pauseClicked() {
        switch (model.getState()) {
            case CREATED:
                model.start();
                break;
            case PAUSED:
                model.resume();
                break;
            case STARTED:
                model.pause();
        }
        updateThread(model.getState() == STARTED);
        updateStartPauseButtonText();
        if (model.getState() == PAUSED) {
            startBlinkCommand.call();
        } else {
            stopBlinkCommand.call();
        }
    }

    LiveData<String> getTimeString() {
        return timeString;
    }

    @Getter SingleLiveEvent<Void> enableButtonCommand = new SingleLiveEvent<>();

    private void updateStartPauseButtonText() {
        Context context = ApplicationState.getInstance().getContext();
        switch (model.getState()) {
            case STARTED:
                buttonString.postValue(context.getString(R.string.pause));
                enableButtonCommand.call();
                break;
            case PAUSED:
                buttonString.postValue(context.getString(R.string.continuestr));
                break;
            default:
                buttonString.postValue(context.getString(R.string.start));
        }
    }

    LiveData<String> getStartPauseButtonText() {
        return buttonString;
    }

    void eventStateChanged() {
        updateStartPauseButtonText();
        updateThread(model.getState() == STARTED);
        if (model.getState() == PAUSED) {
            startBlinkCommand.call();
        } else {
            stopBlinkCommand.call();
        }
    }
}
