package com.dominikjambor.sportassistant.modules.generic.TimeModule;

import android.view.View;

import androidx.fragment.app.Fragment;

import com.dominikjambor.sportassistant.logic.EventTimeProvider;
import com.dominikjambor.sportassistant.logic.SportEvent;
import com.dominikjambor.sportassistant.modules.Module;

import org.json.JSONObject;

import lombok.NoArgsConstructor;

import static com.dominikjambor.sportassistant.modules.generic.TimeModule.TimeWidgetModel.formatMillis;

@NoArgsConstructor
public class TimeModule extends Module implements EventTimeProvider {

    private TimeModuleModel model;
    private TimeWidgetModel widgetModel;

    public TimeModule(SportEvent event) {
        super(event);
        model = new TimeModuleModel();
        widgetModel = new TimeWidgetModel(model);
    }

    @Override
    public View createWidgetView(Fragment parent) {
        return new TimeWidgetView(widgetModel, parent);
    }

    @Override
    public Fragment createConfigurationFragment(JSONObject preset) {
        return null;
    }

    @Override
    public boolean saveConfiguration() {
        return true;
    }

    @Override
    public Fragment createFinalizationFragment() {
        return null;
    }

    @Override
    public boolean saveFinalization() {
        return true;
    }

    @Override
    public void onEventStarted() {
        model.start();
        widgetModel.eventStateChanged();
    }

    @Override
    public void onEventEnded() {
        model.end();
        widgetModel.eventStateChanged();
        log("", "{ resultText : \"Match duration: " + formatMillis(model.getDuration()) + "\" }");
    }

    @Override
    public void onEventPaused() {
        model.pause();
        widgetModel.eventStateChanged();
    }

    @Override
    public void onEventResumed() {
        model.resume();
        widgetModel.eventStateChanged();
    }

    @Override
    public long getCleanDuration() {
        return model.getDuration();
    }

    @Override
    public long getFullDuration() {
        return model.getFullDuration();
    }

    @Override
    public String getLogTimeStampText() {
        return formatMillis(model.getDuration());
    }
}
