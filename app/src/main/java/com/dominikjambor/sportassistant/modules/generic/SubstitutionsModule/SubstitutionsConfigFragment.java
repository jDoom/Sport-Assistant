package com.dominikjambor.sportassistant.modules.generic.SubstitutionsModule;

import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.dominikjambor.sportassistant.R;
import com.dominikjambor.sportassistant.data.Player;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SubstitutionsConfigFragment extends Fragment {
    private SubstitutionsWidgetModel model;
    private JSONObject preset;

    private List<Player> selections1 = new ArrayList<>();
    private List<Player> selections2 = new ArrayList<>();

    private List<LinearLayout> places1 = new ArrayList<>();
    private List<LinearLayout> places2 = new ArrayList<>();

    @BindView(R.id.generic_substitutions_config_placesNumber) TextView countText;
    @BindView(R.id.generic_substitutions_config_team1Name) TextView team1Name;
    @BindView(R.id.generic_substitutions_config_placesContainer1) LinearLayout placesContainer1;
    @BindView(R.id.generic_substitutions_config_team2Name) TextView team2Name;
    @BindView(R.id.generic_substitutions_config_placesContainer2) LinearLayout placesContainer2;

    public SubstitutionsConfigFragment() {

    }

    public SubstitutionsConfigFragment(SubstitutionsWidgetModel model, JSONObject preset) {
        this.model = model;
        this.preset = preset;
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.module_generic_substitution_configuration, container, false);

        ButterKnife.bind(this, root);
        team1Name.setText(model.getTeam1().team.getName());
        team2Name.setText(model.getTeam2().team.getName());

        if (preset != null) {
            try {
                if (preset.has("courtCount"))
                    model.setCourtCount(preset.getInt("courtCount"));
            } catch (Exception e) {
                Toast.makeText(requireContext(), "Error loading preset", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }

        countText.setText(String.valueOf(model.getCourtCount()));
        updateContainers();

        return root;
    }

    private void updateContainers() {
        while (places1.size() < model.getCourtCount()) {
            addPlace(placesContainer1, places1, selections1, model.getTeam1Available());
            addPlace(placesContainer2, places2, selections2, model.getTeam2Available());
        }
        while (places1.size() > model.getCourtCount()) {
            removePlace(placesContainer1, places1, selections1, model.getTeam1Available());
            removePlace(placesContainer2, places2, selections2, model.getTeam2Available());
        }
    }

    private void addPlace(LinearLayout placesContainer, List<LinearLayout> places, List<Player> selections, List<Player> availablePlayers) {
        LinearLayout newPlace = new LinearLayout(requireContext());
        newPlace.setLayoutParams(new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        ));
        newPlace.setOrientation(LinearLayout.HORIZONTAL);

        Player toAdd = availablePlayers.size() > 0 ? availablePlayers.remove(availablePlayers.size() - 1) : null;
        selections.add(toAdd);

        TextView placeTextView = new TextView(requireContext());
        placeTextView.setText(toAdd == null ? "Empty" : toAdd.getName());
        placeTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
        ViewGroup.MarginLayoutParams params = new ViewGroup.MarginLayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMarginStart(50);
        placeTextView.setLayoutParams(params);

        final int p = places.size();
        newPlace.addView(placeTextView);
        newPlace.setOnClickListener(v -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(requireContext());
            builder.setTitle("Choose a player!");

            String[] names = availablePlayers.stream().map(Player::getName).toArray(String[]::new);
            names = addElement(names, "Empty");
            builder.setItems(names, (dialog, position)
                    -> {
                Player currentSelection = selections.get(p);
                if (position == availablePlayers.size() && currentSelection != null) {
                    availablePlayers.add(currentSelection);
                    selections.set(p, null);
                    ((TextView) places.get(p).getChildAt(0)).setText("Empty");
                } else if (position < availablePlayers.size()) {
                    if (currentSelection != null) {
                        availablePlayers.add(currentSelection);
                    }
                    Player newSelection = availablePlayers.get(position);
                    selections.set(p, newSelection);
                    availablePlayers.remove(position);
                    ((TextView) places.get(p).getChildAt(0)).setText(newSelection.getName());
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        });

        placesContainer.addView(newPlace);
        places.add(newPlace);
    }

    private void removePlace(LinearLayout placesContainer, List<LinearLayout> places, List<Player> selections, List<Player> availablePlayers) {
        Player selection = selections.get(selections.size() - 1);
        if (selection != null) {
            availablePlayers.add(selection);
        }
        selections.remove(selection);
        placesContainer.removeView(places.get(places.size() - 1));
        places.remove(places.size() - 1);
    }

    JSONObject createPreset() {
        JSONObject preset = new JSONObject();
        try {
            preset.put("courtCount", model.getCourtCount());
        } catch (JSONException e) {
            Toast.makeText(requireContext(), "Error creating preset", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }

        return preset;
    }

    static String[] addElement(String[] a, String e) {
        a = Arrays.copyOf(a, a.length + 1);
        a[a.length - 1] = e;
        return a;
    }

    @OnClick(R.id.generic_substitutions_config_plusPlayer)
    void onPlusClicked() {
        model.incCourtCount();
        updateContainers();
        countText.setText(String.valueOf(model.getCourtCount()));
    }

    @OnClick(R.id.generic_substitutions_config_minusPlayer)
    void onMinusClicked() {
        model.decCourtCount();
        updateContainers();
        countText.setText(String.valueOf(model.getCourtCount()));
    }

    boolean validateAndSave() {
        selections1.forEach(player -> {
            if (player != null) {
                model.getTeam1Available().remove(player);
                model.getTeam1OnCourt().add(player);
            }
        });
        selections2.forEach(player -> {
            if (player != null) {
                model.getTeam2Available().remove(player);
                model.getTeam2OnCourt().add(player);
            }
        });
        return true;
    }
}
