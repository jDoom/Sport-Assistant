package com.dominikjambor.sportassistant.modules.basketball.BasketballShotClockModule;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.dominikjambor.sportassistant.R;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BasketballShotClockConfigFragment extends Fragment {

    @BindView(R.id.basketball_shotclock_config_seconds) TextView secondsText;

    private ShotClockWidgetModel model;
    private JSONObject preset;

    public BasketballShotClockConfigFragment(ShotClockWidgetModel model, JSONObject preset) {
        this.model = model;
        this.preset = preset;
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.module_basketball_shotclock_configuration, container, false);

        ButterKnife.bind(this, root);

        if (preset != null) {
            try {
                if (preset.has("initialValue"))
                    model.setInitialSeconds(preset.getInt("initialValue"));
            } catch (Exception e) {
                Toast.makeText(requireContext(), "Error loading preset", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }
        secondsText.setText(String.valueOf(model.getInitialSeconds()));

        return root;
    }

    @OnClick(R.id.basketball_shotclock_config_plus)
    void plusClicked() {
        model.incInitialSeconds();
        secondsText.setText(String.valueOf(model.getInitialSeconds()));
    }

    @OnClick(R.id.basketball_shotclock_config_minus)
    void minusClicked() {
        model.decInitialSeconds();
        secondsText.setText(String.valueOf(model.getInitialSeconds()));
    }

    JSONObject createPreset() {
        JSONObject preset = new JSONObject();
        try {
            preset.put("initialValue", model.getInitialSeconds());
        } catch (JSONException e) {
            Toast.makeText(requireContext(), "Error creating preset", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }

        return preset;
    }
}
