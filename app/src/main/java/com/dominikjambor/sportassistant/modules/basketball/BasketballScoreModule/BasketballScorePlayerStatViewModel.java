package com.dominikjambor.sportassistant.modules.basketball.BasketballScoreModule;

import androidx.lifecycle.ViewModel;

import com.dominikjambor.sportassistant.data.Player;
import com.dominikjambor.sportassistant.logic.ApplicationState;
import com.dominikjambor.sportassistant.modules.ModuleUtils.StatTableRow;
import com.dominikjambor.sportassistant.persistence.SportAssistantPersistence;
import com.jjoe64.graphview.series.DataPoint;

import org.json.JSONException;
import org.json.JSONObject;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import io.reactivex.Single;
import lombok.Getter;
import lombok.Setter;

import static com.dominikjambor.sportassistant.logic.SportEvent.SportType.BASKETBALL;
import static com.dominikjambor.sportassistant.modules.ModuleUtils.round;

public class BasketballScorePlayerStatViewModel extends ViewModel {
    private Single<List<Player>> allPlayers;
    private Single<List<DataPoint>> teamPoints;
    @Getter private int total = 0;
    @Getter private double avg = 0;
    @Getter private List<StatTableRow> teamTable;
    @Getter private List<String> teamOpponents;
    @Getter
    @Setter
    private Player player;

    Single<List<DataPoint>> getTeamPoints() {
        if (teamPoints == null) {
            SportAssistantPersistence persistence = ApplicationState.getInstance().getPersistence();
            teamPoints = persistence.findAllSportEventsWithLogs(BASKETBALL, player.getTeamId()).map(events -> {
                teamTable = new ArrayList<>();
                teamOpponents = new ArrayList<>();
                List<DataPoint> points = new ArrayList<>();
                AtomicInteger x = new AtomicInteger();
                events.forEach(eventWithLogs -> {
                    AtomicInteger sum = new AtomicInteger();
                    eventWithLogs.logEntries.stream()
                            .filter(entry -> entry.getAuthor().equals(BasketballScoreModule.class.getName()))
                            .forEach(entry -> {
                                if (entry.getData() != null && !entry.getData().isEmpty()) {
                                    try {
                                        JSONObject data = new JSONObject(entry.getData());
                                        if (Objects.equals(data.getString("playerId"), player.getId().toString()) &&
                                                Objects.equals(data.getString("teamId"), player.getTeamId().toString())) {
                                            sum.addAndGet(data.getInt("value"));
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                    teamOpponents.add(player.getTeamId().equals(eventWithLogs.event.getTeam1Id()) ?
                            eventWithLogs.event.getTeam2() : eventWithLogs.event.getTeam1());
                    points.add(new DataPoint(x.get(), sum.get()));

                    teamTable.add(new StatTableRow(eventWithLogs.event.getStartDateTime()
                            .format(DateTimeFormatter.ofPattern("YYYY.MM.dd")),
                            teamOpponents.get(teamOpponents.size() - 1),
                            sum.get()));

                    x.addAndGet(1);
                });
                return points;
            });
        }
        return teamPoints;
    }

    private Single<List<DataPoint>> allPoints;
    @Getter private List<StatTableRow> allTable;
    @Getter private List<String> allOpponents;

    Single<List<DataPoint>> getAllPoints() {
        if (allPoints == null) {
            SportAssistantPersistence persistence = ApplicationState.getInstance().getPersistence();
            allPoints = persistence.findAllSportEventsWithLogs(BASKETBALL).map(events -> {
                total = 0;
                allTable = new ArrayList<>();
                allOpponents = new ArrayList<>();
                List<DataPoint> points = new ArrayList<>();
                AtomicInteger x = new AtomicInteger();
                events.forEach(eventWithLogs -> {
                    AtomicInteger sum = new AtomicInteger();
                    AtomicReference<String> playerCurrentTeam = new AtomicReference<>("");
                    eventWithLogs.logEntries.stream()
                            .filter(entry -> entry.getAuthor().equals(BasketballScoreModule.class.getName()))
                            .forEach(entry -> {
                                if (entry.getData() != null && !entry.getData().isEmpty()) {
                                    try {
                                        JSONObject data = new JSONObject(entry.getData());
                                        if (Objects.equals(data.getString("playerId"), player.getId().toString())) {
                                            sum.addAndGet(data.getInt("value"));
                                            playerCurrentTeam.set(data.getString("teamId"));
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                    if (sum.get() > 0) {
                        allOpponents.add(playerCurrentTeam.get().equals(eventWithLogs.event.getTeam1Id().toString()) ?
                                eventWithLogs.event.getTeam2() : eventWithLogs.event.getTeam1());
                        points.add(new DataPoint(x.get(), sum.get()));

                        allTable.add(new StatTableRow(eventWithLogs.event.getStartDateTime()
                                .format(DateTimeFormatter.ofPattern("YYYY.MM.dd")),
                                allOpponents.get(allOpponents.size() - 1),
                                sum.get()));

                        x.addAndGet(1);
                    }
                    total += sum.get();
                    avg = round((double) total / points.size(), 1);
                });
                return points;
            });
        }
        return allPoints;
    }

    Single<List<Player>> getAllPlayers() {
        if (allPlayers == null) {
            SportAssistantPersistence persistence = ApplicationState.getInstance().getPersistence();
            allPlayers = persistence.findAllPlayers();
        }
        return allPlayers;
    }
}
