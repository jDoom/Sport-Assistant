package com.dominikjambor.sportassistant.modules;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class ModuleManager {
    private static ModuleManager instance;

    public static ModuleManager getInstance() {
        if (instance == null)
            instance = new ModuleManager();
        return instance;
    }

    private ModuleManager() {
        Map<Class<? extends Module>, ModuleConfiguration> configurationMap = new HashMap<>();
        MODULES.forEach(module -> {
            try {
                configurationMap.put(module, module.newInstance().getConfiguration());
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
        configs = ImmutableMap.copyOf(configurationMap);
    }

    private final Map<Class<? extends Module>, ModuleConfiguration> configs;

    public ModuleConfiguration getConfigurationFor(Class<? extends Module> moduleClass) {
        return Objects.requireNonNull(configs.get(moduleClass));
    }

    //GROUPED BY SPORT, GENERIC FIRST
    public static final ImmutableList<Class<? extends Module>> MODULES = ImmutableList.of(
            com.dominikjambor.sportassistant.modules.generic.TimeModule.TimeModule.class,
            com.dominikjambor.sportassistant.modules.generic.SubstitutionsModule.SubstitutionsModule.class,
            com.dominikjambor.sportassistant.modules.generic.NoteModule.NoteModule.class,

            com.dominikjambor.sportassistant.modules.basketball.BasketballScoreModule.BasketballScoreModule.class,
            com.dominikjambor.sportassistant.modules.basketball.BasketballShotClockModule.BasketballShotClockModule.class,

            com.dominikjambor.sportassistant.modules.ultimate.UltimateScoreModule.UltimateScoreModule.class,

            com.dominikjambor.sportassistant.modules.soccer.SoccerScoreModule.SoccerScoreModule.class,
            com.dominikjambor.sportassistant.modules.soccer.SoccerPossessionModule.SoccerPossessionModule.class,

            com.dominikjambor.sportassistant.modules.volleyball.VolleyballScoreModule.VolleyballScoreModule.class
    );
}
