package com.dominikjambor.sportassistant.modules.generic.SubstitutionsModule;

import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.dominikjambor.sportassistant.R;
import com.dominikjambor.sportassistant.data.Player;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.dominikjambor.sportassistant.modules.generic.SubstitutionsModule.SubstitutionsConfigFragment.addElement;
import static com.dominikjambor.sportassistant.ui.utils.Utils.setImageButtonEnabled;

public class SubstitutionsWidgetView extends FrameLayout {

    SubstitutionsWidgetModel model;

    @BindView(R.id.generic_substitutions_widget_team1Swap) ImageButton swapButton1;
    @BindView(R.id.generic_substitutions_widget_team2Swap) ImageButton swapButton2;
    @BindView(R.id.generic_substitutions_widget_team1Name) TextView team1Name;
    @BindView(R.id.generic_substitutions_widget_team2Name) TextView team2Name;

    SubstitutionsWidgetView(SubstitutionsWidgetModel model, Fragment container) {
        super(container.requireContext());
        this.model = model;

        addView(inflate(container.getContext(), R.layout.module_generic_substitution_widget, null));

        ButterKnife.bind(this);

        team1Name.setText(model.getTeam1().team.getName());
        team2Name.setText(model.getTeam2().team.getName());

        toggleButtons(false);
    }

    private void viewPlayers(List<Player> onCourt) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Players on the court");

        String[] names = onCourt.stream()
                .map(p -> p.getName() + (p.getJerseyNumber().isEmpty() ? "" : " (" + p.getJerseyNumber() + ")"))
                .toArray(String[]::new);
        builder.setItems(names, null);

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void toggleButtons(boolean enabled) {
        setImageButtonEnabled(getContext(), enabled, swapButton1, R.drawable.ic_swap);
        setImageButtonEnabled(getContext(), enabled, swapButton2, R.drawable.ic_swap);
    }

    @OnClick(R.id.generic_substitutions_widget_team1View)
    void team1ViewClicked() {
        viewPlayers(model.getTeam1OnCourt());
    }

    @OnClick(R.id.generic_substitutions_widget_team2View)
    void team2ViewClicked() {
        viewPlayers(model.getTeam2OnCourt());
    }

    @OnClick(R.id.generic_substitutions_widget_team1Swap)
    void swapButton1Clicked() {
        getWhoGoesDown(model.getTeam1Available(), model.getTeam1OnCourt());
    }

    @OnClick(R.id.generic_substitutions_widget_team2Swap)
    void swapButton2Clicked() {
        getWhoGoesDown(model.getTeam2Available(), model.getTeam2OnCourt());
    }

    void getWhoGoesDown(List<Player> availablePlayers, List<Player> playersOnCourt) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Who comes down?");

        String[] names = playersOnCourt.stream()
                .map(p -> p.getName() + (p.getJerseyNumber().isEmpty() ? "" : " (" + p.getJerseyNumber() + ")"))
                .toArray(String[]::new);
        if (playersOnCourt.size() < model.getCourtCount()) {
            names = addElement(names, "Nobody");
        }
        builder.setItems(names, (dialog, position) -> {
            Player down = null;
            if (position < playersOnCourt.size()) {
                down = playersOnCourt.get(position);
            }
            getWhoGoesUp(availablePlayers, down);
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void getWhoGoesUp(List<Player> availablePlayers, Player down) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Who goes up?");

        String[] names = availablePlayers.stream()
                .map(p -> p.getName() + (p.getJerseyNumber().isEmpty() ? "" : " (" + p.getJerseyNumber() + ")"))
                .toArray(String[]::new);
        names = addElement(names, "Nobody");
        builder.setItems(names, (dialog, position) -> {
            Player up = null;
            if (position < availablePlayers.size()) {
                up = availablePlayers.get(position);
            }
            doSwap(down, up);
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void doSwap(Player down, Player up) {
        model.swap(down, up);
        Toast.makeText(getContext(), "Substitution done.", Toast.LENGTH_SHORT).show();
    }
}
