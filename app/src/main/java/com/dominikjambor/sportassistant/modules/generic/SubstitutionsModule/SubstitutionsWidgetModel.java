package com.dominikjambor.sportassistant.modules.generic.SubstitutionsModule;

import com.dominikjambor.sportassistant.data.Player;
import com.dominikjambor.sportassistant.data.Team;
import com.dominikjambor.sportassistant.data.TeamWithPlayers;
import com.dominikjambor.sportassistant.logic.SportEvent;

import org.json.JSONObject;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

import static java.lang.Math.round;

class SubstitutionsWidgetModel {
    SubstitutionsWidgetModel(SubstitutionsModule module, SportEvent event) {
        this.module = module;
        team1 = event.getTeam1();
        team2 = event.getTeam2();

        team1Available.addAll(team1.players);
        team2Available.addAll(team2.players);

        team1Millis = new ArrayList<>();
        team1.players.forEach(p -> team1Millis.add(0));
        team2Millis = new ArrayList<>();
        team2.players.forEach(p -> team2Millis.add(0));

        startDate = LocalDateTime.now().toString();
        eventName = event.getName();
    }

    private String startDate;
    private String eventName;

    @Getter
    @Setter
    private int courtCount = 1;

    private SubstitutionsModule module;
    @Getter private final TeamWithPlayers team1;
    @Getter private final TeamWithPlayers team2;

    private List<Integer> team1Millis;
    private List<Integer> team2Millis;

    @Getter private List<Player> team1OnCourt = new ArrayList<>();
    @Getter private List<Player> team1Available = new ArrayList<>();

    @Getter private List<Player> team2OnCourt = new ArrayList<>();
    @Getter private List<Player> team2Available = new ArrayList<>();

    private Instant start;

    void incCourtCount() {
        if (courtCount < 20) {
            courtCount++;
        }
    }

    void decCourtCount() {
        if (courtCount > 1) {
            courtCount--;
        }
    }

    void resetTimer() {
        start = Instant.now();
    }

    void updateTimes() {
        long elapsedSeconds = ChronoUnit.MILLIS.between(start, Instant.now());

        for (int i = 0; i < team1.players.size(); i++) {
            if (team1OnCourt.contains(team1.players.get(i))) {
                team1Millis.set(i, team1Millis.get(i) + (int) elapsedSeconds);
            }
        }
        for (int i = 0; i < team2.players.size(); i++) {
            if (team2OnCourt.contains(team2.players.get(i))) {
                team2Millis.set(i, team2Millis.get(i) + (int) elapsedSeconds);
            }
        }
        resetTimer();
    }

    void swap(Player down, Player up) {
        if (down == null && up == null)
            return;
        updateTimes();

        List<Player> availablePlayers = getTeam1().players.contains(down != null ? down : up) ? team1Available : team2Available;
        List<Player> playersOnCourt = getTeam1().players.contains(down != null ? down : up) ? team1OnCourt : team2OnCourt;

        if (down != null) {
            playersOnCourt.remove(down);
            availablePlayers.add(down);
        }
        if (up != null) {
            availablePlayers.remove(up);
            playersOnCourt.add(up);
        }

        Team team = getTeam1().players.contains(down != null ? down : up) ? team1.team : team2.team;
        module.log("Substitution in " + team.getName() +
                ", Down: " + (down == null ? "-" : down.getName()) +
                ", Up: " + (up == null ? "-" : up.getName()), "{}");
    }

    void logTimes() {
        JSONObject logData = new JSONObject();
        try {
            logData.put("dateTime", startDate);
            logData.put("eventName", eventName);
            for (int i = 0; i < team1.players.size(); i++) {
                logData.put(team1.players.get(i).getId().toString(), round(team1Millis.get(i) / 1000.0f));
            }
            for (int i = 0; i < team2.players.size(); i++) {
                logData.put(team2.players.get(i).getId().toString(), round(team2Millis.get(i) / 1000.0f));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        module.log("", logData.toString());
    }
}
