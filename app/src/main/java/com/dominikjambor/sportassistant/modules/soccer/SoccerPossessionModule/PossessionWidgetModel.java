package com.dominikjambor.sportassistant.modules.soccer.SoccerPossessionModule;

import com.dominikjambor.sportassistant.data.TeamWithPlayers;
import com.dominikjambor.sportassistant.logic.SportEvent;
import com.dominikjambor.sportassistant.ui.utils.SingleLiveEvent;

import org.json.JSONObject;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import lombok.Getter;

import static com.dominikjambor.sportassistant.modules.soccer.SoccerPossessionModule.PossessionWidgetModel.BallStatus.NONE;
import static com.dominikjambor.sportassistant.modules.soccer.SoccerPossessionModule.PossessionWidgetModel.BallStatus.TEAM_1;
import static com.dominikjambor.sportassistant.modules.soccer.SoccerPossessionModule.PossessionWidgetModel.BallStatus.TEAM_2;
import static java.lang.Math.round;

class PossessionWidgetModel {
    private long team1Millis = 0;
    private long team2Millis = 0;

    boolean paused = false;

    private BallStatus ballStatus = NONE;

    private Instant start = Instant.now();

    enum BallStatus {
        NONE,
        TEAM_1,
        TEAM_2
    }

    private SoccerPossessionModule module;
    @Getter private TeamWithPlayers team1;
    @Getter private TeamWithPlayers team2;
    private String startDate;

    PossessionWidgetModel(SoccerPossessionModule module, SportEvent event) {
        this.module = module;
        team1 = event.getTeam1();
        team2 = event.getTeam2();

        startDate = LocalDateTime.now().toString();
    }

    @Getter private SingleLiveEvent<Void> enableButtonsCommand = new SingleLiveEvent<>();

    void start() {
        enableButtonsCommand.call();
    }

    void changeStatusTo(BallStatus status) {
        update();
        ballStatus = status;
    }

    void pause() {
        update();
        paused = true;
    }

    void resume() {
        start = Instant.now();
        paused = false;
    }

    void logTimes(boolean update) {
        if (update) update();
        JSONObject logData = new JSONObject();
        try {
            logData.put("dateTime", startDate);
            logData.put(team1.team.getId().toString(), round(team1Millis / 1000.0f));
            logData.put(team2.team.getId().toString(), round(team2Millis / 1000.0f));
            logData.put("sum", round(team1Millis / 1000.0f) + round(team2Millis / 1000.0f));
            logData.put("team1", team1.team.getName());
            logData.put("team2", team2.team.getName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        module.log("", logData.toString());

        long team1Percent = round(((double) team1Millis / (team1Millis + team2Millis)) * 100);
        long team2Percent = round(((double) team2Millis / (team1Millis + team2Millis)) * 100);

        module.log("", "{ resultText : \"Possession rates: " +
                team1.team.getName() + " [" + team1Percent + "% - " +
                team2Percent + "%] " + team2.team.getName() + "\" }");
    }

    private void update() {
        if (!paused) {
            if (ballStatus == TEAM_1) {
                team1Millis += ChronoUnit.MILLIS.between(start, Instant.now());
            } else if (ballStatus == TEAM_2) {
                team2Millis += ChronoUnit.MILLIS.between(start, Instant.now());
            }
        }
        start = Instant.now();
    }
}
