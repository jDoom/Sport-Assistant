package com.dominikjambor.sportassistant.modules.generic.SubstitutionsModule;

import androidx.lifecycle.ViewModel;

import com.dominikjambor.sportassistant.data.Player;
import com.dominikjambor.sportassistant.logic.ApplicationState;
import com.dominikjambor.sportassistant.modules.generic.SubstitutionsModule.SubstitutionsModule.StatTableRow;
import com.dominikjambor.sportassistant.persistence.SportAssistantPersistence;
import com.jjoe64.graphview.series.DataPoint;

import org.json.JSONException;
import org.json.JSONObject;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import io.reactivex.Single;
import lombok.Getter;
import lombok.Setter;

import static com.dominikjambor.sportassistant.modules.ModuleUtils.round;

public class SubstitutionPlayerStatViewModel extends ViewModel {
    private Single<List<Player>> allPlayers;
    private double total = 0;
    @Getter
    private double avg = 0;
    @Getter
    @Setter
    private Player player;

    private Single<List<DataPoint>> dataPoints;
    @Getter private List<StatTableRow> table;
    @Getter private List<String> dates;

    Single<List<DataPoint>> getDataPoints() {
        if (dataPoints == null) {
            SportAssistantPersistence persistence = ApplicationState.getInstance().getPersistence();
            dataPoints = persistence.findLogEntriesByAuthor(SubstitutionsModule.class.getName()).map(entries -> {
                total = 0;
                table = new ArrayList<>();
                dates = new ArrayList<>();
                List<DataPoint> points = new ArrayList<>();
                AtomicInteger x = new AtomicInteger();
                entries.forEach(entry -> {
                    if (entry.getData() != null && !entry.getData().isEmpty()) {
                        try {
                            JSONObject data = new JSONObject(entry.getData());
                            if (data.has(player.getId().toString())) {
                                double val = round(data.getInt(player.getId().toString()) / 60.0, 1);
                                points.add(new DataPoint(x.get(), val));
                                total += val;
                                avg = round(total / points.size(), 1);

                                LocalDateTime dateTime = LocalDateTime.parse(data.getString("dateTime"));
                                String longDate = dateTime.format(DateTimeFormatter.ofPattern("YYYY/MM/dd"));
                                String shortDate = dateTime.format(DateTimeFormatter.ofPattern("YY/MM/dd"));

                                dates.add(shortDate);
                                table.add(new StatTableRow(
                                        longDate,
                                        data.getString("eventName"),
                                        val
                                ));
                                x.addAndGet(1);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                return points;
            });
        }
        return dataPoints;
    }

    String getTotal() {
        return String.valueOf(round(total, 1));
    }

    Single<List<Player>> getAllPlayers() {
        if (allPlayers == null) {
            SportAssistantPersistence persistence = ApplicationState.getInstance().getPersistence();
            allPlayers = persistence.findAllPlayers();
        }
        return allPlayers;
    }
}
