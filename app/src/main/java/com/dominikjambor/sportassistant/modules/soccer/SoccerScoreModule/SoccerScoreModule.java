package com.dominikjambor.sportassistant.modules.soccer.SoccerScoreModule;

import android.view.View;

import androidx.fragment.app.Fragment;

import com.dominikjambor.sportassistant.logic.SportEvent;
import com.dominikjambor.sportassistant.modules.Module;

import org.json.JSONObject;

import lombok.AllArgsConstructor;

public class SoccerScoreModule extends Module {

    private SoccerScoreConfigFragment configFragment;
    private ScoreWidgetView widgetView;

    public SoccerScoreModule() {
    }

    public SoccerScoreModule(SportEvent event) {
        super(event);
        model = new ScoreWidgetModel(this, event);
    }

    private ScoreWidgetModel model;

    @Override
    public View createWidgetView(Fragment parent) {
        widgetView = new ScoreWidgetView(model, parent);
        return widgetView;
    }

    @Override
    public Fragment createConfigurationFragment(JSONObject preset) {
        configFragment = new SoccerScoreConfigFragment(model, preset);
        return configFragment;
    }

    @Override
    public Fragment createPlayerStatFragment() {
        return new SoccerScorePlayerStatFragment();
    }

    @Override
    public Fragment createTeamStatFragment() {
        return new SoccerScoreTeamStatFragment();
    }

    @Override
    public JSONObject createPreset() {
        return configFragment.createPreset();
    }

    @Override
    public boolean saveConfiguration() {
        return configFragment.validateAndSave();
    }

    @Override
    public void onEventStarted() {
        widgetView.toggleButtons(true);
    }

    @Override
    public void onEventEnded() {
        log("", "{ resultText : \"Result: " +
                model.getTeam1().team.getName() + "   " + model.getTeam1Score().getValue() + " - " +
                model.getTeam2Score().getValue() + "   " + model.getTeam2().team.getName() + "\" }");
    }

    @Override
    public void onEventPaused() {
        widgetView.toggleButtons(false);
    }

    @Override
    public void onEventResumed() {
        widgetView.toggleButtons(true);
    }

    @AllArgsConstructor
    static class StatTableRow {
        public String dateStr;
        public String opponent;
        public int score;
    }
}
