package com.dominikjambor.sportassistant.modules.basketball.BasketballShotClockModule;

import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.dominikjambor.sportassistant.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

class ShotClockWidgetView extends FrameLayout {
    private final ShotClockWidgetModel model;
    @BindView(R.id.basketball_shotclock_widget_time) TextView timeText;
    @BindView(R.id.basketball_shotclock_widget_pauseButton) Button pauseButton;

    ShotClockWidgetView(ShotClockWidgetModel model, Fragment container) {
        super(container.requireContext());

        addView(inflate(container.getContext(), R.layout.module_basketball_shotclock_widget, null));
        ButterKnife.bind(this);

        Animation blink = new AlphaAnimation(1.0f, 0.0f);
        blink.setDuration(500);
        blink.setInterpolator(Math::round);
        blink.setRepeatMode(Animation.REVERSE);

        this.model = model;
        model.getTimeString().observe(container, timeText::setText);
        model.getPauseButtonText().observe(container, pauseButton::setText);

        pauseButton.setOnClickListener(v -> model.pause());
        model.getEnablePauseCommand().observe(container, o -> pauseButton.setEnabled(true));
    }

    @OnClick(R.id.basketball_shotclock_widget_resetButton)
    void resetClicked() {
        model.reset();
    }

    @OnClick(R.id.basketball_shotclock_widget_logButton)
    void logClicked() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Choose team!");

        String[] names = {model.getTeam1().team.getName(), model.getTeam2().team.getName()};
        builder.setItems(names, (dialog, position)
                -> {
            model.logViolation(position == 0 ? model.getTeam1() : model.getTeam2());
            Toast.makeText(getContext(), "Violation logged.", Toast.LENGTH_SHORT).show();
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
