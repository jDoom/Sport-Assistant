package com.dominikjambor.sportassistant.modules.soccer.SoccerScoreModule;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.dominikjambor.sportassistant.R;
import com.dominikjambor.sportassistant.data.Team;
import com.jjoe64.graphview.DefaultLabelFormatter;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.dominikjambor.sportassistant.modules.soccer.SoccerScoreModule.SoccerScoreModule.*;

public class SoccerScoreTeamStatFragment extends Fragment {

    @BindView(R.id.soccer_score_teamstat_playername) TextView playerName;
    @BindView(R.id.soccer_score_teamstat_total) TextView total;
    @BindView(R.id.soccer_score_teamstat_avg) TextView avg;

    @BindView(R.id.soccer_score_teamstat_graph) GraphView graph;
    @BindView(R.id.soccer_score_teamstat_table) LinearLayout container;

    private CompositeDisposable trash = new CompositeDisposable();
    private SoccerScoreTeamStatViewModel viewModel;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.module_soccer_score_teamstats, container, false);

        ButterKnife.bind(this, root);
        viewModel = new ViewModelProvider(requireParentFragment()).get(SoccerScoreTeamStatViewModel.class);

        if (viewModel.getTeam() == null) {
            trash.add(viewModel.getAllTeams()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(teams -> {
                        AlertDialog.Builder builder = new AlertDialog.Builder(requireContext());
                        builder.setTitle("Choose a team!");

                        String[] names = teams.stream().map(Team::getName).toArray(String[]::new);
                        builder.setItems(names, (dialog, position)
                                -> onTeamChoose(teams.get(position)));
                        builder.setCancelable(false);
                        AlertDialog dialog = builder.create();
                        dialog.show();
                    })
            );
        } else {
            onTeamChoose(viewModel.getTeam());
        }

        return root;
    }

    private void onTeamChoose(Team team) {
        playerName.setText(team.getName());
        viewModel.setTeam(team);
        trash.add(viewModel.getPoints()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dataPoints -> {
                            createGraph(dataPoints, graph);
                            createTable(viewModel.getTable(), container, "Score");
                            total.setText(String.format("Total: %s points.", String.valueOf(viewModel.getTotal())));
                            avg.setText(String.format("Average: %s points.", String.valueOf(viewModel.getAvg())));
                        },
                        Throwable::printStackTrace));
    }

    private void createTable(List<StatTableRow> rows, LinearLayout tableContainer, String thirdColumn) {
        tableContainer.removeAllViews();
        tableContainer.addView(createRow("Date", "Opponent", thirdColumn, true));
        rows.forEach(StatTableRow -> tableContainer.addView(createRow(StatTableRow.dateStr,
                StatTableRow.opponent,
                String.valueOf(StatTableRow.score),
                false)));
        if (rows.isEmpty()) {
            tableContainer.addView(createRow("",
                    requireContext().getString(R.string.no_data_to_show),
                    "", false));
        }
    }

    private LinearLayout createRow(String date, String opponent, String score, boolean isBold) {
        LinearLayout rowLayout = new LinearLayout(requireContext());
        rowLayout.setLayoutParams(new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        ));
        rowLayout.setOrientation(LinearLayout.HORIZONTAL);

        TextView dateTextView = new TextView(requireContext());
        dateTextView.setText(date);
        dateTextView.setLayoutParams(new LinearLayout.LayoutParams(
                0,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                0.25f
        ));
        if (isBold) dateTextView.setTypeface(null, Typeface.BOLD);
        rowLayout.addView(dateTextView);

        TextView opponentTextView = new TextView(requireContext());
        opponentTextView.setText(opponent);
        opponentTextView.setLayoutParams(new LinearLayout.LayoutParams(
                0,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                0.55f
        ));
        if (isBold) opponentTextView.setTypeface(null, Typeface.BOLD);
        rowLayout.addView(opponentTextView);

        TextView sumTextView = new TextView(requireContext());
        sumTextView.setText(String.valueOf(score));
        sumTextView.setLayoutParams(new LinearLayout.LayoutParams(
                0,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                0.2f
        ));
        if (isBold) sumTextView.setTypeface(null, Typeface.BOLD);
        rowLayout.addView(sumTextView);

        return rowLayout;
    }

    private void createGraph(List<DataPoint> dataPoints, GraphView graph) {
        DataPoint[] pointArray = new DataPoint[dataPoints.size()];
        LineGraphSeries<DataPoint> series =
                new LineGraphSeries<>(dataPoints.toArray(pointArray));
        graph.removeAllSeries();
        graph.addSeries(series);

        series.setDrawDataPoints(true);
        series.setAnimated(true);

        graph.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter() {
            @Override
            public String formatLabel(double value, boolean isValueX) {
                if (isValueX) {
                    if (value % 1 == 0 && (int) value < viewModel.getOpponents().size()) {
                        return viewModel.getOpponents().get((int) value);
                    } else {
                        return "";
                    }

                } else {
                    return super.formatLabel(value, false);
                }
            }
        });
        graph.getGridLabelRenderer().setNumHorizontalLabels(3);

        graph.getViewport().setYAxisBoundsManual(true);
        graph.getViewport().setMinY(0);
        if (!dataPoints.isEmpty()) {
            graph.getViewport().setMaxY(Collections.max(dataPoints, (o1, o2) ->
                    Double.compare(o1.getY(), o2.getY())).getY() + 1);
        } else {
            graph.getViewport().setMaxY(30);
        }

        graph.getViewport().setScalableY(false);
        graph.getViewport().setScrollableY(false);
        graph.getViewport().setScrollable(true);
        graph.getViewport().setScalable(true);

        graph.getGridLabelRenderer().setHumanRounding(true);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        trash.clear();
    }
}
