package com.dominikjambor.sportassistant.modules;

import com.dominikjambor.sportassistant.logic.SportEvent.SportType;

public interface ModuleConfiguration {
    String getModuleName();

    String getShortDescription();

    String getVersion();

    SportType getSportType();

    boolean hasPlayerStats();

    boolean hasTeamStats();

    boolean hasConfigFragment();

    boolean hasFinalFragment();
}