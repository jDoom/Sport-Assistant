package com.dominikjambor.sportassistant.modules.volleyball.VolleyballScoreModule;

import androidx.lifecycle.MutableLiveData;

import com.dominikjambor.sportassistant.data.TeamWithPlayers;
import com.dominikjambor.sportassistant.logic.SportEvent;

import lombok.Getter;

@Getter
class ScoreWidgetModel {

    ScoreWidgetModel(VolleyballScoreModule module, SportEvent event) {
        this.module = module;
        team1 = event.getTeam1();
        team2 = event.getTeam2();
    }

    private VolleyballScoreModule module;
    private final TeamWithPlayers team1;
    private final TeamWithPlayers team2;

    private MutableLiveData<Integer> team1Score = new MutableLiveData<>(0);
    private MutableLiveData<Integer> team2Score = new MutableLiveData<>(0);
    private MutableLiveData<Integer> team1SetScore = new MutableLiveData<>(0);
    private MutableLiveData<Integer> team2SetScore = new MutableLiveData<>(0);
    private int setNum = 1;

    void score(TeamWithPlayers team) {
        if (team == team1 && team1Score.getValue() != null) {
            team1Score.postValue(team1Score.getValue() + 1);
        } else if (team == team2 && team2Score.getValue() != null) {
            team2Score.postValue(team2Score.getValue() + 1);
        }

        module.log(team.team.getName() + " scored " + "a point", "{}");
    }

    void nextSet() {
        if (team1Score.getValue() == null || team2Score.getValue() == null
                || team1SetScore.getValue() == null || team2SetScore.getValue() == null) return;
        if (team1Score.getValue() >= team2Score.getValue()) {
            team1SetScore.setValue(team1SetScore.getValue() + 1);
        }
        if (team1Score.getValue() <= team2Score.getValue()) {
            team2SetScore.setValue(team2SetScore.getValue() + 1);
        }
        module.log("Set " + setNum + " ended. Result: " +
                team1.team.getName() + "   " + team1Score.getValue() + " - " +
                team2Score.getValue() + "   " + team2.team.getName(), "{}");
        team1Score.postValue(0);
        team2Score.postValue(0);
        setNum++;
    }
}
