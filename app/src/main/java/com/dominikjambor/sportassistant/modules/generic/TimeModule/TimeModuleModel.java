package com.dominikjambor.sportassistant.modules.generic.TimeModule;

import com.dominikjambor.sportassistant.logic.SportEvent.EventState;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import lombok.Getter;

import static com.dominikjambor.sportassistant.logic.SportEvent.EventState.CREATED;
import static com.dominikjambor.sportassistant.logic.SportEvent.EventState.ENDED;
import static com.dominikjambor.sportassistant.logic.SportEvent.EventState.PAUSED;
import static com.dominikjambor.sportassistant.logic.SportEvent.EventState.STARTED;

class TimeModuleModel {
    private LocalDateTime startDateTime;
    private LocalDateTime pauseStarted;
    private long pauseMillis;
    private long duration;
    @Getter
    private EventState state;

    TimeModuleModel() {
        startDateTime = LocalDateTime.now();
        pauseStarted = LocalDateTime.now();
        pauseMillis = 0;
        state = CREATED;
    }

    void start() {
        if (state == CREATED) {
            startDateTime = LocalDateTime.now();
            state = STARTED;
        }
    }

    void pause() {
        if (state == STARTED) {
            pauseStarted = LocalDateTime.now();
            state = PAUSED;
        }
    }

    void resume() {
        if (state == PAUSED) {
            LocalDateTime pauseEnded = LocalDateTime.now();
            pauseMillis += ChronoUnit.MILLIS.between(pauseStarted, pauseEnded);
            state = STARTED;
        }
        updateDuration(LocalDateTime.now());
    }

    void end() {
        state = ENDED;
    }

    long getUpdatedDuration(LocalDateTime now) {
        updateDuration(now);
        return duration;
    }

    long getDuration() {
        return duration;
    }

    long getFullDuration() {
        return duration + pauseMillis;
    }

    private void updateDuration(LocalDateTime now) {
        duration = ChronoUnit.MILLIS.between(startDateTime, now) - pauseMillis;
    }
}
