package com.dominikjambor.sportassistant.modules.generic.SubstitutionsModule;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.dominikjambor.sportassistant.R;
import com.dominikjambor.sportassistant.data.Player;
import com.dominikjambor.sportassistant.modules.generic.SubstitutionsModule.SubstitutionsModule.StatTableRow;
import com.jjoe64.graphview.DefaultLabelFormatter;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class SubstitutionPlayerStatFragment extends Fragment {

    @BindView(R.id.generic_substitutions_playerstat_playername) TextView playerName;
    @BindView(R.id.generic_substitutions_playerstat_total) TextView total;
    @BindView(R.id.generic_substitutions_playerstat_avg) TextView avg;

    @BindView(R.id.generic_substitutions_playerstat_graph) GraphView graph;
    @BindView(R.id.generic_substitutions_playerstat_table) LinearLayout container;

    private CompositeDisposable trash = new CompositeDisposable();
    private SubstitutionPlayerStatViewModel viewModel;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.module_generic_substitution_playerstats, container, false);

        ButterKnife.bind(this, root);
        viewModel = new ViewModelProvider(requireParentFragment()).get(SubstitutionPlayerStatViewModel.class);

        if (viewModel.getPlayer() == null) {
            trash.add(viewModel.getAllPlayers()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(players -> {
                        AlertDialog.Builder builder = new AlertDialog.Builder(requireContext());
                        builder.setTitle("Choose a player!");

                        String[] names = players.stream().map(Player::getName).toArray(String[]::new);
                        builder.setItems(names, (dialog, position)
                                -> onPlayerChoose(players.get(position)));
                        builder.setCancelable(false);
                        AlertDialog dialog = builder.create();
                        dialog.show();
                    })
            );
        } else {
            onPlayerChoose(viewModel.getPlayer());
        }

        return root;
    }

    private void onPlayerChoose(Player player) {
        playerName.setText(String.format("%s (%s)", player.getName(), player.getJerseyNumber()));
        viewModel.setPlayer(player);
        trash.add(viewModel.getDataPoints()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dataPoints -> {
                            createGraph(dataPoints, graph);
                            createTable(viewModel.getTable(), container, "Minutes");
                            total.setText(String.format("Total: %s minutes.", viewModel.getTotal()));
                            avg.setText(String.format("Average: %s minutes.", viewModel.getAvg()));
                        },
                        Throwable::printStackTrace));
    }

    private void createTable(List<StatTableRow> rows, LinearLayout tableContainer, String thirdColumn) {
        tableContainer.removeAllViews();
        tableContainer.addView(createRow("Date", "Event", thirdColumn, true));
        rows.forEach(StatTableRow -> tableContainer.addView(createRow(StatTableRow.dateStr,
                StatTableRow.opponent,
                String.valueOf(StatTableRow.duration),
                false)));
        if (rows.isEmpty()) {
            tableContainer.addView(createRow("",
                    requireContext().getString(R.string.no_data_to_show),
                    "", false));
        }
    }

    private LinearLayout createRow(String date, String opponent, String duration, boolean isBold) {
        LinearLayout rowLayout = new LinearLayout(requireContext());
        rowLayout.setLayoutParams(new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        ));
        rowLayout.setOrientation(LinearLayout.HORIZONTAL);

        TextView dateTextView = new TextView(requireContext());
        dateTextView.setText(date);
        dateTextView.setLayoutParams(new LinearLayout.LayoutParams(
                0,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                0.25f
        ));
        if (isBold) dateTextView.setTypeface(null, Typeface.BOLD);
        rowLayout.addView(dateTextView);

        TextView opponentTextView = new TextView(requireContext());
        opponentTextView.setText(opponent);
        opponentTextView.setLayoutParams(new LinearLayout.LayoutParams(
                0,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                0.55f
        ));
        if (isBold) opponentTextView.setTypeface(null, Typeface.BOLD);
        rowLayout.addView(opponentTextView);

        TextView sumTextView = new TextView(requireContext());
        sumTextView.setText(String.valueOf(duration));
        sumTextView.setLayoutParams(new LinearLayout.LayoutParams(
                0,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                0.2f
        ));
        if (isBold) sumTextView.setTypeface(null, Typeface.BOLD);
        rowLayout.addView(sumTextView);

        return rowLayout;
    }

    private void createGraph(List<DataPoint> dataPoints, GraphView graph) {
        DataPoint[] pointArray = new DataPoint[dataPoints.size()];
        LineGraphSeries<DataPoint> series =
                new LineGraphSeries<>(dataPoints.toArray(pointArray));
        graph.removeAllSeries();
        graph.addSeries(series);

        series.setDrawDataPoints(true);
        series.setAnimated(true);

        graph.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter() {
            @Override
            public String formatLabel(double value, boolean isValueX) {
                if (isValueX) {
                    if (value % 1 == 0 && (int) value < viewModel.getDates().size()) {
                        return viewModel.getDates().get((int) value);
                    } else {
                        return "";
                    }

                } else {
                    return super.formatLabel(value, false);
                }
            }
        });
        graph.getGridLabelRenderer().setNumHorizontalLabels(3);

        graph.getViewport().setYAxisBoundsManual(true);
        graph.getViewport().setMinY(0);
        if (!dataPoints.isEmpty()) {
            graph.getViewport().setMaxY(Collections.max(dataPoints, (o1, o2) ->
                    Double.compare(o1.getY(), o2.getY())).getY() + 1);
        } else {
            graph.getViewport().setMaxY(30);
        }

        graph.getViewport().setScalableY(false);
        graph.getViewport().setScrollableY(false);
        graph.getViewport().setScrollable(true);
        graph.getViewport().setScalable(true);

        graph.getGridLabelRenderer().setHumanRounding(true);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        trash.clear();
    }
}
