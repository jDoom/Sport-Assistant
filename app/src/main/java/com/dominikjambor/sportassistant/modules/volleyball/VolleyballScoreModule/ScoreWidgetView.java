package com.dominikjambor.sportassistant.modules.volleyball.VolleyballScoreModule;

import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.dominikjambor.sportassistant.R;
import com.dominikjambor.sportassistant.data.TeamWithPlayers;

import butterknife.BindView;
import butterknife.ButterKnife;

class ScoreWidgetView extends FrameLayout {

    @BindView(R.id.volleyball_score_widget__team1Name) TextView team1Name;
    @BindView(R.id.volleyball_score_widget__team2Name) TextView team2Name;
    @BindView(R.id.volleyball_score_widget__team1Score) TextView team1Score;
    @BindView(R.id.volleyball_score_widget__team2Score) TextView team2Score;
    @BindView(R.id.volleyball_score_widget__team1SetScore) TextView team1SetScore;
    @BindView(R.id.volleyball_score_widget__team2SetScore) TextView team2SetScore;
    @BindView(R.id.volleyball_score_widget__nextSetButton) Button nextSet;

    @BindView(R.id.volleyball_score_widget__1p1Button) Button team1IncrementBy1;
    @BindView(R.id.volleyball_score_widget__2p1Button) Button team2IncrementBy1;
    private ScoreWidgetModel model;

    ScoreWidgetView(ScoreWidgetModel model, Fragment container) {
        super(container.requireContext());
        this.model = model;

        addView(inflate(container.getContext(), R.layout.module_volleyball_score_widget, null));

        ButterKnife.bind(this);

        team1Name.setText(model.getTeam1().team.getName());
        model.getTeam1Score().observe(container, integer -> team1Score.setText(String.valueOf(integer)));
        model.getTeam1SetScore().observe(container, integer -> team1SetScore.setText(String.valueOf(integer)));
        team1IncrementBy1.setOnClickListener(view -> handleScore(model.getTeam1()));

        team2Name.setText(model.getTeam2().team.getName());
        model.getTeam2Score().observe(container, integer -> team2Score.setText(String.valueOf(integer)));
        model.getTeam2SetScore().observe(container, integer -> team2SetScore.setText(String.valueOf(integer)));
        team2IncrementBy1.setOnClickListener(view -> handleScore(model.getTeam2()));

        nextSet.setOnClickListener(v -> nextSet());
    }

    public void nextSet() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("Close this set?")
                .setPositiveButton(R.string.yes, (dialog, id) -> model.nextSet())
                .setNegativeButton(R.string.cancel, null);
        builder.create().show();
    }

    public void toggleButtons(boolean enabled) {
        team1IncrementBy1.setEnabled(enabled);
        team2IncrementBy1.setEnabled(enabled);
        nextSet.setEnabled(enabled);
    }

    private void handleScore(TeamWithPlayers team) {
        model.score(team);
    }
}
