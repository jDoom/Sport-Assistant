package com.dominikjambor.sportassistant.modules.basketball.BasketballScoreModule;

import android.view.View;

import androidx.fragment.app.Fragment;

import com.dominikjambor.sportassistant.logic.SportEvent;
import com.dominikjambor.sportassistant.modules.Module;

import org.json.JSONObject;

public class BasketballScoreModule extends Module {

    private BasketballScoreConfigFragment configFragment;
    private ScoreWidgetView widgetView;

    public BasketballScoreModule() {
    }

    public BasketballScoreModule(SportEvent event) {
        super(event);
        model = new ScoreWidgetModel(this, event);
    }

    private ScoreWidgetModel model;

    @Override
    public View createWidgetView(Fragment parent) {
        widgetView = new ScoreWidgetView(model, parent);
        return widgetView;
    }

    @Override
    public Fragment createConfigurationFragment(JSONObject preset) {
        configFragment = new BasketballScoreConfigFragment(model, preset);
        return configFragment;
    }

    @Override
    public Fragment createPlayerStatFragment() {
        return new BasketballScorePlayerStatFragment();
    }

    @Override
    public Fragment createTeamStatFragment() {
        return new BasketballScoreTeamStatFragment();
    }

    @Override
    public JSONObject createPreset() {
        return configFragment.createPreset();
    }

    @Override
    public boolean saveConfiguration() {
        return configFragment.validateAndSave();
    }

    @Override
    public void onEventStarted() {
        widgetView.toggleButtons(true);
    }

    @Override
    public void onEventEnded() {
        log("", "{ resultText : \"Result: " +
                model.getTeam1().team.getName() + "   " + model.getTeam1Score().getValue() + " - " +
                model.getTeam2Score().getValue() + "   " + model.getTeam2().team.getName() + "\" }");
    }

    @Override
    public void onEventPaused() {
        widgetView.toggleButtons(false);
    }

    @Override
    public void onEventResumed() {
        widgetView.toggleButtons(true);
    }
}
