package com.dominikjambor.sportassistant.modules.generic.TimeModule;

import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.dominikjambor.sportassistant.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TimeWidgetView extends FrameLayout {

    @BindView(R.id.timeModule_startPauseButton) Button startPauseButton;
    @BindView(R.id.timeModule_timeText) TextView timeText;

    TimeWidgetView(TimeWidgetModel model, Fragment container) {
        super(container.requireContext());

        addView(inflate(container.getContext(), R.layout.module_generic_time_widget, null));
        ButterKnife.bind(this);

        Animation blink = new AlphaAnimation(1.0f, 0.0f);
        blink.setDuration(500);
        blink.setInterpolator(Math::round);
        blink.setRepeatMode(Animation.REVERSE);
        blink.setRepeatCount(Animation.INFINITE);

        model.getTimeString().observe(container, timeText::setText);
        startPauseButton.setOnClickListener(view -> model.pauseClicked());

        model.getStartBlinkCommand().observe(container.getViewLifecycleOwner(),
                o -> timeText.startAnimation(blink));
        model.getStopBlinkCommand().observe(container.getViewLifecycleOwner(),
                o -> timeText.clearAnimation());

        model.getStartPauseButtonText().observe(container, startPauseButton::setText);
        model.getEnableButtonCommand().observe(container, o -> startPauseButton.setEnabled(true));
    }
}
