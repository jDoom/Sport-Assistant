package com.dominikjambor.sportassistant.modules.basketball.BasketballShotClockModule;

import android.view.View;

import androidx.fragment.app.Fragment;

import com.dominikjambor.sportassistant.logic.SportEvent;
import com.dominikjambor.sportassistant.modules.Module;

import org.json.JSONObject;

public class BasketballShotClockModule extends Module {

    private BasketballShotClockConfigFragment configFragment;
    private ShotClockWidgetView widgetView;

    public BasketballShotClockModule() {
    }

    public BasketballShotClockModule(SportEvent event) {
        super(event);
        model = new ShotClockWidgetModel(this, event);
    }

    private ShotClockWidgetModel model;

    @Override
    public View createWidgetView(Fragment parent) {
        widgetView = new ShotClockWidgetView(model, parent);
        return widgetView;
    }

    @Override
    public Fragment createConfigurationFragment(JSONObject preset) {
        configFragment = new BasketballShotClockConfigFragment(model, preset);
        return configFragment;
    }

    @Override
    public boolean saveConfiguration() {
        model.save();
        return true;
    }

    @Override
    public JSONObject createPreset() {
        return configFragment.createPreset();
    }

    @Override
    public void onEventStarted() {
        model.reset();
    }

    @Override
    public void onEventEnded() {
        model.stopThread();
    }

    @Override
    public void onEventPaused() {
        if (!model.isPaused()) {
            model.pause();
        }
    }

    @Override
    public void onEventResumed() {
        if (model.isPaused()) {
            model.pause();
        }
    }
}
