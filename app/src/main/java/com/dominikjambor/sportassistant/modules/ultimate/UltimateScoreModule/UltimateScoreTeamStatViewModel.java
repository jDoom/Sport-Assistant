package com.dominikjambor.sportassistant.modules.ultimate.UltimateScoreModule;

import androidx.lifecycle.ViewModel;

import com.dominikjambor.sportassistant.data.Team;
import com.dominikjambor.sportassistant.logic.ApplicationState;
import com.dominikjambor.sportassistant.modules.ultimate.UltimateScoreModule.UltimateScoreModule.StatTableRow;
import com.dominikjambor.sportassistant.persistence.SportAssistantPersistence;
import com.jjoe64.graphview.series.DataPoint;

import org.json.JSONException;
import org.json.JSONObject;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

import io.reactivex.Single;
import lombok.Getter;
import lombok.Setter;

import static com.dominikjambor.sportassistant.logic.SportEvent.SportType.ULTIMATE;
import static com.dominikjambor.sportassistant.modules.ModuleUtils.round;

public class UltimateScoreTeamStatViewModel extends ViewModel {
    private Single<List<Team>> allTeams;
    private Single<List<DataPoint>> points;
    @Getter private int total = 0;
    @Getter private double avg = 0;
    @Getter private List<StatTableRow> table;
    @Getter private List<String> opponents;
    @Getter
    @Setter
    private Team team;

    Single<List<DataPoint>> getPoints() {
        if (points == null) {
            SportAssistantPersistence persistence = ApplicationState.getInstance().getPersistence();
            points = persistence.findAllSportEventsWithLogs(ULTIMATE, team.getId()).map(events -> {
                total = 0;
                table = new ArrayList<>();
                opponents = new ArrayList<>();
                List<DataPoint> points = new ArrayList<>();
                AtomicInteger x = new AtomicInteger();
                events.forEach(eventWithLogs -> {
                    AtomicInteger sum = new AtomicInteger();
                    eventWithLogs.logEntries.stream()
                            .filter(entry -> entry.getAuthor().equals(UltimateScoreModule.class.getName()))
                            .forEach(entry -> {
                                if (entry.getData() != null && !entry.getData().isEmpty()) {
                                    try {
                                        JSONObject data = new JSONObject(entry.getData());
                                        if (Objects.equals(data.getString("teamId"), team.getId().toString())) {
                                            sum.addAndGet(data.getInt("value"));
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                    opponents.add(team.getId().equals(eventWithLogs.event.getTeam1Id()) ?
                            eventWithLogs.event.getTeam2() : eventWithLogs.event.getTeam1());
                    points.add(new DataPoint(x.get(), sum.get()));

                    table.add(new StatTableRow(eventWithLogs.event.getStartDateTime()
                            .format(DateTimeFormatter.ofPattern("YYYY.MM.dd")),
                            opponents.get(opponents.size() - 1),
                            sum.get()));

                    x.addAndGet(1);
                    total += sum.get();
                    avg = round((double) total / points.size(), 1);
                });
                return points;
            });
        }
        return points;
    }

    Single<List<Team>> getAllTeams() {
        if (allTeams == null) {
            SportAssistantPersistence persistence = ApplicationState.getInstance().getPersistence();
            allTeams = persistence.findAllTeams();
        }
        return allTeams;
    }
}
