package com.dominikjambor.sportassistant.modules.generic.NoteModule;

import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.dominikjambor.sportassistant.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NoteWidgetView extends FrameLayout {

    NoteWidgetModel model;

    @BindView(R.id.generic_note_widget_noteList) ListView noteList;
    private ArrayAdapter<String> adapter;

    NoteWidgetView(NoteWidgetModel model, Fragment container) {
        super(container.requireContext());
        this.model = model;

        addView(inflate(container.getContext(), R.layout.module_generic_note_widget, null));
        ButterKnife.bind(this);

        adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, model.getNotes());
        noteList.setAdapter(adapter);
    }

    @OnClick(R.id.generic_note_widget_addButton)
    void addClicked() {
        final EditText noteTextView = new EditText(getContext());

        noteTextView.setHint("Note...");

        new AlertDialog.Builder(getContext())
                .setTitle("Add Note")
                .setView(noteTextView)
                .setPositiveButton("Ok", (dialog, whichButton) -> {
                    String note = noteTextView.getText().toString().trim();
                    if (!note.isEmpty()) {
                        model.addNote(note);
                        adapter.notifyDataSetChanged();
                    }
                })
                .setNegativeButton("Cancel", null)
                .show();
    }
}
