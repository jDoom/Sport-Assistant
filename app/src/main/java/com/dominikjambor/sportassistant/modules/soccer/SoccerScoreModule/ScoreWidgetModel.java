package com.dominikjambor.sportassistant.modules.soccer.SoccerScoreModule;

import androidx.lifecycle.MutableLiveData;

import com.dominikjambor.sportassistant.data.Player;
import com.dominikjambor.sportassistant.data.TeamWithPlayers;
import com.dominikjambor.sportassistant.logic.SportEvent;

import org.json.JSONObject;

import lombok.Getter;
import lombok.Setter;

@Getter
class ScoreWidgetModel {

    ScoreWidgetModel(SoccerScoreModule module, SportEvent event) {
        this.module = module;
        team1 = event.getTeam1();
        team2 = event.getTeam2();
    }

    private SoccerScoreModule module;
    private final TeamWithPlayers team1;
    private final TeamWithPlayers team2;

    @Setter private boolean playerSelectionEnabled;

    private MutableLiveData<Integer> team1Score = new MutableLiveData<>(0);
    private MutableLiveData<Integer> team2Score = new MutableLiveData<>(0);

    void score(TeamWithPlayers team, int value, Player player) {
        if (team == team1 && team1Score.getValue() != null) {
            team1Score.postValue(team1Score.getValue() + value);
        } else if (team == team2 && team2Score.getValue() != null) {
            team2Score.postValue(team2Score.getValue() + value);
        }

        JSONObject data = new JSONObject();
        try {
            data.put("teamId", team.team.getId().toString());
            if (player != null) {
                data.put("playerId", player.getId().toString());
            }
            data.put("value", value);
        } catch (Exception e) {
            module.log("Score data failure: " + e.getMessage(), "{}");
        }

        module.log(team.team.getName() +
                        ((player != null) ? " (" + player.getName() + ")" : "") + " scored " +
                        ((value == 1) ? "a point" : value + " points"),
                data.toString()
        );
    }
}
