package com.dominikjambor.sportassistant.modules.generic.NoteModule;

import com.dominikjambor.sportassistant.logic.EventTimeProvider;
import com.dominikjambor.sportassistant.logic.SportEvent;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;

public class NoteWidgetModel {
    @Getter private List<String> notes = new ArrayList<>();
    private NoteModule module;
    private SportEvent event;

    NoteWidgetModel(NoteModule module, SportEvent event) {
        this.module = module;
        this.event = event;
    }

    void addNote(String note) {
        EventTimeProvider timeProvider = event.getTimeProvider();
        notes.add(0, timeProvider == null ? note :
                timeProvider.getLogTimeStampText() + " " + note);
        note = "Note: ".concat(note);
        module.log(note, "{}");
    }
}
