package com.dominikjambor.sportassistant.modules;

import lombok.AllArgsConstructor;

public class ModuleUtils {
    public static double round(double value, int precision) {
        int scale = (int) Math.pow(10, precision);
        return (double) Math.round(value * scale) / scale;
    }

    @AllArgsConstructor
    public static class StatTableRow {
        public String dateStr;
        public String opponent;
        public int score;
    }
}
