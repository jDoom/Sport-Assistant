package com.dominikjambor.sportassistant.modules.soccer.SoccerPossessionModule;

import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.dominikjambor.sportassistant.R;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.dominikjambor.sportassistant.modules.soccer.SoccerPossessionModule.PossessionWidgetModel.BallStatus.NONE;
import static com.dominikjambor.sportassistant.modules.soccer.SoccerPossessionModule.PossessionWidgetModel.BallStatus.TEAM_1;
import static com.dominikjambor.sportassistant.modules.soccer.SoccerPossessionModule.PossessionWidgetModel.BallStatus.TEAM_2;

public class PossessionWidgetView extends FrameLayout {

    @BindView(R.id.soccer_possession_widget_team1Button) Button team1Button;
    @BindView(R.id.soccer_possession_widget_team2Button) Button team2Button;
    @BindView(R.id.soccer_possession_widget_noTeamButton) Button noTeamButton;
    @BindView(R.id.soccer_possession_widget_team1Name) TextView team1Name;
    @BindView(R.id.soccer_possession_widget_team2Name) TextView team2Name;

    PossessionWidgetView(PossessionWidgetModel model, Fragment container) {
        super(container.requireContext());

        addView(inflate(container.getContext(), R.layout.module_soccer_possession_widget, null));

        ButterKnife.bind(this);
        team1Name.setText(model.getTeam1().team.getName());
        team2Name.setText(model.getTeam2().team.getName());

        model.getEnableButtonsCommand().observe(container, o -> {
            team1Button.setEnabled(true);
            team2Button.setEnabled(true);
        });
        team1Button.setOnClickListener(v -> {
            model.changeStatusTo(TEAM_1);
            team1Button.setEnabled(false);
            team2Button.setEnabled(true);
            noTeamButton.setEnabled(true);
        });
        team2Button.setOnClickListener(v -> {
            model.changeStatusTo(TEAM_2);
            team1Button.setEnabled(true);
            team2Button.setEnabled(false);
            noTeamButton.setEnabled(true);
        });
        noTeamButton.setOnClickListener(v -> {
            model.changeStatusTo(NONE);
            team1Button.setEnabled(true);
            team2Button.setEnabled(true);
            noTeamButton.setEnabled(false);
        });

    }
}
