package com.dominikjambor.sportassistant.modules.ultimate.UltimateScoreModule;

import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.dominikjambor.sportassistant.R;
import com.dominikjambor.sportassistant.data.TeamWithPlayers;

import butterknife.BindView;
import butterknife.ButterKnife;

class ScoreWidgetView extends FrameLayout {

    @BindView(R.id.ultimate_score_widget__team1Name) TextView team1Name;
    @BindView(R.id.ultimate_score_widget__team2Name) TextView team2Name;
    @BindView(R.id.ultimate_score_widget__team1Score) TextView team1Score;
    @BindView(R.id.ultimate_score_widget__team2Score) TextView team2Score;

    @BindView(R.id.ultimate_score_widget__1p1Button) Button team1IncrementBy1;
    @BindView(R.id.ultimate_score_widget__2p1Button) Button team2IncrementBy1;
    private ScoreWidgetModel model;

    ScoreWidgetView(ScoreWidgetModel model, Fragment container) {
        super(container.requireContext());
        this.model = model;

        addView(inflate(container.getContext(), R.layout.module_ultimate_score_widget, null));

        ButterKnife.bind(this);

        team1Name.setText(model.getTeam1().team.getName());
        model.getTeam1Score().observe(container, integer -> team1Score.setText(String.valueOf(integer)));
        team1IncrementBy1.setOnClickListener(view -> handleScore(model.getTeam1(), 1));

        team2Name.setText(model.getTeam2().team.getName());
        model.getTeam2Score().observe(container, integer -> team2Score.setText(String.valueOf(integer)));
        team2IncrementBy1.setOnClickListener(view -> handleScore(model.getTeam2(), 1));
    }

    public void toggleButtons(boolean enabled) {
        team1IncrementBy1.setEnabled(enabled);
        team2IncrementBy1.setEnabled(enabled);
    }

    private void handleScore(TeamWithPlayers team, int value) {
        if (!model.isPlayerSelectionEnabled() || team.players.isEmpty()) {
            model.score(team, value, null);
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setTitle("Choose player!");

            String[] names = team.players.stream()
                    .map(p -> p.getName() + (p.getJerseyNumber().isEmpty() ? "" : " (" + p.getJerseyNumber() + ")")).toArray(String[]::new);
            builder.setItems(names, (dialog, position)
                    -> model.score(team, value, team.players.get(position)));

            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }
}
