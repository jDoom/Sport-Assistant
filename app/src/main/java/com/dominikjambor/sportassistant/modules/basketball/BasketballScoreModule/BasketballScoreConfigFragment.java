package com.dominikjambor.sportassistant.modules.basketball.BasketballScoreModule;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.dominikjambor.sportassistant.R;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BasketballScoreConfigFragment extends Fragment {

    private ScoreWidgetModel model;
    private JSONObject preset;

    public BasketballScoreConfigFragment(ScoreWidgetModel model, JSONObject preset) {
        this.model = model;
        this.preset = preset;
        setRetainInstance(true);
    }

    @BindView(R.id.basketball_score_config_enablePlayer) CheckBox enablePlayers;
    @BindView(R.id.basketball_score_config_enableOnePointers) CheckBox enableOnePointers;
    @BindView(R.id.basketball_score_config_enableTwoPointers) CheckBox enableTwoPointers;
    @BindView(R.id.basketball_score_config_enableThreePointers) CheckBox enableThreePointers;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.module_basketball_score_configuration, container, false);

        ButterKnife.bind(this, root);

        if (preset != null) {
            try {
                if (preset.has("enablePlayers"))
                    enablePlayers.setChecked(preset.getBoolean("enablePlayers"));
                if (preset.has("enableOnePointers"))
                    enableOnePointers.setChecked(preset.getBoolean("enableOnePointers"));
                if (preset.has("enableTwoPointers"))
                    enableTwoPointers.setChecked(preset.getBoolean("enableTwoPointers"));
                if (preset.has("enableThreePointers"))
                    enableThreePointers.setChecked(preset.getBoolean("enableThreePointers"));
            } catch (Exception e) {
                Toast.makeText(requireContext(), "Error loading preset", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }

        return root;
    }

    boolean validateAndSave() {
        model.setPlayerSelectionEnabled(enablePlayers.isChecked());
        model.setOnePointersEnabled(enableOnePointers.isChecked());
        model.setTwoPointersEnabled(enableTwoPointers.isChecked());
        model.setThreePointersEnabled(enableThreePointers.isChecked());
        return true;
    }

    JSONObject createPreset() {
        JSONObject preset = new JSONObject();
        try {
            preset.put("enablePlayers", enablePlayers.isChecked());
            preset.put("enableOnePointers", enableOnePointers.isChecked());
            preset.put("enableTwoPointers", enableTwoPointers.isChecked());
            preset.put("enableThreePointers", enableThreePointers.isChecked());
        } catch (Exception e) {
            Toast.makeText(requireContext(), "Error creating preset", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
        return preset;
    }
}
