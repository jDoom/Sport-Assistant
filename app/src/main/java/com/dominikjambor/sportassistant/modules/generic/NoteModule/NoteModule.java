package com.dominikjambor.sportassistant.modules.generic.NoteModule;

import android.view.View;

import androidx.fragment.app.Fragment;

import com.dominikjambor.sportassistant.logic.SportEvent;
import com.dominikjambor.sportassistant.modules.Module;

public class NoteModule extends Module {

    private NoteWidgetModel widgetModel;
    private NoteWidgetView widgetView;

    public NoteModule() {
    }

    public NoteModule(SportEvent event) {
        super(event);
        widgetModel = new NoteWidgetModel(this, event);
    }

    @Override
    public View createWidgetView(Fragment parent) {
        widgetView = new NoteWidgetView(widgetModel, parent);
        return widgetView;
    }
}
