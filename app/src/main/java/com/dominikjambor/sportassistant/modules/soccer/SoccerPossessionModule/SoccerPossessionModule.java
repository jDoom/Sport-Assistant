package com.dominikjambor.sportassistant.modules.soccer.SoccerPossessionModule;

import android.view.View;

import androidx.fragment.app.Fragment;

import com.dominikjambor.sportassistant.logic.SportEvent;
import com.dominikjambor.sportassistant.modules.Module;

import lombok.AllArgsConstructor;

import static com.dominikjambor.sportassistant.logic.SportEvent.EventState.STARTED;

public class SoccerPossessionModule extends Module {
    private PossessionWidgetModel widgetModel;
    private PossessionWidgetView widgetView;
    //private SubstitutionsConfigFragment configFragment;

    public SoccerPossessionModule() {
    }

    public SoccerPossessionModule(SportEvent event) {
        super(event);
        widgetModel = new PossessionWidgetModel(this, event);
    }

    @Override
    public View createWidgetView(Fragment parent) {
        widgetView = new PossessionWidgetView(widgetModel, parent);
        return widgetView;
    }

    @Override
    public Fragment createTeamStatFragment() {
        return new PossessionTeamStatFragment();
    }

    @Override
    public void onEventStarted() {
        widgetModel.start();
    }

    @Override
    public void onEventEnded() {
        widgetModel.logTimes(event.getState() == STARTED);
    }

    @Override
    public void onEventPaused() {
        widgetModel.pause();
    }

    @Override
    public void onEventResumed() {
        widgetModel.resume();
    }

    @AllArgsConstructor
    static class StatTableRow {
        public String dateStr;
        public String opponent;
        public double duration;
    }
}
