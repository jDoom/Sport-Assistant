package com.dominikjambor.sportassistant;

import com.dominikjambor.sportassistant.data.Player;
import com.dominikjambor.sportassistant.data.Team;
import com.dominikjambor.sportassistant.data.TeamWithPlayers;
import com.dominikjambor.sportassistant.logic.SportEvent;
import com.dominikjambor.sportassistant.modules.Module;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Collections;
import java.util.UUID;

import static com.dominikjambor.sportassistant.logic.SportEvent.EventState.CREATED;
import static com.dominikjambor.sportassistant.logic.SportEvent.EventState.ENDED;
import static com.dominikjambor.sportassistant.logic.SportEvent.EventState.PAUSED;
import static com.dominikjambor.sportassistant.logic.SportEvent.EventState.STARTED;
import static com.dominikjambor.sportassistant.logic.SportEvent.SportType.BASKETBALL;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class SportEventTest {

    private SportEvent sportEvent;
    private static Team TEAM_ONE = new Team(UUID.fromString("9a0cfe03-6573-4b75-b00d-10939a9d2daa"), "Team1");
    private static Team TEAM_TWO = new Team(UUID.fromString("713a3383-196b-4df5-b075-62a56c9bed96"), "Team2");
    private static Player PLAYER_ONE = new Player(UUID.fromString("7c1769c5-f05a-4d71-9196-91786d5ce8b5"), "p1name", "jn1", TEAM_ONE.getId());
    private static Player PLAYER_TWO = new Player(UUID.fromString("97fb8e4d-9c22-4bd1-bde7-1a0bb7d58bfe"), "p2name", "jn2", TEAM_TWO.getId());
    private static String EVENT_NAME = "name";
    private static String EVENT_DESC = "desc";

    @Before
    public void init() {
        sportEvent = new SportEvent(EVENT_NAME, EVENT_DESC,
                new TeamWithPlayers(TEAM_ONE, Collections.singletonList(PLAYER_ONE)),
                new TeamWithPlayers(TEAM_TWO, Collections.singletonList(PLAYER_TWO)),
                BASKETBALL);
    }

    @Test
    public void instantiationTest() {
        Assert.assertEquals(sportEvent.getName(), EVENT_NAME);
        Assert.assertEquals(sportEvent.getDescription(), EVENT_DESC);
        Assert.assertEquals(sportEvent.getSport(), BASKETBALL);
        Assert.assertEquals(sportEvent.getState(), CREATED);
    }

    @Test
    public void eventFlowTest() throws InterruptedException {
        Assert.assertEquals(sportEvent.getState(), CREATED);
        sportEvent.start();
        Assert.assertEquals(sportEvent.getState(), STARTED);
        sportEvent.pause();
        Assert.assertEquals(sportEvent.getState(), PAUSED);
        sportEvent.resume();
        Assert.assertEquals(sportEvent.getState(), STARTED);
        Thread.sleep(10);
        sportEvent.end();
        Assert.assertEquals(sportEvent.getState(), ENDED);

        Assert.assertTrue(sportEvent.getEndDateTime().isAfter(sportEvent.getStartDateTime()));
    }

    @Test
    public void moduleTest() {
        Module module = Mockito.mock(Module.class);
        sportEvent.getModules().add(module);

        sportEvent.start();
        verify(module, times(1)).onEventStarted();
        sportEvent.pause();
        verify(module, times(1)).onEventPaused();
        sportEvent.resume();
        verify(module, times(1)).onEventResumed();
        sportEvent.end();
        verify(module, times(1)).onEventEnded();
    }

    @Test
    public void logTest() {
        sportEvent.log("author", "data", "text");
        Assert.assertEquals(sportEvent.getLog().size(), 1);
        Assert.assertEquals(sportEvent.getLog().get(0).getAuthor(), "author");
        Assert.assertEquals(sportEvent.getLog().get(0).getData(), "data");
        Assert.assertEquals(sportEvent.getLog().get(0).getText(), "text");
    }
}
