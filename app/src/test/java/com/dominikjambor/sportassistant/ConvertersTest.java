package com.dominikjambor.sportassistant;

import com.dominikjambor.sportassistant.logic.SportEvent.SportType;
import com.dominikjambor.sportassistant.persistence.room.Converters;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.UUID;

import static com.dominikjambor.sportassistant.logic.SportEvent.SportType.BASKETBALL;
import static org.junit.Assert.assertEquals;

public class ConvertersTest {
    @Test
    public void uuidTest() {
        String strUUID = "9db31431-70ff-41ba-a7f0-be1e4c50d147";
        UUID uuid = UUID.fromString(strUUID);

        assertEquals(Converters.uuidFromString(strUUID), uuid);
        assertEquals(Converters.uuidToString(uuid), strUUID);
    }

    @Test
    public void localDateTimeTest() {
        LocalDateTime localDateTime = LocalDateTime.now();
        String localDateTimeStr = localDateTime.toString();

        assertEquals(Converters.localDateTimeFromString(localDateTimeStr), localDateTime);
        assertEquals(Converters.localDateTimeToString(localDateTime), localDateTimeStr);
    }

    @Test
    public void sportTypeTest() {
        SportType sportType = BASKETBALL;
        String sportTypeStr = "BASKETBALL";

        assertEquals(Converters.sportTypeFromString(sportTypeStr), sportType);
        assertEquals(Converters.sportTypeToString(sportType), sportTypeStr);
    }

    @Test
    public void jsonObjectTest() throws JSONException {
        String jsonObjectStr = "{\"data\":1}";
        JSONObject jsonObject = new JSONObject(jsonObjectStr);

        assertEquals(Converters.jsonObjectFromString(jsonObjectStr).toString(), jsonObject.toString());
        assertEquals(Converters.jsonObjectToString(jsonObject), jsonObjectStr);
    }
}