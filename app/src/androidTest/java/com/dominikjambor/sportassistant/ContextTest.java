package com.dominikjambor.sportassistant;

import android.content.Context;

import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner;
import androidx.test.platform.app.InstrumentationRegistry;

import com.dominikjambor.sportassistant.logic.ApplicationState;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4ClassRunner.class)
public class ContextTest {
    @Test
    public void appContextTest() {
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        assertEquals("com.dominikjambor.sportassistant", appContext.getPackageName());
    }

    @Test
    public void applicationStateTest() {
        ApplicationState.init(InstrumentationRegistry.getInstrumentation().getTargetContext());
        Assert.assertNotNull(ApplicationState.getInstance());
        Assert.assertNotNull(ApplicationState.getInstance().getContext());
        Assert.assertNotNull(ApplicationState.getInstance().getPersistence());
        Assert.assertNull(ApplicationState.getInstance().getCurrentEvent());
    }
}
