package com.dominikjambor.sportassistant;

import android.content.Context;

import androidx.room.Room;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner;

import com.dominikjambor.sportassistant.data.ArchivedLogEntry;
import com.dominikjambor.sportassistant.data.ArchivedSportEvent;
import com.dominikjambor.sportassistant.data.ArchivedSportEventWithLogs;
import com.dominikjambor.sportassistant.data.LogEntry;
import com.dominikjambor.sportassistant.data.Player;
import com.dominikjambor.sportassistant.data.Preset;
import com.dominikjambor.sportassistant.data.Team;
import com.dominikjambor.sportassistant.data.TeamWithPlayers;
import com.dominikjambor.sportassistant.logic.SportEvent;
import com.dominikjambor.sportassistant.persistence.room.AppDatabase;
import com.dominikjambor.sportassistant.persistence.room.LogEntryDAO;
import com.dominikjambor.sportassistant.persistence.room.PlayerDAO;
import com.dominikjambor.sportassistant.persistence.room.PresetDAO;
import com.dominikjambor.sportassistant.persistence.room.SportEventDAO;
import com.dominikjambor.sportassistant.persistence.room.TeamDAO;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4ClassRunner.class)
public class DatabaseEntityReadWriteTest {
    private PlayerDAO playerDao;
    private PresetDAO presetDAO;
    private TeamDAO teamDao;
    private LogEntryDAO logEntryDAO;
    private SportEventDAO sportEventDAO;
    private AppDatabase db;
    private static UUID UUID_1 = UUID.fromString("81b7aa9c-8883-11ea-bc55-0242ac130003");
    private static UUID UUID_2 = UUID.fromString("8ea8e5f4-8883-11ea-bc55-0242ac130003");

    @Before
    public void createDb() {
        Context context = ApplicationProvider.getApplicationContext();
        db = Room.inMemoryDatabaseBuilder(context, AppDatabase.class)
                .allowMainThreadQueries()
                .build();
        playerDao = db.playerDAO();
        teamDao = db.teamDAO();
        presetDAO = db.presetDAO();
        logEntryDAO = db.logEntryDAO();
        sportEventDAO = db.sportEventDAO();
    }

    @After
    public void closeDb() {
        db.close();
    }

    @Test
    public void playerDaoTest() {
        Team team = new Team(UUID_2, "team");
        Player player = new Player(UUID_1, "player", "jersey", UUID_2);

        teamDao.insert(team).blockingAwait();
        playerDao.insert(player).blockingAwait();

        //findAll

        List<Player> result = playerDao.findAll().blockingGet();
        assertThat(result, equalTo(singletonList(player)));

        //findByTeam

        result = playerDao.findByTeam(UUID_2).blockingGet();
        assertThat(result, equalTo(singletonList(player)));

        //find updated

        player.setName("name2");
        playerDao.update(player).blockingAwait();

        result = playerDao.findAll().blockingGet();
        assertThat(result, equalTo(singletonList(player)));

        //delete

        playerDao.delete(player).blockingAwait();
        result = playerDao.findAll().blockingGet();
        assertThat(result, equalTo(emptyList()));
    }

    @Test
    public void teamDaoTest() {
        Team team = new Team(UUID_1, "team");
        Player player = new Player(UUID_2, "player", "jersey", UUID_1);
        teamDao.insert(team).blockingAwait();
        playerDao.insert(player).blockingAwait();

        //findAll

        List<Team> result = teamDao.findAll().blockingGet();
        assertThat(result, equalTo(singletonList(team)));

        //findAllWithPlayers

        List<TeamWithPlayers> resultWithPlayers = teamDao.findAllTeamsWithPlayers().blockingGet();
        assertThat(resultWithPlayers, equalTo(singletonList(new TeamWithPlayers(team, singletonList(player)))));

        //find updated

        team.setName("name2");
        teamDao.update(team).blockingAwait();

        result = teamDao.findAll().blockingGet();
        assertThat(result, equalTo(singletonList(team)));

        //delete SET NULL

        teamDao.delete(team).blockingAwait();
        result = teamDao.findAll().blockingGet();
        assertThat(result, equalTo(emptyList()));

        player.setTeamId(null);
        List<Player> playerResult = playerDao.findAll().blockingGet();
        assertThat(playerResult, equalTo(singletonList(player)));
    }

    @Test
    public void presetDaoTest() throws JSONException {
        Preset preset = new Preset("name", new JSONObject("{data:5}"));

        presetDAO.insert(preset).blockingAwait();

        //findAll

        List<Preset> result = presetDAO.findAll().blockingGet();
        assertTrue(equals(result.get(0), preset));

        //delete

        presetDAO.delete(preset).blockingAwait();
        result = presetDAO.findAll().blockingGet();
        assertThat(result, equalTo(emptyList()));
    }

    private static boolean equals(Preset p1, Preset p2) {
        return p1.getName().equals(p2.getName()) &&
                p1.getData().toString().equals(p2.getData().toString());
    }

    @Test
    public void logEntryDaoTest() {
        ArchivedSportEvent event = new ArchivedSportEvent(
                UUID_1, "name", "desc", LocalDateTime.now(), LocalDateTime.now(),
                SportEvent.SportType.SOCCER, "t1", "t2", UUID.randomUUID(), UUID.randomUUID()
        );
        ArchivedLogEntry logEntry = new ArchivedLogEntry(
                new LogEntry("t", "{}", "a", LocalDateTime.now(), 0),
                UUID_1
        );
        logEntry.setId(UUID_2);

        sportEventDAO.insert(event).blockingAwait();
        logEntryDAO.insert(logEntry).blockingAwait();

        //findAll

        List<ArchivedLogEntry> result = logEntryDAO.findAll().blockingGet();
        assertThat(result, equalTo(singletonList(logEntry)));

        //findByEvent

        result = logEntryDAO.findAllByEventId(UUID_1).blockingGet();
        assertThat(result, equalTo(singletonList(logEntry)));

        //findByAuthor

        result = logEntryDAO.findAllByAuthor("a").blockingGet();
        assertThat(result, equalTo(singletonList(logEntry)));

        //delete CASCADE

        sportEventDAO.delete(event).blockingAwait();
        result = logEntryDAO.findAll().blockingGet();
        assertThat(result, equalTo(emptyList()));
    }

    @Test
    public void sportEventDaoTest() {
        ArchivedSportEvent event = new ArchivedSportEvent(
                UUID_1, "name", "desc", LocalDateTime.now(), LocalDateTime.now(),
                SportEvent.SportType.SOCCER, "t1", "t2", UUID.randomUUID(), UUID.randomUUID()
        );
        ArchivedLogEntry logEntry = new ArchivedLogEntry(
                new LogEntry("t", "{}", "a", LocalDateTime.now(), 0),
                UUID_1
        );
        logEntry.setId(UUID_2);

        sportEventDAO.insert(event).blockingAwait();
        logEntryDAO.insert(logEntry).blockingAwait();

        //findAll

        List<ArchivedSportEvent> result = sportEventDAO.findAll().blockingGet();
        assertThat(result, equalTo(singletonList(event)));

        //findAllWithLogEntries

        List<ArchivedSportEventWithLogs> resultWithLogEntries = sportEventDAO.findAllWithLogs().blockingGet();
        assertThat(resultWithLogEntries,
                equalTo(singletonList(new ArchivedSportEventWithLogs(event, singletonList(logEntry)))));

        resultWithLogEntries = sportEventDAO.findAllWithLogs(SportEvent.SportType.SOCCER).blockingGet();
        assertThat(resultWithLogEntries,
                equalTo(singletonList(new ArchivedSportEventWithLogs(event, singletonList(logEntry)))));

        //find updated

        event.setName("name2");
        sportEventDAO.update(event).blockingAwait();

        result = sportEventDAO.findAll().blockingGet();
        assertThat(result, equalTo(singletonList(event)));

        //delete

        sportEventDAO.delete(event).blockingAwait();
        result = sportEventDAO.findAll().blockingGet();
        assertThat(result, equalTo(emptyList()));
    }
}
